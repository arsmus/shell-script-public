#!/bin/bash
#
# 2009 Feb. 15.
# 2008 Jan. 20.
# Ryuichi Hashimoto.
# Reconnect internet on ppxp with EM-ONE(S01SH)
#
#


#
# Usage
#
if [ $# -gt 0 ]; then
	CMDNAME=`basename $0`
	echo 'Reconnect internet on EM-ONE, if internet is not connected.'
	echo "Usage : $CMDNAME"
	exit 1
fi



#
# set variables
#
PPXPCONF='em-one'
NTPSERVER='202.224.32.4'


#
# check EM-ONE connection through USB.
#
cat /proc/bus/usb/devices | grep -q 'Vendor=04dd ProdID=9151'
if [ $? -ne 0 ]; then
	echo 'EM-ONE is not connected to PC through USB.'
	exit 2
fi
echo "Found EM-ONE connected to PC through USB."


#
# check IP connection
#
ifconfig | grep -A1 tun0 | grep -Eq 'inet addr:[0-9]+.[0-9]+.[0-9]+.[0-9]+'
if [ $? -eq 0 ]; then
	echo 'tun0 exists.'
	exit 0
fi
echo "tun0 doesn't exist."


#
# Unload module for ppxp with EM-ONE(S01SH)
#
echo 'Stopping ppxp.'
ppxp -C quit
sleep 8
echo 'Unloading irq usbserial modules.'
modprobe -r ipaq usbserial


#
# Connect internet on ppxp with EM-ONE(S01SH)
#
echo 'Loading irq module.'
modprobe ipaq vendor=0x04dd product=0x9151
echo 'Starting ppxp ip connection.'
ppxp -C source $PPXPCONF
ppxp -C connect

