#!/bin/sh
#
# 2007.Sep.15.
# Written by Ryuichi Hashimoto.
#
# Atach ipaq-qmodule.
#
VEND='0x04dd'
PROD='0x9151'
#
modprobe -r ipaq
modprobe ipaq vendor=$VEND product=$PROD
network-admin
