#!/bin/sh

# 2017 Feb. 12.
# scan virus with clamdscan.

LOG='/var/log/clamav/clamdscan.log'
INFECT_DIR='/var/infected'

if [ ! -d "${INFECT_DIR}" ]; then 
  mkdir -p "${INFECT_DIR}"
  chown root:clamav "${INFECT_DIR}"
  if [ $? -ne 0 ]; then
    echo 'Failed.' 1>&2
    echo "  mkdir -p ${INFECT_DIR}" 1>&2
    echo "  chown root:clamav ${INFECT_DIR}" 1>&2
    exit 1
  fi
fi

echo `date '+%F %R'` >> "${LOG}"

/usr/bin/clamdscan --move="${INFECT_DIR}" -l "${LOG}"  /

