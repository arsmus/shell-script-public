#!/bin/sh

# 2017 May 06.
# 2016 Oct. 08.

LOG='/var/log/clamav/clamscan.log'
scanCycleDays=10
infectedFilesDir='/var/clam/infected'

# Get last scan date.
lastScanDate=`tail -n 1 "${LOG}"`
lastScanDatePlusOne=`expr "${lastScanDate}" + 1`
ret="$?"

# "$ret = 0" means "lastScanDate is integer"
if [ "$ret" -ne 0 ]; then
  lastScanDate=`date --date "20161201" +%s`
fi
if [ ${#lastScanDate} -lt 10 ]; then
  echo 'Last scan date is invalid. Job continues.' 1>&2
fi

# If the period from previous scan is short, abort.
notScanDays=`expr \( \`date +%s\` - ${lastScanDate} \) / \( 60 \* 60 \* 24 \)`
echo "notScanDays : ${notScanDays}" 1>&2
if [ $notScanDays -lt $scanCycleDays ]; then 
  echo 'Aborted. Currently scan was done.' 1>&2
  exit 1
fi

echo -n `LANG=C date "+%Y-%m-%d-%H:%M"` >> "${LOG}"
echo ' Starting clamscan.' >> "${LOG}"
# freshclam --log="${LOG}"
# /usr/bin/clamscan --infected --copy="${infectedFilesDir}" -r --exclude-dir=^/sys /home >> "${LOG}"
/usr/bin/clamscan --infected --copy="${infectedFilesDir}" -r --exclude-dir=^/sys\|^/proc\|^/dev / >> "${LOG}"
echo -n `LANG=C date "+%Y-%m-%d-%H:%M"` >> "${LOG}"
echo ' Finishd.' >> "${LOG}"
echo `LANG=C date +%s` >> "${LOG}"

