#!/bin/sh
# 2017 Apr. 06.

LOG='/var/log/clamav/clamscan.log'
infectedFilesDir='/var/clam/infected'

echo -n `LANG=C date "+%Y-%m-%d-%H:%M"` >> "${LOG}"
echo ' Starting clamscan.' >> "${LOG}"
# freshclam --log="${LOG}"
# /usr/bin/clamscan --infected --copy="${infectedFilesDir}" -r --exclude-dir=^/sys /home >> "${LOG}"
# /usr/bin/clamscan --infected --copy="${infectedFilesDir}" -r --exclude-dir=^/sys\|^/proc\|^/dev / >> "${LOG}"
/usr/bin/clamscan --remove --infected -r --exclude-dir=^/sys\|^/proc\|^/dev / >> "${LOG}"
echo -n `LANG=C date "+%Y-%m-%d-%H:%M"` >> "${LOG}"
echo ' Finishd.' >> "${LOG}"

