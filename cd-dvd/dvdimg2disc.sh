#!/bin/bash
#
# Burn DVD-VIDEO-IMAGE to DVD-VIDEO-DISK
# (using dvdrecord)
#
# 2018 Jun. 20.
# 2007 Mar. 24.
# 2007 Jan. 13.
# Ryuichi Hashimoto.
#

# set values
IFS=' '
LOGDIR=/bigdata/tmp/log
ISODIR=/bigdata/tmp/iso
DVDDIR=/bigdata/tmp/dvd
DEVDVD=/dev/hdc
SPEED='8'

CMDNAME=`basename $0`
STARTTIME=`date`

# check args 
if [ $# -lt 1 ]; then 
	echo "Copy DVD-VIDEO-IMAGE in HDD to DVD-VIDEO-DISK. (01)" 1>&2
	echo "(using dvdrecord)" 1>&2
	echo "Usage: $CMDNAME -l LogDir -i IsoDir -s Speed DVDimgDir " 1>&2
	exit 2
fi

# check command line options
while [ "$#" -gt 0 ]; do
	case $1 in
		"-h" | "--help" | "-\?")
			echo "Copy DVD-VIDEO-IMAGE in HDD to DVD-VIDEO-DISK. (01)" 1>&2
			echo "Usage: $CMDNAME -l LogDir -i IsoDir -s Speed DVDimgDir " 1>&2
			exit 4
			;;
		"-l" | "--log")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -l | --log" 1>&2
				exit 6
			fi
			TMPVAR=$2
			LOGDIR=${TMPVAR%/}
			LOGDIR2=${LOGDIR%/}
			while  [ $LOGDIR != $LOGDIR2 ]; do
					LOGDIR=$LOGDIR2
					LOGDIR2=${LOGDIR%/}
			done
			;;
		"-i" | "--iso")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -i | --iso" 1>&2
				exit 10
			fi
			TMPVAR=$2
			ISODIR=${TMPVAR%/}
			ISODIR2=${ISODIR%/}
			while  [ $ISODIR != $ISODIR2 ]; do
				ISODIR=$ISODIR2
				ISODIR2=${ISODIR%/}
			done
			;;
		"-s" | "--speed")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -s | --speed" 1>&2
				exit 12
			fi
			SPEED=$2
			TMP=`expr $SPEED + 0`
			if [ $TMP -ne $SPEED ]; then
				echo -e "Error. -s|--speed must integer." 1>&2
				exit 14
			fi
			;;
		*)
			if [ "$#" -lt 1 ]; then
				echo "Error. No value for DVD-IMG-DIR" 1>&2
				exit 8
			fi
			TMPVAR=$1
			DVDDIR=${TMPVAR%/}
			DVDDIR2=${DVDDIR%/}
			while  [ $DVDDIR != $DVDDIR2 ]; do
					DVDDIR=$DVDDIR2
					DVDDIR2=${DVDDIR%/}
			done
			break
			;;
	esac
	shift
	shift
done

# get pathname for temporary/log files
SUBDIRNAME=`date "+_20%y%m%d%H%M%S"`

# check DVDDIR
if [ ! -d $DVDDIR ] || [ ! -r $DVDDIR ]; then
	echo -e "$DVDDIR does not exist or is not readable!" 1>&2
	exit 16
fi
if [ ! -d $DVDDIR ] || [ ! -r $DVDDIR ] || [ ! -x $DVDDIR ]; then
	echo -e "Can't read/execute ${DVDDIR}" 1>&2
	exit 20
fi


# mkdir LOGDIR
LOGDIR=${LOGDIR}/${SUBDIRNAME}
mkdir -p ${LOGDIR}
chmod u+rwx ${LOGDIR}
if [ ! -d $LOGDIR ] || [ ! -r $LOGDIR ] || [ ! -w $LOGDIR ] || [ ! -x $LOGDIR ]; then
	echo -e "Can't read/write/execute ${LOGDIR}" 1>&2
	exit 18
fi

# mkdir $ISODIR
ISODIR=${ISODIR}/${SUBDIRNAME}
mkdir -p ${ISODIR}
chmod u+rwx ${ISODIR}
if [ ! -d $ISODIR ] || [ ! -r $ISODIR ] || [ ! -w $ISODIR ] || [ ! -x $ISODIR ]; then
	echo -e "Can't read/write/execute ${ISODIR}" 1>&2
	exit 21
fi

# check dvd-image-file-size
du -bs ${DVDDIR} | awk '{ if ( $1 > 4695687168 ) {exit 26 } else {exit 0 } }'
if [ $? -ne 0 ]; then
	echo -e "Error. ${DVDDIR} is TOO BIG for DVD-VIDEO." 1>&2
	exit 28
fi

# make iso-file
echo 1>&2
echo "$STARTTIME Started" 1>&2
echo "$TIMEENDAUTHOR2 Now" 1>&2
echo -e 'Making ISO file...' 1>&2
BASENAME=`date "+20%y%m%d%H%M%S"`
mkisofs -udf -dvd-video -o ${ISODIR}/${BASENAME}.iso ${DVDDIR}/ 2> "${LOGDIR}/${BASENAME}.mkisofs.log"
if [ $? -ne 0 ]; then
	echo -e 'mkisofs : Error' 1>&2
	exit 30
fi
TIMEENDMKISOFS=`date`

# set dvd-drive DMA on
DEVDMA=`/sbin/hdparm $DEVDVD | grep 'using_dma' | awk '{print $3}'`
if [ $DEVDMA -ne 1 ]; then
	/sbin/hdparm -d 1 $DEVDVD
	if [ $? -ne 0 ]; then
		echo -e "Error. Failed to set DMA ON for ${DEVDVD}." 1>&2
		exit 31
	fi
fi

# burn DVD-VIDEO
echo 1>&2
echo "$STARTTIME Started" 1>&2
echo "$TIMEENDMKISOFS Now" 1>&2
echo -e 'Burning DVD-VIDEO...' 1>&2
dvdrecord speed=${SPEED} -v -dao dev=0,0,0 ${ISODIR}/${BASENAME}.iso 2> "${LOGDIR}/${BASENAME}.dvdrecord.log"

if [ $? -eq 0 ]; then
	rm -rf ${ISODIR}
	echo -e "$CMDNAME Finished!" 1>&2
	echo -e "DVD-VIDE-DISC made." 1>&2
	echo "$STARTTIME Started" 1>&2
	echo "$TIMEENDMKISOFS Mkisofs finished" 1>&2
	FINISHTIME=`date`
	echo "$FINISHTIME All finished" 1>&2
fi

exit $?
