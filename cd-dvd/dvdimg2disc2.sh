#!/bin/bash
#
# copy DVD-VIDEO-IMAGE to DVD-VIDEO-DISK
# (using growisofs to make DVD-DISK directly without making iso-file)
#
# 2018 Jun. 20.
# 2007 Mar. 24.
# 2007 Feb. 03.
# Ryuichi Hashimoto.
#

# set values
IFS=' '
LOGDIR=/bigdata/tmp/log
DVDDIR=/bigdata/tmp/dvd
DEVDVD=/dev/hdc
SPEED='8'

CMDNAME=`basename $0`
STARTTIME=`date`

# check args 
if [ $# -lt 1 ]; then 
	echo "Copy DVD-VIDEO-IMAGE in HDD to DVD-VIDEO-DISK. (01)" 1>&2
	echo "(using growisofs to make DVD-DISK directly without making iso-file)" 1>&2
	echo "Usage: $CMDNAME -l LogDir -s Speed DVDimgDir " 1>&2
	exit 2
fi

# check command line options
while [ "$#" -gt 0 ]; do
	case $1 in
		"-h" | "--help" | "-\?")
			echo "Copy DVD-VIDEO-IMAGE in HDD to DVD-VIDEO-DISK. (01)" 1>&2
			echo "(using growisofs to make DVD-DISK directly without making iso-file)" 1>&2
			echo "Usage: $CMDNAME -l LogDir -s Speed DVDimgDir " 1>&2
			exit 4
			;;
		"-l" | "--log")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -l | --log" 1>&2
				exit 6
			fi
			TMPVAR=$2
			LOGDIR=${TMPVAR%/}
			LOGDIR2=${LOGDIR%/}
			while  [ $LOGDIR != $LOGDIR2 ]; do
					LOGDIR=$LOGDIR2
					LOGDIR2=${LOGDIR%/}
			done
			;;
		"-s" | "--speed")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -s | --speed" 1>&2
				exit 12
			fi
			SPEED=$2
			TMP=`expr $SPEED + 0`
			if [ $TMP -ne $SPEED ]; then
				echo -e "Error. -s|--speed must integer." 1>&2
				exit 14
			fi
			;;
		*)
			if [ "$#" -lt 1 ]; then
				echo "Error. No value for DVD-IMG-DIR" 1>&2
				exit 8
			fi
			TMPVAR=$1
			DVDDIR=${TMPVAR%/}
			DVDDIR2=${DVDDIR%/}
			while  [ $DVDDIR != $DVDDIR2 ]; do
					DVDDIR=$DVDDIR2
					DVDDIR2=${DVDDIR%/}
			done
			break
			;;
	esac
	shift
	shift
done

# get pathname for temporary/log files
SUBDIRNAME=`date "+_20%y%m%d%H%M%S"`

# check DVDDIR
if [ ! -d $DVDDIR ] || [ ! -r $DVDDIR ]; then
	echo -e "$DVDDIR does not exist or is not readable!" 1>&2
	exit 16
fi
if [ ! -d $DVDDIR ] || [ ! -r $DVDDIR ] || [ ! -x $DVDDIR ]; then
	echo -e "Can't read/execute ${DVDDIR}" 1>&2
	exit 20
fi


# mkdir LOGDIR
LOGDIR=${LOGDIR}/${SUBDIRNAME}
mkdir -p ${LOGDIR}
chmod u+rwx ${LOGDIR}
if [ ! -d $LOGDIR ] || [ ! -r $LOGDIR ] || [ ! -w $LOGDIR ] || [ ! -x $LOGDIR ]; then
	echo -e "Can't read/write/execute ${LOGDIR}" 1>&2
	exit 18
fi

# check dvd-image-file-size
du -bs ${DVDDIR} | awk '{ if ( $1 > 4695687168 ) {exit 26 } else {exit 0 } }'
if [ $? -ne 0 ]; then
	echo -e "Error. ${DVDDIR} is TOO BIG for DVD-VIDEO." 1>&2
	exit 28
fi

# set dvd-drive DMA on
DEVDMA=`/sbin/hdparm $DEVDVD | grep 'using_dma' | awk '{print $3}'`
if [ $DEVDMA -ne 1 ]; then
	/sbin/hdparm -d 1 $DEVDVD
	if [ $? -ne 0 ]; then
		echo -e "Error. Failed to set DMA ON for ${DEVDVD}." 1>&2
		exit 31
	fi
fi

# burn DVD-VIDEO (not make iso-file)
echo 1>&2
echo "$STARTTIME Started" 1>&2
echo -e 'Burning DVD-VIDEO...' 1>&2
growisofs -dvd-video -udf -speed=${SPEED} -Z /dev/dvd ${DVDDIR}/ 2> "${LOGDIR}/growisofs.log"

if [ $? -eq 0 ]; then
	echo -e "$CMDNAME Finished!" 1>&2
	echo -e "DVD-VIDE-DISC made." 1>&2
	echo "$STARTTIME Started" 1>&2
	FINISHTIME=`date`
	echo "$FINISHTIME All finished" 1>&2
fi

exit $?
