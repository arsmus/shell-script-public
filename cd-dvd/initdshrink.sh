#!/bin/bash
#
# 2007 Mar. 24 - 2007 Mar. 25.
# Written by Ryuichi Hashimoto.
#
# Set DVD Shrink environment on wine.
#
#
DIR="${HOME}/.wine"
FNAME='user.reg'


# delete previous filename-records
cd $DIR
cp ./${FNAME} ./${FNAME}.dshrink

awk \
'\
/\[.*DVD Shrink.*Preferences\]/{F=1} \
/\[.*DVD Shrink.*Recent File List\]/{F=2} \
/\[.*DVD Shrink.*Recent Targets\]/{F=3} \
/\[.*DVD Shrink.*Settings\]/{F=4} \
($0 ~ /\[.*\]/) && ($0 !~ /\[.*DVD Shrink.*\]/)  {F=0} \
{ \
if ((F == 1) && ($0 ~ /^\"ImportFolder\"/) || (F == 1) && ($0 ~ /^\"TargetFolder\"/)) \
	{next} \
else \
	{ \
		if ((F == 2) && ($0 ~ /^\"File/)) \
			{next} \
		else \
			{ \
				if ((F == 3) && ($0 ~ /^\"File/)) \
					{next} \
			} \
	} \
} \
{print}' \
${FNAME}.dshrink > $FNAME

