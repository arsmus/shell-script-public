#!/bin/bash
#
# 2008 Dec. 13.
# 2008 Dec. 23.
# Ryuichi Hashimoto.
#
# SlideShow pictures
#
CMD='display'
CMDOP=''
SEC=4
declare -i CMDNUM
WKDIR="${PWD}"

SCRNAME=`basename $0`

# check args
if [ $# -lt 1 ]; then
	echo "Slide-Show pictures." 1>&2
	echo "Usage: $SCRNAME -t [--time] SECONDS -d [--dir] WorkingDir pictures [...]" 1>&2
	exit 2
fi

# check command line options
while [ "$#" -gt 0 ]; do

	# -h  --help
	if [ `expr $1 : ^-h.*` -eq 2 ] || [ `expr $1 : ^--help.*` -eq 6 ]
	then
		echo "Slide-Show pictures." 1>&2
		echo "Usage: $SCRNAME -t [--time] SECONDS -d [--dir] WorkingDir pictures [...]" 1>&2
		exit 4

	# -t  --time
	elif [ `expr $1 : ^-t.*` -eq 2 ] || [ `expr $1 : ^--time.*` -eq 6 ]
	then
		if [ "$#" -lt 2 ]; then
			echo "Error. No value for -t | --time" 1>&2
			exit 6
		fi
		SEC=$2
		shift

	# -d  --dir
	elif [ `expr $1 : ^-d.*` -eq 2 ] || [ `expr $1 : ^--dir.*` -eq 5 ]
	then
		if [ "$#" -lt 2 ]; then
			echo "Error. No value for -d | --dir" 1>&2
			exit 8
		fi
		TMPVAR=$2
		WKDIR=${TMPVAR%/}
		WKDIR2=${WKDIR%/}
		while  [ $WKDIR != $WKDIR2 ]; do
			WKDIR=$WKDIR2
			WKDIR2=${WKDIR%/}
		done
		shift

	# display option
	elif [ `expr $1 : ^-.*` -gt 1 ]; then
		CMDOP="$CMDOP $1 $2"
		shift

	# target files
	else
	TARGETS=($@)
	break
	fi
	shift
done

cd $WKDIR

while true; do
	for FNAME in ${TARGETS[@]}
	do
		$CMD $CMDOP $FNAME &
		sleep "$SEC"
		CMDNUM=`ps x | awk '{print $5,$1}' | grep "^${CMD}" | grep -v grep | awk '{print $2}'`
		if [ "$CMDNUM" -gt 1 ]; then
			kill "$CMDNUM"
			continue
		fi

		CMDNUM=`ps x | awk '{print $1,$5}' | grep "/${CMD}$" | grep -v grep | awk '{print $1}'`
		if [ "$CMDNUM" -gt 1 ]; then
			kill "$CMDNUM"
			continue
		fi
		exit 10
	done
done

exit 0
	
