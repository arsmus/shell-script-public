#!/bin/bash
# 2021 Feb. 26.
# 2017 Nov. 19.
# 2017 Sep. 17.
# 2014 Sep. 27.
# 2014 Jul. 22.
# Ryuichi Hashimoto.

# Slideshow pictures sequentially in the directory given as arguement.
#   usage: COMMAND DIR SECONDS


## set debug-mode
# DEBUG='on'
DEBUG='off'

Cmd=${0##*/}

WIDTH='800'
HIGHT='600'

if [ $# -ne 2 ]; then
  echo "${Cmd} DIR SECONDS" >&2
  exit 1
fi

Dir="$1"
Secs=$2

if [ ! -d "${Dir}" ]; then
  echo "${Dir} is not directory." >&2 
  echo "${Cmd} DIR SECONDS" >&2
  exit 1
fi
Dir=${Dir%/}

if [ $Secs -lt 2 ]; then
  echo "param-seconds must be more than 1." >&2 
  echo "${Cmd} DIR SECONDS" >&2
  exit 1
fi

# check dependency
which xprop 1> /dev/null 2> /dev/null
ReturnCode=$?
if [ $ReturnCode -ne 0 ]; then
  echo "Please install x11-utils" 1>&2
  exit 1
fi

which display 1> /dev/null 2> /dev/null
ReturnCode=$?
if [ $ReturnCode -ne 0 ]; then
  echo "Please install imagemagick" 1>&2
  exit 1
fi

which wmctrl 1> /dev/null 2> /dev/null
ReturnCode=$?
if [ $ReturnCode -ne 0 ]; then
  echo "Please install wmctrl" 1>&2
  exit 1
fi

# get and display image-files
while read -d $'\0' Filepath; do
   # get activeWindow on the desktop
   activeWin=`xprop -root _NET_ACTIVE_WINDOW`
   activeWinId=`echo ${activeWin} | grep -Eo '0x[0-9a-fA-F]+,' | grep -Eo '0x[0-9a-fA-F]+'`

   display -resize ${WIDTH}x${HIGHT} "$Filepath" &
   sleep 0.5

   # Set the window which was active before displaying image to top on desktop.
   # And give focus to it.
   wmctrl -i -R ${activeWinId}
   sleep $(( $Secs - 1 ))
   ScriptPid=$$
   PidKill=`ps a -f | awk -v ScPid=$ScriptPid '{if ($3 == ScPid && $9 == "display" && $10 == "-resize") print $2}'`
   kill -9 $PidKill 1> /dev/null 2> /dev/null
done < <(find -L "${Dir}"/ -type f \( -name '*.png' -o -name '*.jpg' \
  -o -name '*.jpeg' -o -name '*.gif' -o -name '*.bmp' \
  -o -name '*.PNG' -o -name '*.JPG' -o -name '*.JPEG' \
  -o -name '*.GIF' -o -name '*.BMP' \) -print0)

exit 0

