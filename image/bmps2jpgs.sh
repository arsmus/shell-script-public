#!/bin/bash
# 2019 Dec. 07.
# Ryuichi Hashimoto.

currentDir=`pwd`
fArray=`\find ./ -size +500k -type f -name *.bmp`
for file in ${fArray}; do
  fName=`basename ${file}`
  echo ${fName}
  parentPath=${file%${fName}}
  parentPath=${currentDir}/${parentPath#./}
  \ls -lh ${parentPath}${fName}
  echo ${parentPath}
  newFileName=${fName%bmp}jpg
  echo ${newFileName}
  cd ${parentPath}
  convert $fName $newFileName
  rm $fName
  echo 
done

