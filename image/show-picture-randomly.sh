#!/bin/bash
# 2017 Apr. 15.
# Ryuichi Hashimoto.

# Show an picture file randomly in the directory recursively specified in the argument.

# Synopsis : command DIR

commandpath="$0"

if [ $# -ne 1 ]; then
  echo "${commandpath##*/} DIR" >&2
  exit 1
fi

dir="$1"
if [ ! -d "${dir}" ]; then
  echo "${dir} is not directory." >&2 
  echo "${commandpath##*/} DIR" >&2
  exit 1
fi

if [ '/' != ${dir: -1} ]; then
  dir="${dir}/" 
fi

countFiles=`find "${dir}" -name '*.jpeg' -o -name '*.jpg' -o -name '*.png' -o -name '*.tiff' -o -name '*.bmp' | wc -l`
numFile="$(( $RANDOM % $countFiles ))"
ristretto -s `find "${dir}" -name '*.jpeg' -o -name '*.jpg' -o -name '*.png' -o -name '*.tiff' -o -name '*.bmp' | sed -n ${numFile}p`

