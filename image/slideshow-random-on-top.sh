#!/bin/bash
# 2022 Jan. 23.
# 2021 Feb. 24.
# 2018 Mar. 31.
# 2018 Jan. 21.
# 2017 Nov. 19.
# Ryuichi Hashimoto.

# Slideshow pictures randomly.
# The picture window is set to top window in desktop.
#   usage : COMMAND DIR SIZE LOCATION SECONDS 


Cmd="${0##*/}"

################
### function ###
################
function usage() {
cat << EOP
usage : "${Cmd}" DIR SIZE LOCATION SECONDS 
  SIZE: ratio to screen-resolution.
    Unit of SIZE is %.(Integer in 0-100)
    Display-size of picture is SIZE(%) of screen-resolution.

  LOCATION: location to be displayed in screen
    nw: North West
    ne: North East
    sw: South West
    se: South East
    ce: Center

Slideshow pictures randomly.
The picture window is set to top window in desktop.
EOP
}

############
### main ###
############

# check command line arguements
if [ $# -ne 4 ]; then
  usage 1>&2
  exit 1
fi

Dir="$1"
Dir="${Dir%/}"
if [ ! -d "${Dir}" ]; then
  echo "${Dir} is not directory." >&2 
  echo "${Cmd} DIR SECONDS" >&2
  exit 1
fi

Size=$2
if [ ${Size} -lt 10 ] || [ ${Size} -gt 100 ] ; then
  echo "Unit of SIZE is %.(Integer in 0-100)" 1>&2
  echo "Size of picture is SIZE(%) of screen-resolution." 1>&2
  echo "" 1>&2
  usage 1>&2
  exit 1
fi

Loc="$3"

Secs=$4
if [ $Secs -lt 2 ]; then
  echo "param-seconds is more than 1." >&2 
  usage 1>&2
  exit 1
fi

# check dependency
which xprop 1> /dev/null 2> /dev/null
ReturnCode=$?
if [ $ReturnCode -ne 0 ]; then
   echo "Please install x11-utils" 1>&2
   exit 1
fi

which display 1> /dev/null 2> /dev/null
ReturnCode=$?
if [ $ReturnCode -ne 0 ]; then
   echo "Please install imagemagick" 1>&2
   exit 1
fi

which wmctrl 1> /dev/null 2> /dev/null
ReturnCode=$?
if [ $ReturnCode -ne 0 ]; then
   echo "Please install wmctrl" 1>&2
   exit 1
fi

# get screen resolution
ScreenX=$(xdpyinfo | grep dimensions  | grep -Po -m 1 "[0-9]+x" | head -n 1)
ScreenX=${ScreenX%x}

ScreenY=$(xdpyinfo | grep dimensions  | grep -Po -m 1 "x[0-9]+" | head -n 1)
ScreenY=${ScreenY#x}


# set display size
DisplayX=$(( ${ScreenX} * ${Size} / 100 ))
DisplayY=$(( ${ScreenY} * ${Size} / 100 ))

# get location of picture in screen
case "${Loc}" in
  nw) # North West
    Geomet="${DisplayX}x${DisplayY}+0+0"
    ;;
  ne) # North East
    OffX=$(( ${ScreenX} - ${DisplayX} )) 
    Geomet="${DisplayX}x${DisplayY}+${OffX}+0"
    ;;
  sw) # South West
    OffY=$(( ${ScreenY} - ${DisplayY} )) 
    Geomet="${DisplayX}x${DisplayY}+0+${OffY}"
    ;;
  se) # South East
    OffX=$(( ${ScreenX} - ${DisplayX} )) 
    OffY=$(( ${ScreenY} - ${DisplayY} )) 
    Geomet="${DisplayX}x${DisplayY}+${OffX}+${OffY}"
    ;;
  ce) # Center
    OffX=$(( ${ScreenX} / 2 - ( ${DisplayX} / 2) )) 
    OffY=$(( ${ScreenY} / 2 - ( ${DisplayY} / 2) )) 
    Geomet="${DisplayX}x${DisplayY}+${OffX}+${OffY}"
    ;;
  *)
    echo "Illegal arguement for location." 1>&2
    echo "" 1>&2
    usage 1>&2
    exit 1
    ;;
esac

# slide show
while :
do
   countFiles=`find -L "${Dir}"/ -print0 -name '*.png' -o -name '*.jpg' -o -name '*.jpeg' -o -name '*.gif' -o -name '*.bmp' -o -name '*.PNG' -o -name '*.JPG' -o -name '*.JPEG' -o -name '*.GIF' -o -name '*.BMP' | awk -F $"\0" '{print NF}'`

   numFile="$(( $RANDOM % $countFiles + 1 ))"
     # Minimam numFile is 1. Not 0.

   # get activeWindow in the desktop
   activeWin=`xprop -root _NET_ACTIVE_WINDOW`
   #activeWinId=`echo ${activeWin} | grep -Eo '0x[0-9a-fA-F]+' | grep -Eo '0x[0-9a-fA-F]+'`
   activeWinId=$(echo ${activeWin} | grep -oP "0x[0-9aa-fA-F]+" | head -n 1)

 display -resize "${DisplayX}x${DisplayY}" -geometry "${Geomet}" `find -L "${Dir}"/ -print0 -name '*.png' -o -name '*.jpg' -o -name '*.jpeg' -o -name '*.gif' -o -name '*.bmp' -o -name '*.PNG' -o -name '*.JPG' -o -name '*.JPEG' -o -name '*.GIF' -o -name '*.BMP' | awk -F $"\0" -v NumFile="${numFile}" '{print $NumFile}'` &

   sleep 0.5
   wmctrl -r "ImageMagick" -b add,above
   wmctrl -i -R ${activeWinId}
   sleep $(( $Secs - 1 ))
   Pid=`ps a | grep "display -resize " | grep -v grep | awk '{print $1}'`
   kill -9 $Pid
done
exit 0

