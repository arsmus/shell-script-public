#!/bin/bash
# 2021 Feb. 26.
# 2018 Jan. 21.
# 2017 Nov. 22.
# 2017 Nov. 21.
# 2017 Nov. 20.
# 2017 Nov. 19.
# Ryuichi Hashimoto.

# Slideshow pictures.
#   usage: COMMAND DIR SECONDS


## set debug-mode
# DEBUG='on'
DEBUG='off'

Cmd=${0##*/}

# WIDTH='800'
# HIGHT='600'
WIDTH='600'
HIGHT='450'

if [ $# -ne 2 ]; then
  echo "${Cmd} DIR SECONDS" >&2
  exit 1
fi

Dir="$1"
secs=$2

if [ ! -d "${Dir}" ]; then
  echo "${Dir} is not directory." >&2 
  echo "${Cmd} DIR SECONDS" >&2
  exit 1
fi
Dir="${Dir%/}"

if [ $secs -lt 2 ]; then
  echo "param-seconds must be more than 1." >&2 
  echo "${Cmd} DIR SECONDS" >&2
  exit 1
fi

# check dependency
which xprop 1> /dev/null 2> /dev/null
ReturnCode=$?
if [ $ReturnCode -ne 0 ]; then
  echo "Please install x11-utils" 1>&2
  exit 1
fi

which display 1> /dev/null 2> /dev/null
ReturnCode=$?
if [ $ReturnCode -ne 0 ]; then
  echo "Please install imagemagick" 1>&2
  exit 1
fi

which wmctrl 1> /dev/null 2> /dev/null
ReturnCode=$?
if [ $ReturnCode -ne 0 ]; then
  echo "Please install wmctrl" 1>&2
  exit 1
fi


# get and display image-files 
while :
do
   # get activeWindow on the desktop
   activeWin=`xprop -root _NET_ACTIVE_WINDOW`
   activeWinId=`echo ${activeWin} | grep -Eo '0x[0-9a-fA-F]+,' | grep -Eo '0x[0-9a-fA-F]+'`

   countFiles=`find -L "${Dir}"/ -type f \( -name '*.png' -o -name '*.jpg' -o -name '*.jpeg' -o -name '*.gif' -o -name '*.bmp' -o -name '*.PNG' -o -name '*.JPG' -o -name '*.JPEG' -o -name '*.GIF' -o -name '*.BMP' \) -print0 | awk -F $"\0" '{print NF}'`

   numFile="$(( $RANDOM % $countFiles + 1 ))"
     # Minimam numFile is 1. Not 0.

   display -resize ${WIDTH}x${HIGHT} `find -L "${Dir}"/ -type f \( -name '*.png' -o -name '*.jpg' -o -name '*.jpeg' -o -name '*.gif' -o -name '*.bmp' -o -name '*.PNG' -o -name '*.JPG' -o -name '*.JPEG' -o -name '*.GIF' -o -name '*.BMP' \) -print0 | awk -F $"\0" -v NumFile="${numFile}" '{print $NumFile}'` &
   sleep 0.5

   # Set the window which was active before displaying image to top on desktop.
   # And give focus to it.
   wmctrl -i -R ${activeWinId}
   sleep $(( $secs - 1 ))

   ScriptPid=$$
   PidKill=`ps a -f | awk -v ScPid=$ScriptPid '{if ($3 == ScPid && $9 == "display" && $10 == "-resize") print $2}'`
   kill -9 $PidKill 1> /dev/null 2> /dev/null
done

exit 0

