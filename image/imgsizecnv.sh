#!/bin/sh
#
# 2014 Jul. 22.
# Ryuichi Hashimoto.

files="./tmp2/*"
for filepath in ${files}
do
	filename=${filepath##*/}
	echo $filename
	# convert -resize 700x700 -unsharp 2x1.4+0.5+0 -colors 65 -quality 100 -verbose ./tmp2/${filename} ./tmp3/s${filename}
	convert -resize 700x700  -verbose ./tmp2/${filename} ./tmp3/s${filename}
done

