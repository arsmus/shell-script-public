#!/bin/bash
# 2022 Jan. 04.
# Ryuichi Hashimoto.


############
### main ###
############

Cmd=${0##*/}

SearchTextFilepath="/bigdata/01/record-file-info/record-file-info.txt"
SearchDir="/bigdata/01"
SearchDir="${SearchDir%/}"

# find video files and operate these. 
while read -d $'\0' TvFilepath; do
  TvFilepathStem=$(echo "${TvFilepath%%.*}" | sed -e "s/\[/\\\[/g" | sed -e "s/\]/\\\]/g")
  CountGrep=$(cat "${SearchTextFilepath}" | grep -Pc "${TvFilepathStem}")
  echo "${TvFilepathStem}: ${CountGrep}" 1>&2
  if [ ${CountGrep} -lt 1 ]; then
    echo "${TvFilepathStem} does not exist."
  fi
done < <(find "${SearchDir}/" -name "*.ts" -print0 -or -name "*.m2ts" -print0 -or -name "*.mp4" -print0 -or -name "*.mpg" -print0 -or -name "*.mpeg" -print0 -or -name "*.mpeg4" -print0 -type f)
exit 0

