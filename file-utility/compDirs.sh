#!/bin/bash
# 2020 Jun. 14.
# 2020 Jun. 13.
# 2020 Jun. 10.
# 2020 Jun. 08.
# Ryuichi Hashimoto.

# diff a file in Dir1 same name-file in Dir2
# Output filenames which differ each other


Cmd=`basename $0`

function usage() {
cat << EOP
usage: ${Cmd} Dir1 Dir2

diff a file in Dir1 same name-file in Dir2
Output filenames which differ each other
EOP
}


############
### main ###
############

# check arguements
if [ $# -ne 2 ]; then
  usage
  exit 1
fi

Dir1=$1
Dir2=$2

if [ ! -d $Dir1 ]; then
  exho "${Dir1} is not directory." 1>&2
  exit 1
fi
Dir1=${Dir1%/}

if [ ! -d $Dir2 ]; then
  exho "${Dir2} is not directory." 1>&2
  exit 1
fi
Dir2=${Dir2%/}

while read -d $'\0' Filepath; do
  echo ${Filepath} | grep -q '.trash'
  ResultCode=$?
  if [ ${ResultCode} -eq 0 ]; then
    continue
  fi

  Filename=${Filepath##*/}
  CountFile=`\find ${Dir2}/ -type f -name "${Filename}" | wc -l`
  if [[ CountFile -lt 1 ]]; then
    echo -n "not exist in ${Dir2}: "
    ls -l ${Filepath} | sed -e 's/[ \t][ \t]*/ /g' | cut -d' ' -f 6-9
    echo ""
  else
    # count of ${Filename} is 1 or more than 1 in ${Dir2}
    while read -d $'\0' Filepath2; do
      Relativepath2=${Filepath2#${Dir2}}
      Relativepath1=${Filepath#${Dir1}}
      if [ ${Relativepath1} = ${Relativepath2} ]; then
        diff -q "${Filepath}" "${Filepath2}" > /dev/null 2>&1
        ResultCode=$?
        if [ $ResultCode -ne 0 ]; then
          ls -l ${Filepath} | sed -e 's/[ \t][ \t]*/ /g' | cut -d' ' -f 6-9
          ls -l ${Filepath2} | sed -e 's/[ \t][ \t]*/ /g' | cut -d' ' -f 6-9
          echo ""
        fi
      fi
    done < <(\find ${Dir2}/ -type f -name ${Filename} -print0)
  fi
done < <(\find ${Dir1}/ -type f -print0)

exit 0

