#!/bin/bash
# 2020 Oct. 03.
# Ryuichi Hashimoto.


Cmd=${0##*/}

function usage() {
cat << EOP
usage: ${Cmd} DIR1 DIR2
Delete old files which exit in both dirs.
EOP
}


############
### main ###
############
Dir1=${1%/}
Dir2=${2%/}

if [ ! -d $Dir1 ]; then
  echo "Error."
  echo "${Dir1} is not directory." 1>&2
  usage
  exit 1
fi

if [ ! -d $Dir2 ]; then
  echo "Error."
  echo "${Dir2} is not directory." 1>&2
  usage
  exit 1
fi

while read -d $'\0' Filepath; do
  Filename="${Filepath##*/}"
  if [ -f "${Dir2}/${Filename}" ]; then
    echo "1st: ${Filepath}"
    if [ "${Filepath}" -nt "${Dir2}/${Filename}" ]; then
      echo "${Dir2}/${Filename} is old."
      rm "${Dir2}/${Filename}"
    else
      echo "${Filepath} is old."
      rm "${{Filepath}"
    fi 
  fi
done < <(find "${Dir1}/" -print0)
exit 0

