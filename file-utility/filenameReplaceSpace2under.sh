#!/bin/bash
# 2017 Oct. 15.
# Ryuichi Hashimoto.

# replace space to _ and remove '?' in filename.

# COMMAND [ -r ] DIR

IFS=$'\n'
Recurs=“off“
Com=`basename $0`


# check arguements
if [ $# -lt 1 ] || [ $# -gt 2 ]; then
   echo "${Com} [ -r ] Dir"
   exit  1
fi 

if [ $# -eq 2 ]; then
   if [ "-r" = $1 ]; then
      Recurs="on"
      Dir="${2%/}"
   else
      Dir="${1%/}"
      if [ "-r" = $2 ]; then
         Recurs=“on“
      else
         echo "${Com} [ -r ] Dir"
         exit 1
      fi
   fi
else
   Dir="${1%/}"
fi

if [ ! -d $Dir ]; then
   echo "${Dir} is not directory."
   exit 1
fi

# get files
if [ 'on' = $Recurs ]; then
   Files=`find ${Dir}/ `
else 
   Files=`find ${Dir}/  -maxdepth 1 `
fi

for EachFile in $Files; do
   Filepath="${EachFile%/}"
   echo "${Filepath}..." 1>&2
   NewName=`echo $Filepath | sed -e 's/ /_/g'`
   NewName2=`echo $NewName | sed -e 's/\?/_/g'`
   if [ $Filepath != $NewName2 ]; then
      mv $Filepath $NewName2
      echo "renamed : ${Filepath} to ${NewName2}"
   fi
done
