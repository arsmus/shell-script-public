#!/usr/bin/env bash
# 2023 Feb. 23.
# Ryuichi Hashimoto.

# Move files in SrcDir to DstDir, if same name files exist in DstDir.
# SrcDir must not have sub-directories.
# When same name files are in sub-directory, files in SrcDir are moved to the sub-directory.

Cmd="${0##*/}"

function usage() {
cat << EOP
Usage: ${Cmd} SrcDir DstDir

Description
Move files in SrcDir to DstDir
if same name files exist in DstDir.
SrcDir must not have sub-directories.
When same name files are in sub-directory,
files in SrcDir are moved to the sub-directory.
EOP
}


if [ $# -ne 2 ]; then
  echo "Illegal number of arguements." 1>&2
  usage
  exit 1
fi

SrcDir="${1%/}"
DstDir="${2%/}"
if [ ! -d "${SrcDir}" ]; then
  echo "${SrcDir} is not a directory." 1>&2
  exit 1
fi
if [ ! -d "${DstDir}" ]; then
  echo "${DstDir} is not a directory." 1>&2
  exit 1
fi

while \read -d $'\0' EachFilePath; do
  if [ ${#EachFilePath} -lt 1 ]; then
    echo "No file in ${SrcDir}" 1>&2
    exit 1
  fi

  EachFileName="${EachFilePath##*/}"
  DstFilePath=$(\find "${DstDir}/" -type f -name "${EachFileName}")
  if [ ${#DstFilePath} -lt 1 ]; then
    echo "${EachFileName} is not in ${DstDir}." 1>&2
    continue
  fi
  mv "${EachFilePath}" "${DstFilePath}"
done < <(\find "${SrcDir}/" -print0)
exit 0

