#!/bin/bash

# 2016 Jul. 02.
# Ryuichi Hashimoto.

# rename all files in directory.

CMD=`basename $0`

if [ $# -lt 1 ]; then
  echo "${CMD} DIR PREFIX-CHAR(S)"
  echo 'rename all files in DIR.'
  echo 'mv DIR/file DIR/PREFIX-CHAR(S)file'
  exit 1
fi

if [ $# -gt 2 ]; then
  echo "${CMD} DIR PREFIX-CHAR(S)"
  echo 'rename all files in DIR.'
  echo 'mv DIR/file DIR/PREFIX-CHAR(S)file'
  exit 1
fi

DIR=`echo $1 | sed -e 's/\/$//'`
PRE=$2

if [ ${#DIR} -lt 1 ]; then
  echo 'Directory is improper.' 1>&2
  echo "${CMD} DIR PREFIX-CHAR(S)" 1>&2
  exit 1
fi

if [ ! -d $DIR  ]; then
  echo 'No directory. Exit.' 1>&2
  exit 1
fi

if [ ${#PRE} -lt 1 ]; then
  echo 'No prefix-char(s). Exit.' 1>&2
  exit 1
fi

files="${DIR}/*"
for filepath in $files; do
  if [ -f $filepath ]; then 
    PARENT=`dirname ${filepath}`
    FILE=`basename ${filepath}`
    mv $filepath ${PARENT}/${PRE}${FILE}
  fi
done

