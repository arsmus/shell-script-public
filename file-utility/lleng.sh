#!/bin/sh
# 2015 Jun. 27.
# Ryuichi Hashimoto.

# list filenames in directory by the length of filename.

dir=$PWD

if [ -d $1 ]; then
  dir=$1
else
  echo "$1 does not exist."
  exit 1
fi

files="${dir}/*"
for filepath in ${files}
do
  filename=`basename $filepath`
  numfilename=`echo ${#filename}`
  echo "${filename} ${numfilename}" >> /tmp/tmpnumfilename.txt
done
sort -n -k 2 /tmp/tmpnumfilename.txt
rm /tmp/tmpnumfilename.txt
exit 0

