#!/bin/bash
# 2019 Mar. 10.
# Ryuichi Hashimoto.

# Output same files in 2 directories to standard-out.

# synopsis
# COMMAND DIR1 DIR2


Command=`basename $0`

# check arguements
if [ $# -ne 2 ]; then
  \echo 'Number of arguements must be 2' 1>&2
  \echo 'Synopsis: ${Command} DIR1 DIR2' 1>&2
  \exit 1
fi

# get 2 directories.
Dir1=$1
Dir1=${Dir1%/}
if [ ! -d ${Dir1} ] || [ ! -r ${Dir1} ]; then
  \echo "${Dir1} is not directory or not readable" 1>&2
  \exit 1
fi

Dir2=$2
Dir2=${Dir2%/}
if [ ! -d ${Dir2} ] || [ ! -r ${Dir2} ]; then
  \echo "${Dir2} is not directory or not readable" 1>&2
  \exit 1
fi


# get files in Dir1 to find same file in Dir2
for EachPath in `\find ${Dir1}/ -maxdepth 1 -type f`; do
  if [ -L ${EachPath} ]; then
    continue
  fi
  EachFile=`basename ${EachPath}`
  FilesArray=`\find ${Dir2}/ -maxdepth 1 -type f -name ${EachFile}`
  for SameFile in ${FilesArray}; do
    if [ -L ${SameFile} ]; then
      continue
    fi
    echo "${SameFile} is same as ${EachPath}"
  done
done

exit 0

