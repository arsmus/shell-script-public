#!/bin/bash
# 2018 Jul. 11.
# 2018 Jul. 09.
# 2018 Jul. 08.
# server.etutsplus.com/shell-script-substr-count/
# 特定文字が何回現れるかカウント


#### function
####   $OPTION
####      i : 大文字・小文字区別するか
####   $STRING : 検索対象文字列
####   $SEARCH : 検索する文字列
function substr_count() {
    local OPTION
    local STRING
    local SEARCH

    #### オプションなし
    if [ $# -eq 2 ]; then

        STRING=$1
        SEARCH=$2

        echo $STRING | sed "s/\(${SEARCH}\)/\1\n/g" | grep -c -E "${SEARCH}"
        return $?

    #### オプションあり
    elif [ $# -eq 3 ]; then

        OPTION=$1
        STRING=$2
        SEARCH=$3

        [ "$OPTION" != "i" ] && return 101

        echo $STRING | sed "s/\(${SEARCH}\)/\1\n/gi" | grep -c -E -i "${SEARCH}"
        return $?

    #### その他
    else
      echo "function substr_count [i] STRING SEARCH" 1>&2
      echo "  i: ignore-case"
      echo "  STRING: string to be searched"
      echo "  SEARCH: search-string"
      return 99
    fi
}


#####
##### main
#####
Cmd=`basename $0`
if [ $# -lt 2 ] && [ $# -gt 2 ]; then
  echo "${Cmd} [i] STRING SEARCH"
  echo "  i: ignore-case"
  echo "  STRING: string to be searched"
  echo "  SEARCH: search-string"
  exit 97
fi


echo "$*" | grep -q '\$'
if [ $? -eq 0 ]; then
   echo "You can not include '$' in search string." 2>&1
   exit 98
fi

substr_count $*
exit 0

