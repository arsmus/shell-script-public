#!/bin/bash
# 2020 Apr. 20.
# 2020 Apr. 19.
# Ryuichi Hashimoto.

# check commands that you gave on arguement are running.

# usage: ${CMD} COMMAND1 [COMMAND2 COMMAND3...]
# check COMMANDS that you gave on arguements are running.
# partial match of command-name is possible.
# return value:
#   0: any inquireing COMMAND is running
#   1: error
#   2: inquireing COMMANDS are not running


CMD=`basename $0`

function usage() {
cat << EOP
usage: ${CMD} COMMAND1 [COMMAND2 COMMAND3...]
check COMMANDS that you gave on arguement are running.
partial match of command-name is possible.
return value:
  0: any inquireing COMMAND is running
  1: error
  2: inquireing COMMANDS are not running
EOP
}


############
### main ###
############
if [ $# -lt 1 ]
then
  usage 1>&2
  exit 1
fi

## get command-line-arguements
ArguesPipe=''
Argues=($@)
for EachArgue in ${Argues[@]}
do
   ArguesPipe=${ArguesPipe}${EachArgue}\|
done
ArguesPipe=${ArguesPipe%\|}

## get parent process which started this script to avoid to search parent process in ps-command-result 
# 6th col of "ps ax" is parent command
# after 7th cols are arguements of parent command
HasParent="no"
NumColPs=`ps ax | grep $PPID | grep -v 'grep' | awk '$1 == '"${PPID}"' {print NF}'`
if [ ${NumColPs} -gt 5 ]
then
  ## case of some parent command started this script
  # get parant command & arguements to store to array
  #   String in ParentStr of result of awk is separated by LF not space.
  HasParent="yes"
  ParentStr=`ps ax | grep $PPID | grep -v 'grep' | awk '{for (i = 6; i <= NF; i++ ) print $i }'`
  # replace LF to space in ParentStr
  ParentStr=`echo $ParentStr | sed 's/\n/ /g'`
  ParentStrArray=($ParentStr)
fi

if [ ${HasParent} = "yes" ]
then
  ps ax -f | grep -v grep | grep -v ${CMD} | grep -E ${ArguesPipe} | grep -v ${ParentStrArray[0]} > /dev/null 2>&1
else
  ps ax -f | grep -v grep | grep -v ${CMD} | grep -E ${ArguesPipe} > /dev/null 2>&1
fi
ResultCode=$?
if [ ${ResultCode} -ne 0 ];then
   # not matched to any command
   \exit 2
fi
# matched to any command
\exit 0

