#!/bin/bash

# 2022 Jul. 17.
# Ryuichi Hashimoto.

# This is a copy from https://qiita.com/tk_01/items/c1bbd844f38575f84f7b

PROGNAME=${0##*/}
state=$1
action=$2

function log {
      logger -i -t "$PROGNAME" "$*"
}

log "Running $action $state"

if [[ $state == post ]]; then
    modprobe -r r8169 \
      && log "Removed r8169" \
      && modprobe -i r8169 \
      && log "Inserted r8169"
fi
exit 0

