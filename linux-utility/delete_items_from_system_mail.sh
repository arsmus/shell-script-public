#!/bin/bash
# 2021 Dec. 31.
# Ryuichi Hashimoto

# Delete mails of mon dd hh:51:ss 2021 from system.
# New file is ${MailDir}/${MailUser}.new


#MailUser=ryuichi
MailUser=mbox
#MailDir=/var/mail
MailDir=${HOME}
MailDir=${MailDir%/}
MailSender='ryuichi@rokkosan.sytes.net'

cp ${MailDir}/${MailUser} /tmp/tmpMailFile
cp ${MailDir}/${MailUser} ${MailDir}/${MailUser}.org

while true; do
  # search first head-line of the item to delete and get the line number of the line.
  DelLineNumBegin=0
  DelLineNumBegin=$( cat /tmp/tmpMailFile | grep -Pn -m 1 "From ${MailSender} .* [0-9][0-9]:51:[0-9][0-9] 20[0-9][0-9]$" | sed -E s/:.*// )
  if [ $[DelLineNumBegin] -lt 1 ]; then
    break
  fi

  # search head-line of second item in all items and get its line number .
  DelLineNumEnd=0
  LineNumAry=( $( cat /tmp/tmpMailFile | grep -Pn "From ${MailSender} .* [0-9][0-9]:[0-9][0-9]:[0-9][0-9] 20[0-9][0-9]$" | sed -E s/:.*// ) )
  for LineNum in ${LineNumAry[@]}; do
    if [ ${DelLineNumBegin} -lt ${LineNum} ]; then
      DelLineNumEnd=$(expr ${LineNum} - 1)
      break
    fi
  done

  if [ ${DelLineNumEnd} -lt 1 ]; then
    # delete item
    cat /tmp/tmpMailFile | sed -e "${DelLineNumBegin},$d" > /tmp/tmpMailFile
    break
  fi

  # delete item
  cat /tmp/tmpMailFile | sed -e "${DelLineNumBegin},${DelLineNumEnd}d" > /tmp/tmpMailFile
done

cp /tmp/tmpMailFile ${MailDir}/${MailUser}.new

exit 0

