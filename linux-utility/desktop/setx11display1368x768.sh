#!/bin/sh

# 2014 Apr. 20.
# Ryuichi Hashimoto.

# set display-resolution 1368x768

xrandr --newmode "1368x768_60.00" 85.25  1368 1440 1576 1784  768 771 781 798 -hsync +vsync
xrandr --addmode VGA1 1368x768_60.00
xrandr --output VGA1 --mode 1368x768_60.00
