#!/bin/bash
# 2020 Apr. 19.
# Ryuichi Hashimoto.

# run isAnyRunning.sh 5 times every 1 second.
# check COMMANDS that you gave on arguements are running.
# partial match of command-name is possible.
# return value:
#   0: any inquireing COMMAND is running
#   1: error
#   2: inquireing COMMANDS are not running
#
# return 0 if script return 0 at least 1 time in loop.


CmdLoop=`basename $0`
export CmdLoop

# loop-count
NumLoop=5

# interval seconds
SleepSec=1

# set default-retrun-code.
Result=0


function usage() {
cat << EOP
usage: ${CmdLoop} COMMAND1 [COMMAND2 COMMAND3...]
run isAnyRunning.sh 5 times every 1 second.
check COMMANDS that you gave on arguements are running.
partial match of command-name is possible.
return value:
  0: any inquireing COMMAND is running
  1: error
  2: inquireing COMMANDS are not running

return 0 if script return 0 at least 1 time in loop.
EOP
}


############
### main ###
############
if [ $# -lt 1 ]
then
  usage 1>&2
  exit 1
fi

# get pipe-connected-arguements-string
ArguesPipe=''
Argues=($@)
for EachArgue in ${Argues[@]}
do
   ArguesPipe=${ArguesPipe}${EachArgue}\|
done
ArguesPipe=${ArguesPipe%\|}

IsRunning="isAnyRunning.sh ${ArguesPipe}"

while [ $NumLoop -gt 0 ]; do
  ${IsRunning}
  ResultCode=$?
  if [ 0 -eq $ResultCode ]; then
    # when some commands are running
    exit 0
  fi
  sleep 1
  NumLoop=$(($NumLoop - 1))
done
exit $ResultCode

