#!/bin/sh
# 2015 Apr. 28.
# NumLock On on Terminal

for tty in /dev/tty[1-9]*; do
	setleds -D +num < $tty  # NumLock(num) ON(+)
done

