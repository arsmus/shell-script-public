#!/bin/bash
# 2021 Feb. 21.
# Ryuichi Hashimoto.

# Reduce mbox (mail from system)


Cmd=${0##/}
User=$(whoami)
MaxMboxSize=5
 # unit of MaxMboxSize in above line: MB
MaxMboxSize=$(expr $MaxMboxSize \* 1024 \* 1024)
 # unit of MaxMboxSize in above line: byte

################
### function ###
################
function reduceMbox() {
  local MaxMboxSize=$1
  local MboxPath="$2"

  if [ ! -f "$MboxPath" ]; then
    return 1
  fi

  MboxSize=$(wc -c "$MboxPath" | cut -d' ' -f1)
  if [ -w "$MboxPath" ] && [ $MboxSize -gt $MaxMboxSize ]; then
    # take a note of owner, group, permission of mbox
    OwnerMbox=$(ls -l "$MboxPath" | cut -d ' ' -f 3)
    GroupMbox=$(ls -l "$MboxPath" | cut -d ' ' -f 4)
    PermissionMbox=$(stat -c %a "$MboxPath")
    # reduce mbox
    CountLine=$(wc -l "$MboxPath" | cut -d ' ' -f 1)
    HalfNumLine=$(( $CountLine / 2))
    cp "$MboxPath" "$MboxPath".bak
    cat "$MboxPath".bak | sed -e 1,${HalfNumLine}d > "$MboxPath"
    # reset owner, group, permission of mbox
    chmod $PermissionMbox "$MboxPath"
    chowner "${OwnerMbox}":"${GroupMbox}"
    return 0
  fi
  return 1
}

################
##### main #####
################
if [ "$User" = 'root' ]; then
  # In case that User is root, reduce mboxes of all users
  UserHomeList=$(cat /etc/passwd | awk -F ":" '{print $6}')
  while read -d $'\n' EachUserHome; do
    MboxPath="${EachUserHome}/mbox"
    reduceMbox $MaxMboxSize "$MboxPath"
  done < <(echo "$UserHomeList")
else
  MboxPath="${HOME}/mbox"
  reduceMbox $MaxMboxSize "$MboxPath"
fi
exit 0

