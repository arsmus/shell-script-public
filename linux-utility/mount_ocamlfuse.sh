#!/bin/bash
# 2021 Apr. 29.
# 2021 Feb. 26.
# Ryuichi Hashimoto.

# mount ocamlfuse on start.


Cmd=${0##*/}

################
### function ###
################
function usage() {
cat << EOS
Usage: $Cmd GOOGLE/DRIVE/DIR/FOR/OCAMLFUSE
EOS
}
################
##### main #####
################
# check arguement
if [ $# -ne 1 ]; then
  echo "Error. Illegal number of arguements." 1>&2
  usage
  exit 1
fi

OcamlfuseDir="$1"
OcamlfuseDir="${OcamlfuseDir%/}"
if [ ! -d "$OcamlfuseDir" ]; then
  echo "$OcamlfuseDir does not exist." 1>&2
  usage
  exit 1
fi

google-drive-ocamlfuse "$OcamlfuseDir"
exit 0

