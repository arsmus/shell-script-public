#!/bin/bash

# 2015 Nov. 22.

# output fullpath of argument file to standard-out.


cmdname=`basename $0`

if [ $# -ne 1 ]; then
  echo "${cmdname} FILENAME" >&2
  echo 'Output fullpath of argument file to standard-out.'
  exit 1
fi

f=$1
if [ -d "$f" ]; then
  base=""
  dir="$f"
else
  base="/$(basename "$f")"
  dir=$(dirname "$f")
fi
dir=$(cd "$dir" && /bin/pwd)
echo "$dir$base"

