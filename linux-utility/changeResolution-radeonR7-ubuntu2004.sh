#!/bin/bash
# 2021 Apr. 19.
# 2021 Apr. 18.
# Ryuichi Hashimoto.

# Change screen-resolution on Ubuntu 20.04


## my screen-resolution
#MyXResolution=1360
#MyYResolution=768

## get screen
#Screen=$(xrandr | grep ' connected ' | grep -Po '^[\w-]+')

## get modeline
#SearchStr=\""${MyXResolution}x${MyYResolution}"
#MyModeline=$(cvt $MyXResolution $MyYResolution | grep Modeline | grep -Po "${SearchStr}.*$")

## get mode ID
#MyModeID=$(cvt $MyXResolution $MyYResolution | grep Modeline | grep -Po "${SearchStr}.*\"" | sed 's/\"//g')

## set new mode to X window
#xrandr --newmode $MyModeline

## add new mode to screen
#xrandr --addmode $Screen $MyModeID

## change screen-resolution
#xrandr --output $Screen --mode $MyModeID

### Failed to run above code.
### Please run below code.

# set new mode to X window
xrandr --newmode "1368x768_60.00"   85.25  1368 1440 1576 1784  768 771 781 798 -hsync +vsync

# add new mode to screen
xrandr --addmode VGA-0 1368x768_60.00

# change screen-resolution
xrandr --output VGA-0 --mode 1368x768_60.00

exit 0

