#!/bin/bash
# 2018 Nov. 11.
# 2016 Sep. 22.
# 2016 Sep. 19.
# Ryuichi Hashimoto.

# This program kills the jobs  relevent to "grive" which keep running for too long time.


# set variables
griveScript='[ \/]grive4cron.sh( +.*$|$)'
griveCommand='[ \/]grive( +.*$|$)'
agoHours='3'

# Quit program, if now-time is between 0:00 and ${agoHours}hour.
#   because grive would be working right.
nowHour=`\date +%-H`
griveTime=`\ps ax -f | \grep -E "${griveScript}" | \grep -v 'grep' | \awk '{print $5}' | \awk 'NR==1' | \grep ':'`
  # Get time of process-started.
timeResult="$?"
  # result value
  #     0: the process started today
  #     Not 0: the process started another day.

if [ "$timeResult" -ne 0 ]; then
# in the case of another day started of process
  if [ "$nowHour" -lt "$agoHours" ]; then
    \echo 'Grive may run now.' >&2
    \echo 'Quit.' >&2
    \exit 0
  fi
else
# in the case of process started today
  griveHour=$( \echo "$griveTime" | \sed 's/\(^[0-9]\+\)\:.*/\1/' )
  if [ $(( $nowHour - $griveHour )) -lt "$agoHour" ]; then
    \echo 'Grive may run now.' >&2
    \echo 'Quit.' >&2
  fi
fi

# search running grive.
\ps ax -f | \grep -E "${griveScript}" | \grep -vq 'grep'
runGrive="$?"
if [ "$runGrive" -ne 0 ]; then
  \echo 'Grive is not running.' >&2
  \exit 0
fi

# get process numbers of grive-scripts
procNums=($(\ps ax -f | \grep -E "${griveScript}" | \grep -v 'grep' | \awk '{print $2}'))

\echo "process numbers of grive4cron.sh : "${procNums[*]}" " >&2

# get process number of grive-command
for procNum in "${procNums[@]}"
do

  \echo "checking grive4cron-process "$procNum" " >&2

  # get substancial grive process
  \unset griveComNum
  griveComNum=`\ps ax -f | \grep "$procNum" | \grep -E "${griveCommand}" | \grep -v 'grep' | \awk '$3=='"$procNum"'{print $2}'`
  if [ ! -z "${griveComNum:+x}" ]; then
    \break
  fi
done

\echo "process number of grive command : "$griveComNum" " >&2

# in the case of another day started of process, kill grive
if [ "$timeResult" -ne 0 ]; then
  # kill processes
  \kill "${griveComNum}"
  \echo "killed processes : "${griveComNum}"" >&2
  for procNum in "${procNums[@]}"
  do
    \kill "${procNum}"
    \echo "killed processes : "${procNum}"" >&2
  done
else
  # in the case of process started today
  diffTime=$(\expr `date +%s` - `date -d "$griveTime" +%s` )
    # `date +%s` : now time value

  # kill grive-processes which started more than $agoHours hours ago.
  if [ $(( ${diffTime} / 3600 )) -gt ${agoHours} ]; then
    # kill processes
    \kill "${griveComNum}"
    \echo "killed processes : "${griveComNum}"" >&2
    for procNum in "${procNums[@]}"
    do
      kill "${procNum}"
      \echo "killed processes : "${procNum}"" >&2
    done
  fi
fi

exit 0

