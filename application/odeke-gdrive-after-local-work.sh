#!/bin/sh

# 2015 Apr. 26.
# Ryuichi Hashimoto.

# You should run this script after you have used odeke-google-drive-files.
# This script pushes local files to cloud-storage.

# Warning
# A file that exists in local but does not exist in remote
# will be copied to remote,
# even if the same name file in remote was deleted recently.

# check arguement
if [ -z $1 ]; then
  base=`basename $0`
  echo 'Error. No arguement for directory.' >&2
  echo "${base} ODEM-GOOGLE-DRIVE-DIR" >&2
  exit
fi

# get odeke-google-drive-directory
DIR=$1

if [ ! -d $DIR ]; then
  echo "${DIR} : not found." >&2
  exit
fi

cd $DIR
drive push
echo "'drive push' finished." >&2

