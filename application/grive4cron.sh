#!/bin/sh
#
# 2020 Apr. 13.
# 2020 Apr. 12.
# 2018 Nov. 11.
# 2016 Nov. 19.
# 2016 Sep. 17.
# 2014 Aug. 03.
# Ryuichi Hashimoto.

# syncronize google-drive
#   check LAN.
#   check grive running.
#   run grive if not running.


# log file
STDOUTLOG="${HOME}/var/log/grive-stdout.log"
STDERRLOG="${HOME}/var/log/grive-stderr.log"
DELETELOG="${HOME}/var/log/grive-delete.log"
SCRIPTLOG="${HOME}/var/log/grive-rerun.log"

# Set IP-address to which test-ICMP-packet is sent.
DEST_ADDRESS='www.yahoo.co.jp'

# Set grive dir in local host
# GRIVEDIR="${HOME}/YOUR-GRIVE-DIR"
GRIVEDIR="${HOME}/grive-my"

##########################
# function               #
#   check LAN connection #
##########################
check_lan() {
  mes_stdout=`\ping -c 1 $DEST_ADDRESS`
  \echo $mes_stdout | \grep 'received, 0% packet loss' | \grep -q -v 'grep'
  if [ $? -eq 0 ]; then
	  \echo 'LAN connected.' >> ${SCRIPTLOG}
	  \return 1
  else
	  \echo 'LAN not connected.' >> ${SCRIPTLOG}
	  \return 2
  fi
}


##################
## main routine ##
## main routine ##
##################
\echo >> ${SCRIPTLOG}
LANG=C \date >> ${SCRIPTLOG}

# if other same processes running, quit.
cmd=`\basename $0`
numcmd=`\ps ax | \grep $cmd | \grep -v "\-c ${cmd}" | \grep -v -c 'grep'`
numcmd=`\expr $numcmd - 1`
if [ ${numcmd} -gt 1 ]; then
  \echo "Another ${cmd} is running." >> ${SCRIPTLOG}
  \exit 1
fi

# check and connect internet
check_lan_result=0
cnt_try_connect=0
until [ $check_lan_result -eq 1 ]
do
  if [ ${cnt_try_connect} -gt 3 ]; then
    \echo 'No internet connection. Abort.' >> ${SCRIPTLOG}
    \exit 1
  fi
  check_lan
  check_lan_result=$?

  if [ ${check_lan_result} -ne 1 ]; then
    cnt_try_connect=`\expr $cnt_try_connect + 1`
  fi
done

# check/run grive.
\ps ax | \grep 'grive\( \|$\)' | \grep -q -v 'grep'
if [ $? -ne 0 ]; then
  \echo 'Starting grive.' >> ${SCRIPTLOG}
  \cd ${GRIVEDIR}

  # save existing logs
  if [ -f ${STDERRLOG} ]; then
    \echo >> ${DELETELOG}
    LANG=C \date >> ${DELETELOG}
    \grep 'delet' ${STDERRLOG} >> ${DELETELOG}
    # mv ${STDERRLOG} ${STDERRLOG}.bak
  fi

  if [ -f ${STDOUTLOG} ]; then
    \mv ${STDOUTLOG} ${STDOUTLOG}.bak
  fi

  # run grive and output new logs
  LANG=C \date > ${STDOUTLOG}
  LANG=C \date >> ${STDERRLOG}
  \grive 1>> ${STDOUTLOG} 2>> ${STDERRLOG}

else
  \echo 'Another Grive already running.' >> ${SCRIPTLOG}
fi

\exit 0

