#!/bin/bash
# 2021 Mar. 14.
# 2018 Dec. 29.
# 2018 Dec. 03.
# Ryuichi Hashimoto.

# Dump mysql-db to file, 1 file to 1 database
# by reading password from ~/.my.cnf
# Connection between server and client must be IP-socket.

# Usage: ${cmdname} DestDir
# Dump mysql-db to file, 1 file to 1 database
# by reading password from ~/.my.cnf
# Connection between server and client must be IP-socket.
# Output: DestDir/mysql-DATABASE.dump


############
### main ###
############
# set debug mode
# debug="true"
debug="false"

MysqlUser=admin
cmdname=${0##*/}

# check command-line option
if [ $# -ne 1 ]; then 
  \echo "Usage: ${cmdname} DestDir" 1>&2
  \echo "Dump mysql-db to file, 1 file to 1 database" 1>&2
  \echo "by reading password from ~/.my.cnf" 1>&2
  \echo "Connection between server and client must be IP-socket." 1>&2
  \echo "Output: DestDir/mysql-DATABASE.dump" 1>&2
  \exit 1
fi

DestDir=$1
if [ ! -w ${DestDir} ]; then
  \echo "${DestDir} does not exist or is not writable."
  exit 1
fi
DestDir=${DestDir%/}

# set databaseNames in array:DBs
AllDBs=`\echo 'show databases' | \mysql`
DBs=()
i=0
for Db in ${AllDBs[@]}; do
  if [ "Database" != ${Db} ] && [ "information_schema" != ${Db} ] && [ "mysql" != ${Db} ] && [ "performance_schema" != ${Db} ] && [ "sys" != ${Db} ]; then
    DBs[i]=${Db}
    let i++
  fi
done

# dump database
for Db in ${DBs[@]}; do
  if [ -f ${DestDir}/mysql-${Db}.dump ]; then
    mv ${DestDir}/mysql-${Db}.dump ${DestDir}/mysql-${Db}.dump.bak
  fi
  \mysqldump -u ${MysqlUser} ${Db} > ${DestDir}/mysql-${Db}.dump
done

exit 0

