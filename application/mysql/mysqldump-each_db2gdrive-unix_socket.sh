#!/bin/bash
# 2021 Dec. 29.
# 2021 Jun. 06.
# 2021 Jun. 05.
# Ryuichi Hashimoto.

# Dump mysql-db to google-drive


Cmd="${0##*/}"
WorkDir="/tmp/mysqldump-each-db"
Gdrive="rc-rasumonly:backup/database/mysql"
Suffix=pc12-xubuntu2004

# check programs depended on
which mysqldump_each-db2each-file-unix_socket.sh 1> /dev/null 2> /dev/null
RetValue=$?
if [ $? -ne 0 ]; then
  echo "Please install mysqldump_each-db2each-file-unix_socket.sh" 1>&2
  exit 1
fi

# check Workdir
WorkDir="${WorkDir%/}"
if [ ! -d "${WorkDir}"  ]; then
  mkdir -p "${WorkDir}"
fi

RetStr=$(rclone ls "${Gdrive}/" --include "mysql-${Suffix}*.dump" 2> /dev/null)
if [ ${#RetStr} -gt 10 ]; then
  rclone move "${Gdrive}/" "${WorkDir}/" --include "mysql-${Suffix}*.dump"
  MvFname=$(find  "${WorkDir}/" -name "mysql-${Suffix}"*.dump)

  while read -d $'\0' MvFname; do
    mv "${MvFname}" "${MvFname}".old
  done < <(find "${WorkDir}/" -type f -name "mysql-${Suffix}"*.dump | sed -z -e 's%\n%\x0%g')

fi
mysqldump_each-db2each-file-unix_socket.sh -s "${Suffix}" "${WorkDir}"
rclone copy "${WorkDir}/" ${Gdrive}/

TmpFile="${HOME}/tmp/tmpMysqlDump.txt"
NowDate=$(date)
echo "$NowDate ${WorkDir}/ " >> "${TmpFile}"
ls -ltr "${WorkDir}"/ >> "${TmpFile}"
echo "" >> "${TmpFile}"

rm "${WorkDir}"/*
exit 0

