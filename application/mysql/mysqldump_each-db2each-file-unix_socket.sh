#!/bin/bash
# 2021 Jun. 2.
# 2021 May 31.
# 2021 Feb. 27.
# 2020 Apr. 15.
# 2018 Dec. 29.
# 2018 Dec. 03.
# Ryuichi Hashimoto.

# Dump mysql-db to file, 1 file to 1 database.
# You have to give the user a privilege to "sudo mysqldump" without password by setting /etc/sudoers.
#   (sample of setting in /etc/sudoers)
#   foouser ALL=(root) NOPASSWD: /usr/bin/mysqldump

# usage: ${cmdname} [-s Suffix] DestDir
# Output: DestDir/mysql-[Suffix]-DATABASE.dump


cmdname=${0##*/}

################
### function ###
################
function usage() {
cat << EOS
Dump mysql-db to file, 1 file to 1 database.
You have to give the user a privilege to "sudo mysqldump" without password by setting /etc/sudoers.
  foouser ALL=(root) NOPASSWD: /usr/bin/mysqldump

usage: ${cmdname} [-s Suffix] DestDir
Output: DestDir/mysql-[Suffix]-DATABASE.dump
EOS
return 0
}

################
##### main #####
################
ArgNum=$#
# check command-line-arguements
if [ $ArgNum -ne 1 -a $ArgNum -ne 3 ]; then 
  \echo "Number of arguements is not proper." 1>&2
  \echo "" 1>&2
  usage 1>&2
  \exit 1
fi

# get command-line-arguements
ArgVal="$1"
shift
if [ $ArgVal = '-s' ]; then
  Suffix="$1"
  shift
  if [ $# -ne 1 ]; then
    \echo "Number of arguements is not proper." 1>&2
    \echo "" 1>&2
    usage 1>&2
    \exit 1
  fi
  DestDir="$1"
else
  if [ $# -ne 0 ]; then
    \echo "Number of arguements is not proper." 1>&2
    \echo "" 1>&2
    usage 1>&2
    \exit 1
  fi
  DestDir="$1"
fi

DestDir="${DestDir%/}"

# to access ocamlfuse-dir
for Cnt in `seq 1 5`; do
  \ls "$DestDir" 1> /dev/null 2> /dev/null
  \sleep 1s
done

if [ ! -d "${DestDir}" ]; then
  \echo "${DestDir} does not exist." 1>&2
  \echo "" 1>&2
  usage 1>&2
  \exit 1
fi
if [ ! -w "${DestDir}" ]; then
  \echo "${DestDir} is not writable." 1>&2
  \echo "" 1>&2
  usage 1>&2
  \exit 1
fi

# set databaseNames in array:DBs
AllDBs=$(\echo 'show databases' | \sudo \mysql)
DBs=()
i=0
for Db in ${AllDBs[@]}; do
  if [ "Database" != "${Db}" ] && [ "information_schema" != "${Db}" ] && [ "mysql" != "${Db}" ] && [ "performance_schema" != "${Db}" ] && [ "sys" != "${Db}" ]; then
    DBs[i]="${Db}"
    let i++
  fi
done

# dump database
for Db in ${DBs[@]}; do
  # backup dump-file
  if [ -f "${DestDir}/mysql-${Suffix}-${Db}.dump" ]; then
    \mv "${DestDir}/mysql-${Suffix}-${Db}.dump" "${DestDir}/mysql-${Suffix}-${Db}.dump.old"
  fi

  # get dump-file
  sudo mysqldump "${Db}" > "${DestDir}/mysql-${Suffix}-${Db}.dump"
done

\exit 0

