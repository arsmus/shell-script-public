#!/bin/bash
# 2020 Apr. 15.
# 2018 Dec. 29.
# 2018 Dec. 03.
# 2018 Nov. 23.
# 2015 Nov. 22.
# 2015 Nov. 21.
# Ryuichi Hashimoto.

# Dump all mysql-db data to standard-out by reading password from ~/.my.cnf


cmdname=`\basename $0`

# command-line option
if [ $# -gt 0 ]; then 
  \echo "usage: ${cmdname}"
  \exit 1
fi

\mysqldump --single-transaction --all-databases

