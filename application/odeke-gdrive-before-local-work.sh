#!/bin/sh

# 2015 Apr. 26.
# Ryuichi Hashimoto.

# You should run this script before you use odeke-google-drive-files.
# This script pulls cloud-storage-files to local.

# Warning
# A file that exist in remote-cloud but not exist in local
# will be copied to local,
# even if the same name file in local was deleted recently.

# check arguement
if [ -z $1 ]; then
  base=`basename $0`
  echo 'Error. No arguement for directory.' >&2
  echo "${base} ODEM-GOOGLE-DRIVE-DIR" >&2
  exit
fi

# get odeke-google-drive-directory
DIR=$1

if [ ! -d $DIR ]; then
  echo "${DIR} : not found." >&2
  exit
fi

cd $DIR
drive pull
echo "'drive pull' finished." >&2

