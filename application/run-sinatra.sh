#!/bin/sh -e

# 2021 Feb. 13.
# 2018 Dec. 31.
# 2018 Oct. 13.
# 2016 Feb. 09.
# Ryuichi Hashimoto.

# Start daemon to list recorded-TV-files on web-browser.


cmdname=${0##*/}

if [ $# -gt 0 ]; then
  echo "Usage"
  echo "  This script runs as below."
  echo "    Sets current directory to ${HOME}/sinatra"
  echo "    Runs sinatra-script."
  echo "      # bundle exec app.rb -e production"
  echo "    You can get TV-files-list at http://HOST:4567/tv."
  echo "Bye."
  exit 1
fi

cd ${HOME}/sinatra

# bundle exec ruby ./app.rb -e production > ./app-rb-out.log  2> ./app-rb-err.log &
bundle exec ruby ./app.rb -e production

exit 0

