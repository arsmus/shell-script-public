#!/bin/bash
#
######################################
#
# 2009 Jun. 10.
# Ryuichi Hashimoto.
#
# Restart Networking for emobile-d12hw
#
######################################


#####################
#
# check IP connection
#
#####################
ifconfig | grep -A1 ppp0 | grep -Eq 'inet.*:[0-9]+.[0-9]+.[0-9]+.[0-9]+'
if [ $? -eq 0 ]; then
	echo 'ppp0 exists.'
	exit 0
fi
echo "ppp0 doesn't exist."

####################
#
# Restart networking
#
####################
/etc/init.d/networking restart

