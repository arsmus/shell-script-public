#!/bin/bash
# 2020 May  23.
# 2020 May  18.
# 2020 May  17.
# 2020 Apr. 12.
# 2020 Apr. 11.
# ryuichi Hashimoto

# Disconnect VPN Gate server connection

# You have to be a root user.


Cmd=`basename $0`

# usage
function usage() {
  cat << EOP
usage:
    $ su -
    ${Cmd}
EOP
}

# check host is communicating with outside of LAN
which isCommunicatingOutLan.sh > /dev/null 2>&1
if [ $? -ne 0 ]
then
  echo "isCommunicatingOutLan.sh does not exist in command-PATH."
  echo "Abort."
  exit 1
fi

UsingInternet="yes"
while [ "yes" = ${UsingInternet} ]
do
  isCommunicatingOutLan.sh > /dev/null 2>&1
  if [ $? -ne 1 ]
  then
    # Host is communicating with outside through LAN.
    # Changing packet-route stops existing communication through LAN.
    sleep 1m
  else
    UsingInternet="no"
  fi
done

# check vpnclient is running
ps ax -f | grep vpnclient | grep -v grep > /dev/null 2>&1
if [ $? -ne 0 ]
then
  echo "vpnclient is not running."
  echo "Abort."
  exit 1
fi

# move to VpnClient-command-dir
PathVpnClientCom=`find /usr/ -type f -name vpnclient`
DirVpnClient=`dirname ${PathVpnClientCom}`
cd $DirVpnClient

# check packet-route-setting
VpnNIC=`./vpncmd localhost /CLIENT /CMD AccountList | grep LAN | sed -e "s/^.*|//"`
LinuxVirNIC=`ip l | grep -Po -m 1 "\S+${VpnNIC}"`
if [ $? -ne 0 ]
then
  echo "Virtual NIC on linux does not exist."
  echo "Abort."
  exit 1
fi
if [ $? -ne 0 ]
then
  echo "Default packet-route does not pass through virtual NIC."
  echo "Abort."
  exit 1
fi

# resume packet-route
RealNIC=`route -n | grep -m 1 '192.168.' | sed 's/[\t ]\+/\t/g' | cut -f 8`
RealNIC=`route -n | awk '{if ($2  ~ /^192.168./) {print}}' | sed 's/[\t ]\+/\t/g' | cut -f 8`
RealGWayAddress=`route -n | grep ${RealNIC} | sed 's/[\t ]\+/\t/g' | cut -f 2 | grep -m 1 -Po '192.168.[0-9]+.[0-9]+'`
ServerIP=`route -n | grep ${RealNIC} | sed 's/[\t ]\+/ /g' | awk -v adrs=${RealGWayAddress} '{if ($2 == adrs){print $1}}'`
route del default
route add default gw ${RealGWayAddress} dev ${RealNIC}
route del ${ServerIP} 

# disconnect VPN Gate
AccountName=`./vpncmd localhost /CLIENT /CMD AccountList | grep 接続設定名 | sed -e "s/^.*|//"`
./vpncmd localhost /CLIENT /CMD AccountDisconnect ${AccountName}
./vpnclient stop

# kill dhclient
while :
do
  NumPsLines=`ps ax -f | grep "dhclient .*${LinuxVirNIC}" | grep -v grep | wc -l`
  if [ $NumPsLines -lt 1 ]
  then
    break
  fi
  PidDhclient=`ps ax -f | grep -v grep | grep -m1 "dhclient .*${LinuxVirNIC}" | sed -n '1,1p' | sed 's/[\t ]\+/\t/g' | cut -f 2`
  kill -9 ${PidDhclient}
done

route -n 1>&2
ps ax -f | grep -v grep | grep vpn 1>&2

exit 0

