#!/bin/bash
# 2020 May 16.
# Ryuichi Hashimoto.

# outut a list of VPN-Gate-servers in Japan but not Daiyuu Nobori to standard-out.


Cmd=`basename $0`

function usage() {
cat << EOP
usage: $Cmd

Outut a list of VPN-Gate-servers in Japan but not Daiyuu Nobori to standard-out.
EOP
}


##########
## main ##
##########
# check number of arguements
if [ $# -gt 0 ]
then
  echo "No arguements required." 1>&2
  usage 1>&2
  exit 1
fi

# get VPN Gate server address 
# echo $ServerIP
wget http://www.vpngate.net/api/iphone/ -O - 2> /dev/null | grep JP | grep -v Daiyuu | sort -rn -k 5 -t "," | cut -d , -f 1-2 | awk 'BEGIN { FS = "," } ;{print $1".opengw.net,"$2}'
exit 0

