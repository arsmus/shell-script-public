#!/bin/sh

# 2014 Aug. 31.
# Ryuichi Hashimoto.

# Connect LAN with E-Mobile.

# IP-address that is sent test-ICMP-packet.
#   192.168.13.1 : E-MOBILE GL06P LAN-side address.
DEST_ADDRESS='192.168.13.1'
# DEST_ADDRESS='www.yahoo.co.jp'

# LANID for NetworkManager
LANID='GL06P-F80113610662'


# function #
# check LAN connection function
check_lan() {
  mes_stdout=`ping -c 1 $DEST_ADDRESS`
  echo $mes_stdout | grep 'received, 0% packet loss' | grep -q -v 'grep'
  if [ $? -eq 0 ]; then
    echo 'LAN connected.' 1>&2
    return 1
  else
    echo 'LAN not connected.' 1>&2
    return 2
  fi
}

## main routine ##
## main routine ##

# check/connect LAN
check_lan_result=0
cnt_try_connect=0
until [ $check_lan_result -eq 1 ]
do
  if [ ${cnt_try_connect} -gt 3 ]; then
    echo 'No internet connection. Abort.' 1>&2
    exit 1
  fi
  check_lan
  check_lan_result=$?

  if [ ${check_lan_result} -ne 1 ]; then
    # start connecting LAN
    echo 'Start connecting LAN.' 1>&2
    nmcli con up id $LANID
    cnt_try_connect=`expr $cnt_try_connect + 1`
  fi
done

