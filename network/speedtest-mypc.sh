#!/bin/bash
# 2017 Oct. 14.
# 2017 Sep. 17.
# 2017 Sep. 03.
# 2017 Sep. 02.
# 2017 Aug. 19.
# Ryuichi Hashimoto.

# return result of your internet-speed-test
#   Output net-speed of download in Mbit/s to standard-out.
#   Return Download-speed(Mbit/s) as result-code.
#   Return 255(Mbit/s) as result-code if the speed is faster than 254Mbit/sec.
#   Return 0 if the speed is not gotten.

OSAKA='6476'
IKEDA='8832'
NERIMA='12511'
# SERVER=${OSAKA}
# SERVER=${IKEDA}
SERVER=${NERIMA}

speedResult=`speedtest &`
sleep 255s
ps x | grep '/usr/bin/speedtest' | grep -v 'grep' 1>&2
ps x | grep '/usr/bin/speedtest' | grep -q -v 'grep'
if [ $? -eq 0 ] ; then
  # slow speed
  pids=`ps x | grep '/usr/bin/speedtest ' | grep -v 'grep' | awk '{print $1}'`
  kill -9 $pids
  echo 'killed speedtest-process' 1>&2
  echo 'Maybe slow' 1>&2
  echo "Download speed : unknown"
  exit 1
fi

# check result of speedtest
speedReal=`echo ${speedResult} | grep -Eo "Download: [[:digit:]]+\.*[[:digit:]]* Mbit/s" | grep -Eo "[[:digit:]]+\.*[[:digit:]]*" | head -n 1 `

if [ ${speedReal}z = 'z' ]; then
  # slow speed
  echo 'no speedtest result' 1>&2
  echo 'Maybe slow' 1>&2
  echo "Download speed : unknown"
  exit 1
fi

speedInt=`echo "${speedReal} * 1000" | bc | grep -Eo "[[:digit:]]+" | head -n 1`
echo "${speedInt}Kbit/s"
exit 0

