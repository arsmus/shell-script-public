#!/bin/bash
#
# 2020 Apr. 12.
# 2020 Apr. 11.
# Ryuichi Hashimoto.
#
# Check if host is communicating with outside of LAN.
#   retrun code
#     0: host is using internet
#     1: host is not using internet


# List up programs which access to internet and with which you fear to break internet-communications.
# Connecting to VPN-gate-server breaks existing internet access.
#   CMDS='INETPROGRAM1|INETPROGRAM2|INETPROGRAM3'
CMDS='rec_radiko.sh|rec-FMO_TFM_HFM-55min-in_bigdata_radio.sh|rec-listenRadio.sh|rec-nhk_fm542min-in-bigdata_radio.sh|rec_nhk_radio-50sec_sleep.sh|rec_nhk_radio.sh|grive'

function isCommunicate() {
  ps ax -f | grep -E $CMDS | grep -v grep 
  if [ $? -ne 0 ];then
     # nothing uses internet
     return 1
  fi
  # something uses internet
  return 0
}


############
### main ###
############

# set loop-count
NumLoop=5

# set default-retrun-code.
Result=1

while [ $NumLoop -gt 0 ]; do
  isCommunicate
    # return code of isCommunicate
    #   0: host is using internet
    #   1: host is not using internet

  if [ 0 -eq $? ]; then
    # when some commands are running
    Result=0
  fi
  sleep 1
  NumLoop=$(($NumLoop - 1))
done
exit $Result

