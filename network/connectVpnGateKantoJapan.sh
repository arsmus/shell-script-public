#!/bin/bash
# 2020 May  23.
# 2020 May  16.
# 2020 May  03.
# 2020 Apr. 12.
# 2020 Apr. 11.
# 2020 Apr. 06.
# Ryuichi Hashimoto

# connect to VPN-server of Tsukuba in Japan

# You have to be a root user.

# flow
# 1) get VPN Gate server address of Daiyuu in Japan 
# 2) check host is communicating with outside of LAN
# 3) add packet-route to server to routing table.
# 4) start vpnclient.
# 5) change VPN-server's address of account.
# 6) connect account.
# 7) give ip-address to virtual NIC.
# 8) change default route to VPN-Gate in routing table.
# You can enjoy VPN-Gate-server


Command=`basename $0` 

# usage
function usage() {
cat << EOP
usage:
    $ su -
    # ${Command} 
flow:
1) get VPN Gate server address of Daiyuu in Japan 
2) check host is communicating with outside of LAN
3) add packet-route to server to routing table.
4) start vpnclient.
5) change VPN-server's address of account.
6) connect account.
7) give ip-address to virtual NIC.
8) change default route to VPN-Gate in routing table.
You can enjoy VPN-Gate-server
EOP
}


function checkIPstr() {
  local IP=$1
  local NumDot=`echo -n $IP | sed -e 's@[^.]@@g' | wc -c`
  local TmpIP=${IP}
  local LengthTmpIP=${#TmpIP}
  local StrIPpart=""
  local HeadLetter=""

  ## check IP
  # count dots
  if [ ${NumDot} -ne 3 ]; then
    echo "Number of dot in IP-address ${IP} is not 3."
    return 1
  fi

  if [ $LengthTmpIP -lt 8 ]
  then
    echo "Too few address-letters."
    return 1
  fi

  # check number and digit of number
  while [ ${LengthTmpIP} -gt 0 ]
  do
    # get top strings before 1st dot
    StrIPpart=`echo ${TmpIP} | grep -oP '^[0-9]*'`
    if [ ${#StrIPpart} -lt 1 ]
    then
      echo "No number in some area in IP-address."
      return 1
    fi

    # store IPtmp for next loop
    TmpIP=`echo ${TmpIP} | sed -e "s/^${StrIPpart}//"`

    # if head of TmpIP is ".", remove "."
    HeadLetter=`echo ${TmpIP:0:1}`
    if [[ $HeadLetter = "." ]]
    then
      TmpIP=${TmpIP#.}

      # if TmpIP has no letter, then finish loop
      if [ ${#TmpIP} -lt 1 ]
      then
        break
      fi
    fi

    # check StrIPpart is number
    expr $StrIPpart + 1 >/dev/null 2>&1
    if [ $? -gt 1 ]
    then
      echo "Non-number-letters are included in IP-address."
      return 1
    fi

    # check StarIPpart is 0-255
    if [[ $StrIPpart -lt 0 || $StrIPpart -gt 255 ]]
    then
      echo "Number of IP-address is out of range[0-255]."
      return 1
    fi
    LengthTmpIP=${#TmpIP}
  done

  return 0
}


##############
#### main ####
##############

# get VPN Gate server address 
ServerIP=`wget http://www.vpngate.net/api/iphone/ -O - 2> /dev/null | grep JP | grep Daiyuu | sort -rn -k 5 -t "," | grep -oP -m 1 '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'`

# check ServerIP-address-string
RetCheckIPstr=`checkIPstr ${ServerIP}`
if [ $? -gt 0 ]
then
  echo $RetCheckIPstr
  exit 1
fi

# check host is communicating with outside of LAN
which isCommunicatingOutLan.sh > /dev/null 2>&1
if [ $? -ne 0 ]
then
  echo "isCommunicatingOutLan.sh does not exist in command-PATH."
  echo "Abort."
  exit 1
fi
isCommunicatingOutLan.sh > /dev/null 2>&1
if [ $? -ne 1 ]
then
  echo "Host is communicating with outside through LAN."
  echo "Changing packet-route stops existing communication through LAN."
  echo "Abort."
  exit 1
fi

# add packet-route to server to routing table.
RealNIC=`route | grep -m 1 default | sed 's/[\t ]\+/\t/g' | cut -f 8`
RealGWayAddress=`route -n | grep ${RealNIC} | sed 's/[\t ]\+/\t/g' | cut -f 2 | sort -r | sed -n '1,1p'`
route add ${ServerIP} gw ${RealGWayAddress} dev ${RealNIC}

# start vpnclient.
PathVpnClientCom=`find /usr/ -type f -name vpnclient`
DirVpnClient=`dirname ${PathVpnClientCom}`
cd $DirVpnClient
./vpnclient start

AccountName=`./vpncmd localhost /CLIENT /CMD AccountList | grep 接続設定名 | sed -e "s/^.*|//"`
if [ ${#AccountName} -lt 1 ]
then
  echo "No VPN-account found."
  echo "Abort."
  ./vpnclient stop
  route del ${ServerIP}
  exit 1
fi

AccountHub=`./vpncmd localhost /CLIENT /CMD AccountList | grep HUB | sed -e "s/^.*|//"`
if [ ${#AccountHub} -lt 1 ]
then
  echo "No VPN-account-hub found."
  echo "Abort."
  ./vpnclient stop
  route del ${ServerIP}
  exit 1
fi

# set server-address to account
./vpncmd localhost /CLIENT /CMD AccountSet ${AccountName} /SERVER:${ServerIP}:443 /HUB:${AccountHub}

# connect to VPN Gate server
./vpncmd localhost /CLIENT /CMD AccountConnect ${AccountName}

# set an address on virtual NIC on linux
VpnNIC=`./vpncmd localhost /CLIENT /CMD AccountList | grep LAN | sed -e "s/^.*|//"`
LinuxVirNIC=`ip l | grep -Po "\S+${VpnNIC}"`
dhclient $LinuxVirNIC

# change default route of routing table to flow to VPN Gate server
VirNicAddress=` ip -4 a show ${LinuxVirNIC} | grep -Po "inet\s+[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+" | grep -Po "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"`
VirGWayAddress=`echo ${VirNicAddress} | grep -Po "[0-9]+\.[0-9]+\."`
VirGWayAddress="${VirGWayAddress}254.254"
route del default
route add default gw ${VirGWayAddress} dev ${LinuxVirNIC}
exit 0

