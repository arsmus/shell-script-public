#!/bin/bash
# 2020 Apr. 04.
# Ryuichi Hashimoto.
# Add VPN-GATE-SERVER to network-default-route

# get network-interface-card
NIC=`route | grep -m 1 default | sed 's/[\t ]\+/\t/g' | \cut -f 8`

# get default-gateway-address
GW_ADRS=`route -n | grep ${NIC} | sed 's/[\t ]\+/\t/g' | cut -f 2 | sort -r | sed -n '1,1p'`

# get VPN-GATE-SERVER
VPNGATE=`wget http://www.vpngate.net/api/iphone/ -O - 2> /dev/null | grep JP | grep Tsukuba | sort -rn -k 5 -t "," | grep -oP -m 1 '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+'`

# Add VPN-GATE-SERVER to network-default-route
route add -host ${VPNGATE} gw ${GW_ADRS}
exit 0

