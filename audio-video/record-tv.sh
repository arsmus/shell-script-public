#!/bin/bash

# 2015 Sep. 12.
# 2015 Aug. 09.
# Ryuichi Hashimoto.

# This script starts tv-recording with recpt1.

# command format
# do-pt-tv-record.sh preSec postSec CHANNEL durationSec MODE outFilePath

if [ $# -ne 6  ]; then
  echo "`basename $0` preSec postSec CHANNEL durationSec MODE outFilePath" >&2
  exit 1
fi

# get args

  # 録画開始前マージン時間(秒)設定
  preSec=$1

  # preSecが数字であることのチェック
  expect_empty=`echo -n ${preSec} | sed 's/[0-9]//g'`
  if [ "x" = "x${preSec}" ] || [ "x" != "x${expect_empty}" ]; then
    echo 'preSec(録画開始前マージン時間) must be number.' >&2
    echo 'Check script.' >&2
    exit 1
  fi

  shift

  # 録画終了後マージン時間(秒)設定
  postSec=$1

  # postSecが数字であることのチェック
  expect_empty=`echo -n ${postSec} | sed 's/[0-9]//g'`
  if [ "x" = "x${postSec}" ] || [ "x" != "x${expect_empty}" ]; then
    echo 'postSec(録画終了後マージン時間) must be number.' >&2
    echo 'Check script.' >&2
    exit 1
  fi

  shift

  # get channel-number
  # channel number in Kobe.
  # NHK-sogo-Kobe:22 NHK-KYOIKU:13 YTV:14 ABC:15 MBS:16 KTV:17 TVO:18 SUN:26
  # BS1:101 BS-Premium:103 BS日テレ:141 BS朝日:151 BS-TBS: 161 BS-Japan: 171 BS-Fuji: 181
  while :
  do
    CHANNEL=$1
    case "$CHANNEL" in
    "22" ) break ;;
    "13" ) break ;;
    "14" ) break ;;
    "15" ) break ;;
    "16" ) break ;;
    "17" ) break ;;
    "18" ) break ;;
    "26" ) break ;;
    "101" ) break ;;
    "103" ) break ;;
    "141" ) break ;;
    "151" ) break ;;
    "161" ) break ;;
    "171" ) break ;;
    "181" ) break ;;
    esac
    echo "Invalid channel number: $1 " >&2
    exit 2
  done

  shift
  
  # get recording-duration-time
  declare -i durationSec
  durationSec=$1
  if [ $durationSec -lt 60  ]; then
    echo "Invalid record time ${durationSec}" >&2
    exit 3
  fi

  shift
  
  # get mode
  while :
  do
    MODE=$1
    case "$MODE" in
    "0" ) break ;;
    "1" ) break ;;
    "2" ) break ;;
    "3" ) break ;;
    esac
    echo "Invalid MODE number: $1 " >&2
    exit 4
  done

  shift
  
  # get outFilePath 
  outFilePath=$1
  if [ "${outFilePath%.ts}" =  "${outFilePath}"  ] && [ "${outFilePath%.mp4}" =  "${outFilePath}"  ] && [ "${outFilePath%.mp3}" =  "${outFilePath}"  ]; then
    echo "Bad output-file-name: ${outFilePath}" >&2
    exit 5
  fi

RECORDER=`which recpt1`
if [ $? -ne 0 ] ; then
  echo 'recpt1 not exist.' >&2
  exit 1
fi

sleep $((60 - $preSec))

durationSec=$(($preSec + $durationSec + $postSec))

# fail safe
case $CHANNEL in
    101|102|191|192|193)
	if [ "${SID}" = 'hd' ]; then
	    SID=${CHANNEL}
	fi ;;
esac
if [ -z "$SID" ]; then
    SID='hd'
fi

# 無加工TS取得
if [ ${MODE} = 0 ]; then
   # MODE=0では必ず無加工のTSを吐き出すこと
   $RECORDER --b25 --strip $CHANNEL $durationSec $outFilePath > /dev/null

# 1SIDのTS取得
elif [ ${MODE} = 1 ]; then
   # 目的のSIDのみ残す
   $RECORDER --b25 --strip --sid $SID $CHANNEL $durationSec $outFilePath > /dev/null

# H.264変換ファイル取得
elif [ ${MODE} = 2 ]; then
	$RECORDER --b25 --strip --sid $SID $CHANNEL $durationSec  ${outFilePath}_tss.ts > /dev/null
	avconv -y -i ${outFilePath}_tss.ts -f mp4 -c:v libx264 -preset slow -r 30000/1001 -aspect 16:9 -s 800x450  -bufsize 20000k -maxrate 25000k -acodec copy -threads $(/usr/bin/getconf _NPROCESSORS_ONLN) $outFilePath
	if [ $? -eq 0 -a -s $outFilePath ] ; then
		rm ${outFilePath}_tss.ts
	else
		mv ${outFilePath}_tss.ts $outFilePath
	fi

# MP3音声のみを取得
elif [ ${MODE} = 3 ]; then
	$RECORDER --b25 --strip --sid $SID $CHANNEL $durationSec ${outFilePath}_tss.ts > /dev/null
	avconv -y -i ${outFilePath}_tss.ts -acodec libmp3lame -ac 2 -ab 128k -ar 48000 -vn $outFilePath
	if [ $? -eq 0 -a -s $outFilePath ] ; then
		rm ${outFilePath}_tss.ts
	else
		mv ${outFilePath}_tss.ts $outFilePath
	fi
fi
exit 0

