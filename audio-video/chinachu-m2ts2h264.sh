#!/bin/bash
# 2021 Mar. 31.
# 2021 Mar. 19.
# 2021 Feb. 21.
# 2021 Feb. 20.
# 2021 Feb. 16.
# 2021 Feb. 11.
# Ryuichi Hashimoto.


# Convert MPEG2-FILEs to MP4-FILEs and add MP4-FILE-information to recorded.json.
# Delete data which do not exist in chinachu recorded.json from /home/CHINACHU-USER/var/chinachu-is_converted2mp4.txt.

# Be careful that /home/CHINACHU-USER/var/chinachu-is_converted2mp4.txt is also used in chinachu-save_record_information-asRecordedCommand.sh.


CmdPath="${0}"
Cmd="${0##*/}"
# IsConvertedPath=/FILEPATH/OF/IS_CONVERTED
IsConvertedPath="${HOME}/var/chinachu-is_converted2mp4.txt"
# Mp4fileDir=/FILEPATH/OF/CONVERTED/MP4/FILE
Mp4fileDir=/var/bigdata/01/chinachu-mp4-210206
# ChinachuJsonPath=/CHINACHU/data/recorded.json
ChinachuJsonPath="${HOME}/chinachu/data/recorded.json"

# exist? dir/file
if [ ! -f "$IsConvertedPath"  ]; then
  echo "$IsConvertedPath does not exist." 1>&2
  exit 1
fi
if [ ! -d "$Mp4fileDir" ]; then
  echo "$Mp4fileDir does not exist." 1>&2
  exit 1
fi
if [ ! -f "$ChinachuJsonPath" ]; then
  echo "$ChinachuJsonPath does not exist." 1>&2
  exit 1
fi

################
### function ###
################
function usage() {
cat << EOS
usage: ${Cmd}

This script is used for Chinachu TV-recording system.
MPEG2-FILEs are converted to MP4-FILEs and MP4-FILE-informations are added to recorded.json.
Data which do not exist in chinachu recorded.json are deleted from /home/CHINACHU-USER/var/chinachu-is_converted2mp4.txt.

Be careful that /home/CHINACHU-USER/var/chinachu-is_converted2mp4.txt is also used in chinachu-save_record_information-asRecordedCommand.sh.

It's a good idea to install this in cron.

Set path of mp4-file-save-directory to variable Mp4fileDir at the top of script.
Set path of chinachu-recorded.json to variable ChinachuJsonPath at the top of script.

Form of each line in /home/CHINACHU-USER/var/chinachu-is_converted2mp4.txt
(on success to converting)
  RecordedFilepath__TAB__InformationJson__TAB__SaveTime__TAB__MP4FILEPATH

(on failed to converting)
  RecordedFilepath__TAB__InformationJson__TAB__SaveTime__TAB__Failed_converting[..]
EOS
return 0
}

############
### main ###
############

## Is another process of same name running?
##   If there is a process that Both PID and GID are different from this script's, another process of same name as this process is running.
##   Abort if another process of same name as this process is running.
# get GID of this process
ProcList=$(ps ax -o pid,ppid,pgid)
PidLines=$(echo "${ProcList}" | grep $$)
Gid=$(echo "$PidLines" | awk -v Pid=$$ '{if ($1 == Pid) print $3}')
# get PID that might be another process whose name is same as this script
OtherPid=$(pgrep -fo "$CmdPath")
# If OtherPid and Gid are different from this script's, another process of this script is running.
if [ $$ -ne $OtherPid ]; then
  OtherGid=$(echo "$ProcList" | awk -v Pid=$OtherPid '{if ($1 == Pid) print $3}')
  if [ $OtherGid -ne $Gid ]; then
    echo "Another ${CmdPath} is running." 1>&2
    echo "Aborted." 1>&2
    exit 1
  fi
fi

# check arguement
if [ $# -gt 0 ]; then
  usage
  exit 1
fi

# check dependency
which ts2h264ffmpeg-quit_on_another_ffmpeg.sh 1> /dev/null 2> /dev/null
ResultCode=$?
if [ $ResultCode -ne 0 ]; then
  echo "Please install ts2h264ffmpeg-quit_on_another_ffmpeg.sh in PATH-directory from https://bitbucket.org/arsmus/shell-script-public/src/ca136746b8e01cffba28f7b162a9d283700ba9b5/audio-video/ts2h264ffmpeg-quit_on_another_ffmpeg.sh?at=master" 1>&2
fi

## While deleting a video-information from chinachu, the information in IsConvertedPath is left.
## Delete the information from IsConvertedPath.
##   1st column of the line in IsConvertedPath is filepath.
##   If the filename without extension is not included in chinachu-recorded.json, delete the line. (extension is .m2ts or .mp4)
cp "$IsConvertedPath" "${IsConvertedPath}.tmp"
while read Line; do
  # get 1st column (filepath) of line
  FilepathSaved=$(echo "$Line" | awk -F "__TAB__" '{print $1}')
  FilepathSaved=${FilepathSaved%.*}
  Filename=${FilepathSaved##*/}

  # Is Filename found in chinachu/data/recorded.json?
  #   If Filename is not found in chinachu/data/recorded.json,
  #   delete the line in which Filename is included from IsConvertedPath.
  cat "$ChinachuJsonPath" | grep -q "${Filename}"
  ResultCode=$?
  if [ $ResultCode -ne 0 ]; then
    # delete the line from IsConvertedPath
    cat "${IsConvertedPath}" | grep -v "$Filename" > "${IsConvertedPath}.tmp2"
    mv "${IsConvertedPath}.tmp2" "${IsConvertedPath}"
  fi
done < "${IsConvertedPath}.tmp"


## get data for conversion from IsConvertedPath
# get first line of only 3 fields lines from IsConvertedPath
#   If 4 fields, the MPEG2-file is already processed.
#   4th field shows that past converting was seccessful or not.
NotConvertedLine=$(cat "${IsConvertedPath}" | awk -F "__TAB__" '{if (NF == 3) print}' | head -n 1)
if [ ${#NotConvertedLine} -lt 5 ]; then
  echo "No file which is not converted to H.264" 1>&2
  exit 2
fi
Mpeg2filePath=$(echo "${NotConvertedLine}" | awk -F "__TAB__" '{print $1}')
Mpeg2fileJson=$(echo "${NotConvertedLine}" | awk -F "__TAB__" '{print $2}')

if [ ! -f "$Mpeg2filePath" ]; then
  echo "${Mpeg2filePath} does not exist." 1>&2
  exit 1
fi
echo "$Mpeg2fileJson" | jq '. | length' 1> /dev/null 2> /dev/null
ResultCode=$?
if [ $ResultCode -ne 0 ]; then
  echo "$Mpeg2fileJson has no json." 1>&2
  exit 1
fi

# convert Mpeg2filePath to H.264
ResultStr=$(ts2h264ffmpeg-quit_on_another_ffmpeg.sh -d "$Mp4fileDir" "${Mpeg2filePath}")
  # value of ResultStr is mp4-filepath or "Failed_converting_${infileTmp}"

## Add result of convesion to ${IsConvertedPath}
# * Search a line including ${Mpeg2filePath} in ${IsConvertedPath}
# * Add "__TAB__${ResultStr}" to the end of the line.
#     __TAB__ : field separater
#
#   Format of ${IsConvertedPath} before converting:
#     ${RecordedFilepath}__TAB__${InformationJson}__TAB__${SaveTime}

IsConvertedFileTmp=$(cat "${IsConvertedPath}")
echo "${IsConvertedFileTmp}" | awk -v MpegPath="${Mpeg2filePath}" -v ResStr="${ResultStr}" 'BEGIN {HeadMpeg2="^"MpegPath } {if ($0 ~ HeadMpeg2) {print $0 "__TAB__" ResStr} else {print $0}}' > ${IsConvertedPath}

# In case that convertion failed, quit.
#   ResultStr get 'Failed_converting' from std-out of ts2h264ffmpeg-quit_on_another_ffmpeg.sh
if [[ "${ResultStr}" =~ 'Failed_converting' ]]; then
  exit 1
fi

# Format of ${IsConvertedPath} after converting:
#   (on success to converting)
#     ${RecordedFilepath}__TAB__${InformationJson}__TAB__${SaveTime}__TAB__MP4FILEPATH
#
#   (on failed to converting)
#     ${RecordedFilepath}__TAB__${InformationJson}__TAB__${SaveTime}__TAB__Failed_converting[..]

## edit recorded.json
#    出典　https://github.com/kenh0u/recordedCommand/blob/master/recorded.sh
ChinachuJsonTmp=$(cat "${ChinachuJsonPath}")
echo "${ChinachuJsonTmp}" | jq -c ".+[ \
  $(
    echo ${Mpeg2fileJson} |
    jq  ".id += \"_mp4\"" | \
    jq  ".recorded = \"${ResultStr//\//\\\/}\"" | \
    jq -c ".title = \"[mp4]$(echo $Mpeg2fileJson | jq '.title' | tr --delete \")\"" \
  )
  ]" > "$ChinachuJsonPath"

exit 0

