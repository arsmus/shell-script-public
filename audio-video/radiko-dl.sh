#!/usr/bin/zsh
# Copyright (c) 2017 uru (https://twitter.com/uru_2)
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal

# 2021 Apr. 25.
# 2021 Jan. 17.
# Ryuichi Hashimoto edited.

# command option
#  -id) radio-station-id
#  -ft) start time in yymmddHHMMSS
#  -to) end time in yymmddHHMMSS
#  -n)  title (not necessary)
#  -o)  outfile-path


Cmd=${0##*/}

# dir=~/Downloads/radiko
dir=/bigdata/02/sound/radio/unsaved

readonly AUTHKEY_VALUE="bcd151073c03b352e1ef2fd66c32209da9ca0afa"


################
### function ###
################
function usage() {
cat << EOP
usage: $Cmd -id STATION -ft SATRTTIME -to ENDTIME -n STRING-ATTACHED-TO OUTFILE-NAME -o OUT-DIR
-id, -ft, -to: necessary options
format of STARTTIME, ENDTIME: yymmddHHMMSS
  example: 210131153000
EOP
}


############
### main ###
############
if [ $# -lt 3 ]; then
  usage
  exit 1
fi

# オプションを取得
while [ $# -gt 0 ]
do
    case $1 in
        -id) shift; id="$1" ;;
        -ft) shift; ft="$1" ;;
        -to) shift; to="$1" ;;
        -n) shift; program="$1" ;;
        -o) shift; dir="$1" ;;
        *) break ;;
    esac
    shift
done

# オプションがそろっているかチェック
if test -z "$id" || test -z "$ft" || test -z "$to"; then
    echo '-id 放送局ID -ft 開始時間（190430133000） -to 終了時間' 1>&2
    usage
    exit 1
fi
if [ 12 -ne ${#ft} ]; then
  echo 'Please input yymmddHHMMSS to -ft option.' 1>&2
  usage
  exit 1
fi
if [ 12 -ne ${#to} ]; then
  echo 'Please input yymmddHHMMSS to -to option.' 1>&2
  usage
  exit 1
fi

ft="20${ft}"
to="20${to}"

# ファイル名を決める
if test ! -z "$program"; then
    filename="${ft}_${program}_${id}"
else
    filename="${ft}_${id}"
fi

# フォルダへ移動
dir=${dir%/}
if test -d "${dir}"; then
    cd "${dir}"
else
    mkdir -p "${dir}"
    cd "${dir}"
fi

# 承認ひとつめ
auth1=`curl \
    --silent \
    --header "X-Radiko-App: pc_html5" \
    --header "X-Radiko-App-Version: 0.0.1" \
    --header "X-Radiko-Device: pc" \
    --header "X-Radiko-User: dummy_user" \
    --dump-header - \
    --output /dev/null \
    "https://radiko.jp/v2/api/auth1"`

authtoken=$(echo "${auth1}" | awk 'tolower($0) ~/^x-radiko-authtoken: / {print substr($0,21,length($0)-21)}')
keyoffset=$(echo "${auth1}" | awk 'tolower($0) ~/^x-radiko-keyoffset: / {print substr($0,21,length($0)-21)}')
keylength=$(echo "${auth1}" | awk 'tolower($0) ~/^x-radiko-keylength: / {print substr($0,21,length($0)-21)}')

partialkey=$(echo "${AUTHKEY_VALUE}" | dd bs=1 "skip=${keyoffset}" "count=${keylength}" 2> /dev/null | base64)

# 承認ふたつめ
curl \
    --silent \
    --insecure \
    --header "X-Radiko-Device: pc" \
    --header "X-Radiko-User: dummy_user" \
    --header 'X-Radiko-AuthToken: '${authtoken} \
    --header 'X-Radiko-PartialKey: '${partialkey} \
    --output /dev/null \
    "https://radiko.jp/v2/api/auth2"

# 放送済みかどうかチェック
Year=${to:0:4}
Month=${to:4:2}
Day=${to:6:2}
Hour=${to:8:2}
Minute=${to:10:2}
Second=${to:12:2}
to_formated="${Year}-${Month}-${Day} ${Hour}:${Minute}:${Second}"
to_unix=`date -d "${to_formated}" +%s`
now=`date +%s`
if test $to_unix -ge $now; then
    echo '放送終了前です' 1>&2
    exit 1
fi

# ダウンロード
ffmpeg \
    -y \
    -loglevel error \
    -fflags +discardcorrupt \
    -headers 'X-Radiko-Authtoken: '${authtoken} \
    -i 'https://radiko.jp/v2/api/ts/playlist.m3u8?station_id='$id'&l=15&ft='$ft'&to='$to \
    -bsf:a aac_adtstoasc -acodec copy "${filename}".m4a

# 通知など出してみる
# osascript -e 'display notification "'"$filename"' 完了しました" with title "radiko-dl.sh" sound name "Glass"'
exit 0
