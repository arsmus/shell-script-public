#!/bin/bash
# 2021 Feb. 20.
# 2019 May  16.
# 2019 Apr. 14.
# Ryuichi Hashimoto.

# Record a simul-radio-program to FILE.m4a

# COMMAND STATION-ID DURATION(sec) SAVE-DIR


CMD=${0##*/}

################
### function ###
################
function usage() {
cat << EOS
usage: $CMD STATION-ID DURATION(sec) SAVE-DIR

Record a simul-radio-program in SAVE-DIR.
EOS
}

############
### main ###
############
# check dependency
\which ffmpeg 1> /dev/null 2> /dev/null
ResultCode=$?
if [ $ResultCode -ne 0 ]; then
  \echo "Please install ffmpeg" 2>&1
  \exit 1
fi

# check arguments
if [ $# -ne 3 ]; then
  \echo "Illegal number of arguments." 2>&1
  usage
  \exit 1
fi

# get arguements
StationId=$1
StreamUrl="http://mtist.as.smartstream.ne.jp/${StationId}/livestream/playlist.m3u8"

Duration=$2
\expr "$Duration" + 1 >/dev/null 2>&1
ResultCode=$?
if [ $ResultCode -ne 0 ]; then
  \echo "Illegal DURATION." 2>&1
  usage
  \exit 1
fi

Dir="$3"
Dir="${Dir%/}"
if [ ! -d "$Dir" ] || [ ! -w "$Dir" ]; then
  \echo "${Dir} is not directory or not writable." 2>&1
  usage
  \exit 1
fi

# get filename for output
Date=`\date '+%Y%m%d%H%M'`
OutFile="${Date}radio${StationId}.m4a"

# recording
\ffmpeg -y -i "${StreamUrl}" -t ${Duration} -movflags faststart -vn -acodec copy "${Dir}/${OutFile}"

\exit 0

