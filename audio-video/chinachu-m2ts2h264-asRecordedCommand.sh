#!/bin/bash
# 2021 Feb. 07.
# convert FILE.m2ts which is just recorded by chinachu to FILE.mp4.

# Original script is published on https://github.com/kenh0u/recordedCommand/blob/master/recorded.sh.


CMD=${0##*/}
RECORDEDJSON="/home/USER/chinachu/data/recorded.json"
MP4DIR="/YOUR/MP4/DIR/"
TSFILEPATH=$1
TSFILENAME=${1##*/}
MP4FILENAME="${TSFILENAME%.*}.mp4"
MP4FILEPATH="${MP4DIR}${MP4FILENAME}"
CPU_CORES=$(/usr/bin/getconf _NPROCESSORS_ONLN)
# width-resolution of mp4-video is set equal to or less than value of NewX
NewX=870
FFMPEGpresetfile='/usr/local/my/ffmpeg/libx264-ts-good.ffpreset'

#######################################################
#### /usr/local/my/ffmpeg/libx264-ts-good.ffpreset ####
#######################################################
## level=41
## crf=25
## coder=1
## flags=+loop
## cmp=+chroma
## partitions=+parti8x8+parti4x4+partp8x8+partb8x8
## me_method=umh
## subq=7
## me_range=16
## g=250
## keyint_min=25
## sc_threshold=40
## i_qfactor=0.71
## b_strategy=1
## qmin=10
## rc_eq=’blurCplx^(1-qComp)’
## bf=16
## bidir_refine=1
## refs=6
#######################################################


if [ ! -f ${RECORDEDJSON} ]; then
  echo "${RECORDEDJSON} does not exist." 1>&2
  exit 1
fi
if [ ! -d ${MP4DIR} ]; then
  echo "${MP4DIR} does not exist." 1>&2
  exit 1
fi


################
### function ###
################
# get resolution of movie.
getResolut(){
  Resolut=`\ffprobe -i ${TSFILEPATH} 2>&1 | \grep -oP '\d\d+x\d\d+'`
  ResolutX=`\echo $Resolut | \grep -oP '^\d\d+'`
  ResolutY=`\echo $Resolut | \grep -oP '\d\d+$'`
  # x-size of resolution is ResolutX
  # y-size of resolution is ResolutY
  # by the result of above routine.
}

################
### function ###
################
# get resolution for new video.
getNewResolution(){
  getResolut
  # NewX : max width-resolution
  while [[ $NewX -gt 100 ]]; do
    NewY=$(( NewX * ResolutY / ResolutX ))
    # 上記計算で小数部が切り捨てられていないNewYを求めるため
    # NewYが小数点以下を切り捨てたものかどうかチェックする
    #   NewYは小数部が切り捨てられていれば1小さい整数になっている
    NewYReal=`\echo "scale=5; $NewY + 0.00000" | \bc`
    # NewYが小数部を切り捨てていればNewYRealは本来よりも1小さい
    NewY2=`\echo "scale=5; $NewX * $ResolutY / $ResolutX" | \bc`
    # NewY2は切り捨てられていない正しい値(実数型)

    # $NewYReal = $NewY2ならばNewYは正しい整数値
    #   実数を -eq で比較できないので == で文字として比較する
    if [[ $NewYReal == $NewY2 ]]; then

      # NewYが奇数ではffmpegでの動画変換が失敗するようなので偶数を選ぶ
      if [ `expr $NewY % 2` -eq 0 ]; then
        # 偶数
        \break
      fi
    fi
    NewX=$(( NewX - 10 ))
  done
  if [[ $NewX -lt 101 ]]; then
    # Could not get the resolution for new video of ${TSFILEPATH}
    NewX=${ResolutX}
    NewY=${ResolutY}
  fi
  # x-size of resolution for new video is NewX
  # y-size of resolution for new video is NewY
  # by the result of above routine.
}


############
### main ###
############

## for escaping conflict of writing to recorded.json
\sleep 5

# wait when TSFILENAME is opened.
COUNT=0
while [ $(lsof | grep "${TSFILENAME}") ]; do
  COUNT=`expr ${COUNT} + 1`
  \sleep 5
  if [ ${COUNT} -gt 100 ]; then
    echo "${TSFILENAME} is opened by another process." 1>&2
    exit 1
  fi
done

### encode ts to mp4
## disallow writing to ts file
chmod 444 "$1"

# check the file is mpeg2 or not
# uncoded yet.


# check the file is decrypted or not
# uncoded yet.


# check the file has only one stream or not
# uncoded yet.


## convert large video-resolution to less resolution
# get resolution of video
getResolut
if [ $ResolutX -lt 1000 ]; then
  # In case of small resolution-video, not change resolution
  NewX=$ResolutX
  NewY=$ResolutY
else
  getNewResolution
fi

# convert to H.264.
nice -n 10 ffmpeg -y -i ${TSFILEPATH} -f mp4 -vcodec libx264 -fpre ${FFMPEGpresetfile} -r 30000/1001 -aspect 16:9 -s ${NewX}x${NewY} -bufsize 20000k -maxrate 25000k -acodec aac -absf aac_adtstoasc -strict -2 -threads $CPU_CORES ${MP4FILEPATH}
Result=$?
if [ $Result -ne 0 ]; then
  if [ -f ${MP4FILEPATH} ]; then
    \rm -f ${MP4FILEPATH}
  fi
  \echo "Failed H.264 converting." 1>&2
  exit 1
fi

## allow writing to ts file
chmod 666 "$1"

# allow writing to mp4 file
chmod 666 "${MP4FILEPATH}"

## append to recorded.json
RECORDEDJSONTEMP=`cat $RECORDEDJSON`
echo $RECORDEDJSONTEMP | jq -c ".+[ \
  $(
    echo $2 |
    jq  ".id += \"_mp4\"" | \
    jq  ".recorded = \"${MP4FILEPATH//\//\\\/}\"" | \
    jq -c ".title = \"[mp4]$(echo $2 | jq '.title' | tr --delete \")\"" \
  )
  ]" > $RECORDEDJSON

## ${MP4FILEPATH//\//\\\/} について
##    ${MP4FILEPATH//ABC/xyz} は変数MP4FILEPATHの文字列内のすべてのABCをxyzに置き換えた文字列を返す。
##    上例では \/ が ABC に、\\\/ が xyz に相当する。
##    \/ は / をメタ文字ではなく単なる文字として扱う。
##    \\\/ は \\ と \/ に分けられる。
##    \\ は \ をメタ文字ではなく単なる文字として扱う。
##    \/ は / をメタ文字ではなく単なる文字として扱う。
##    したがって、 \\\/ は単なる文字としての \/ を表す。
##    ${MP4FILEPATH//\//\\\/} はMP4FILEPATHの文字列内のすべての / を \/ に置き換えた文字列を返す。

## | , && , || , & は、1行を複数行に分ける時、行末にバックスラッシュを書かなくてもよい。
##    $(コマンド)の ( , ) も？

exit 0

