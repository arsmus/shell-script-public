#!/bin/bash
# 2016 Feb. 12.
# 2016 Feb. 11.
# Ryuichi Hashimoto.

# debug='on'
debug='off'

mp4dir="/bigdata"
tmpDir="/bigdata/err-mp3"

for filepath in `\find ${mp4dir}/ -type f -name '*.mp4'`; do
  echo $filepath 1>&2
  filename=${filepath##*/}

  if [ -e ${tmpDir}/${filename} ]; then
    if [ $debug = 'on' ]; then
      echo "${tmpDir}/${filename} exists." 1>&2
    fi
    continue
  fi

  outStr=`avconv -i $filepath 2>&1`
  if [ $debug = 'on' ]; then
    echo $outstr 1>&2
  fi
  num=`echo $outStr | grep -c "Audio: aac"`
  if [ $num -gt 0 ]; then
    num=`echo $outStr | grep -c "1024x576"`
    if [ $num -gt 0 ]; then
      avconv -y -i $filepath -f mp4  -c:v libx264 -preset slow -r 30000/1001 -aspect 16:9 -s 800x450 -c:a libmp3lame -ac 2 -b:a 192k -threads $(/usr/bin/getconf _NPROCESSORS_ONLN) ${tmpDir}/${filename} && mv ${tmpDir}/${filename} $filepath
    else
      avconv -y -i $filepath -f mp4 -c:v copy -c:a libmp3lame -ac 2 -b:a 192k -threads $(/usr/bin/getconf _NPROCESSORS_ONLN) ${tmpDir}/${filename} && mv ${tmpDir}/${filename} ${filepath}
    fi
  fi
done

