#!/bin/bash
# 2021 Feb. 16.
# 2021 Feb. 15.
# Ryuichi Hashimoto.

# repeat chinachu-m2ts2h264.sh till all m2ts-files are converted to H.264.


Cmd=${0##*/}
ConvertCommand=chinachu-m2ts2h264.sh
IsConvertedPath=/home/tv/var/chinachu-is_converted2mp4.txt
if [ ! -f $IsConvertedPath ]; then
  echo "$IsConvertedPath does not exist." 1>&2
  exit 1
fi

################
### function ###
################
function usage() {
cat << EOS
usage: $Cmd

Convert m2ts-files in chinachu to H.264-mp4-files and add mp4-information to chinachu.
EOS
}

############
### main ###
############
# check arguements
if [ $# -gt 0 ]; then
  echo "No arguements are allowed."
  usage
  exit 1
fi

# check dependency
$(which ${ConvertCommand}) 1> /dev/null 2> /dev/null
ResultCode=$?
if [ $ResultCode -ne 0 ]; then
  echo "Please install $ResultCode from https://bitbucket.org/arsmus/shell-script-public/src/master/audio-video/chinachu-m2ts2h264.sh" 1>&2
  exit 1
fi

## run ConvertCommand as long as there are rows with 3 columns in IsConvertedPath.
##   Columns are separated by "__TAB__" in IsConvertedPath.
##   If the line has 3 columns, the line is not converted yet.
##   If the line has 4 columns, the line is already converted.
##   2 while-loops are nested.
##   Contents in IsConvertedPath is changed by ConvertCommand in inside-while-loop. 
##   Program newly re-enter inside-while-loop every time after changing content in inside loop
IsFinished='No'
CountWhile=0
while [ $CountWhile -lt 100 ]; do
  CountWhile=`expr $CountWhile + 1`

  if [ $IsFinished = 'Yes' ]; then
    break
  fi

  # check 3 column rows exist in IsConvertedPath or not.
  Num3colRows=0
  Num3colRows=$(cat "$IsConvertedPath" | awk -F "__TAB__" 'BEGIN { Num3cols = 0 }{ if (NF == 3 ) ++Num3cols } END {print Num3cols}')
  if [ $Num3colRows -eq 0 ]; then
    IsFinished='Yes'
    break
  fi

  while read Row; do
    ##  run ConvertCommand if row has 3 columns.
    # get 3 column row


    ${ConvertCommand}

  done < "$IsConvertedPath"
done



## run ConvertCommand as long as there are rows with 3 columns in IsConvertedPath.
##   Columns are separated by "__TAB__" in IsConvertedPath.
##   There are 3 columns in the line which is not converted.
##   There are 4 columns in the line which is converted.
##   Set 2 while-loops nested.
##   Contents in IsConvertedPath is changed by ConvertCommand in inside-while-loop. 
##   Newly enter inside-while-loop every time after changing content in inside loop
IsFinished='No'
CountWhile=0
while [ $CountWhile -lt 100 ]; do
  CountWhile=`expr $CountWhile + 1`
  if [ $IsFinished = 'Yes' ]; then
    break
  fi

  while read Row; do
    # check 3 column rows exist or not.
    Num3colRows=0
    Num3colRows=$(echo $Row | awk -F "__TAB__" 'BEGIN { Num3cols = 0 }{ if (NF == 3 ) ++Num3cols } END {print Num3cols}')
    if [ $Num3colRows -eq 0 ]; then
      IsFinished='Yes'
      break
    fi

    # run ConvertCommand if row has 3 columns.
    ${ConvertCommand}

  done < "$IsConvertedPath"
done

exit 0

