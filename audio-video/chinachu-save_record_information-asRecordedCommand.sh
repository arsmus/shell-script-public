#!/bin/bash
# 2021 Mar. 31.
# 2021 Feb. 20.
# 2021 Feb. 09.
# Ryuichi Hashimoto.


# Save filepath and information of the TV program just recorded by chinachu in a file additionally.
# Delete data which do not exist in chinachu recorded.json from the file.

# Be careful that SaveFilePath(/home/USER/var/chinachu-is_converted2mp4.txt) is also used in chinachu-m2ts2h264.sh.


Cmd=${0##/}
# SaveFileDir=/DIR/FOR/IS_CONVERTED-FILE
SaveFileDir="${HOME}/var"
SaveFileDir=${SaveFileDir%/}
# SaveFileName=IS_CONVERTED-FILE
SaveFileName=chinachu-is_converted2mp4.txt
SaveFilePath="${SaveFileDir}/${SaveFileName}"
# ChinachuRecordedJson="/PATH/OF/YOUR/CHINACHU/recorded.json"
ChinachuRecordedJson="${HOME}/chinachu/data/recorded.jsoa"n
RecordedFilePath="$1"
InformationJson="$2"
SaveTime=$(date "+%Y%m%d%H%M%S")


################
### function ###
################
function usage() {
cat << EOS
Save filepath and information of the TV program just recorded by chinachu in a file additionally.
Delete data which do not exist in chinachu recorded.json from the file.

Set path of this script to recordedCommand in chinachu/config.json.
Be careful that /home/CHINACHU-USER/var/chinachu-is_converted2mp4.txt is also used in chinachu-m2ts2h264.sh.
EOS
}

############
### main ###
############
if [ ! -d "$SaveFileDir" ]; then
  mkdir -p "$SaveFileDir"
  echo "Directory $SaveFileDir (for chinachu-is_converted2mp4.txt) is created." 1>&2
fi
if [ ! -f "$SaveFilePath" ]; then
  touch "${SaveFilePath}"
  echo "$SaveFilePath is created." 1>&2
fi
if [ ! -f "$RecordedFilePath" ]; then
  echo "$RecordedFilePath is not found." 1>&2
  exit 1
fi
if [ ${#InformationJson} -lt 1 ]; then
  echo "$InformationJson is empty." 1>&2
  exit 1
fi

# write recordedFilePath json-data dateTime with "__TAB__"-separated to file
echo "${RecordedFilePath}__TAB__${InformationJson}__TAB__${SaveTime}" >> "${SaveFilePath}"

## Delete what does not exist in chinachu/data/recorded.json as the result of deleting files from chinachu.
##  1st column of the line in SaveFilePath is filepath.
##  If the filename without extension is not included in chinachu-recorded.json, delete the line. (extension is .m2ts or .mp4)

# loop to process each line in SaveFilePath
cp "$SaveFilePath" "${SaveFilePath}.tmp"
while read Line; do
  # get 1st column (filepath) of line
  FilepathSaved=$(echo "$Line" | awk -F "__TAB__" '{print $1}')
  FilepathSaved=${FilepathSaved%.*}
  Filename=${FilepathSaved##*/}

  # Is Filename found in chinachu/data/recorded.json?
  cat "$ChinachuRecordedJson" | grep -q "${Filename}"
  ResultCode=$?
  if [ $ResultCode -ne 0 ]; then
    # delete the line from SaveFilePath
    cat "${SaveFilePath}" | grep -v "$Filename" > "${SaveFilePath}.tmp2"
    mv "${SaveFilePath}.tmp2" "${SaveFilePath}"
  fi
done < "${SaveFilePath}.tmp"

exit 0

