#!/bin/bash
#
# 2008 Mar. 14.
# 2008 Mar. 8.
# Written by Rryuichi Hashimoto.
#
# Extract sound from mpeg2, convert to mp3
#
#
# set values
DSTDIR='.'
CMDNAME=`basename $0`
STARTTIME=`date`

# check args
if [ $# -lt 1 ]; then
	echo "Extract sound from mpeg2, convert to mp3." 1>&2
	echo "Usage: $CMDNAME -d DstDir SrcMpeg2[ ...]" 1>&2
	exit 2
fi

# check command line options
while [ "$#" -gt 0 ]; do
	case $1 in
		"-h" | "--help" | "-\?")
			echo "Extract sound from mpeg2, convert to mp3." 1>&2
			echo "Usage: $CMDNAME -d DstDir SrcMpeg2[ ...]" 1>&2
			exit 4
			;;
		"-d")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -d" 1>&2
				exit 8
			fi
			TMPVAR=$2
			DSTDIR=${TMPVAR%/}
			DSTDIR2=${DSTDIR%/}
			while  [ $DSTDIR != $DSTDIR2 ]; do
				DSTDIR=$DSTDIR2
				$DSTDIR2=${DSTDIR%/}
			done
			;;
		*)
			SRCNAME=($@)
			break
			;;
	esac
	shift
	shift
done

# check SrcMpegFile
for FNAME in ${SRCNAME[@]}
do
	if [ ! -f $FNAME ] || [ ! -r $FNAME ]; then
		echo -e "$FNAME does not exist or is not readable!" 1>&2
		exit 10
	fi
done



# makedir DSTDIR
if [ ! -d $DSTDIR ]; then 
	mkdir -p ${DSTDIR}
fi
chmod +rwx ${DSTDIR}
if [ ! -d $DSTDIR ] || [ ! -r $DSTDIR ] || [ ! -w $DSTDIR ] || [ ! -x $DSTDIR ]; then
	echo -e "Can't read/write/execute ${DSTDIR}" 1>&2
	exit 12
fi

for FNAME in ${SRCNAME[@]}
do
	BASEFNAME=${FNAME##*/}
	BASEFNAME=${BASEFNAME%.*}
	ffmpeg -y -i $FNAME -vn -acodec copy ${DSTDIR}/${BASEFNAME}.mp3 1>&2
done

FINISHTIME=`date`
echo "$STARTTIME Started"
echo "$FINISHTIME All finished"

exit $?

