#!/bin/bash
# 2022 Dec. 18.
# 2019 Mar. 23.
# Ryuichi Hashimoto.

# get audio-stream from VIDEO-FILE.
# audio-stream-file is saved in the same directory of VIDEO-FILE.


COMMAND=`basename $0`

################
### function ###
################
function usage_exit() {
cat << EOS
Usage: ${COMMAND} [-d DestDir] VideoFile
Get an audio-stream from a VideoFile.
If '-d' option is not given, OutAudioFile is saved in the same directory of VideoFile.
EOS
exit 1
}

##########
## main ##
##########

DestDir="${HOME}"

# check arguements
if [ $# -lt 1 ] || [ $# -eq 2 ] || [ $# -gt 3 ]; then
  echo "Illegal count of arguements." 1>&2
  usage_exit 1>&2
fi

if [ $# -eq 1 ]; then
  VideoFile="$1"
  if [ ! -r ${VideoFile} ]; then
    echo "${VideoFile} is not readable." 1>&2
    exit 1
  fi
  VideoFilePath=$(readlink -e ${VideoFile})
  VideoFileParentDir="${VideoFilePath%/*}"
  DestDir="${VideoFileParentDir}"

else
  # 3 arguements (-d dir VideoFile)
  if [ '-d' == "$1" ]; then
    shift
    DestDir="$1"
    DestDir="${DestDir%/}"
    shift
    VideoFile="$1"
    if [ ! -r ${VideoFile} ]; then
      echo "${VideoFile} is not readable." 1>&2
      exit 1
    fi
    VideoFilePath=$(readlink -e ${VideoFile})
    VideoFileParentDir="${VideoFilePath%/*}"
  else
    echo "Illegal arguements." 1>&2
    usage_exit 1>&2
  fi
fi

if [ ! -w "${DestDir}" ]; then
  echo "${DestDir} is not writable." 1>&2
  exit 1
fi

VideoFileBase="${VideoFilePath%.*}"
VideoFileBase="${VideoFileBase##*/}"

# get audio-type
AudioType=`ffmpeg -i ${VideoFilePath} 2>&1 | grep -Po 'Audio: .*? ' | cut -d ' ' -f 2-2`

# get audio-stream
# ffmpeg -i ${VideoFilePath} -acodec copy -map 0:1 "${DestDir}/${VideoFileBase}.${AudioType}" 
ffmpeg -i ${VideoFilePath} -c:a copy "${DestDir}/${VideoFileBase}.${AudioType}" 

exit 0

