#!/usr/bin/awk -f
# 2018 Nov. 10.
# Ryuichi Hashimoto

# Standard-in: standard-error-output of "ffmpeg -i MovieFile"
# Output: SIDs of high-resolution of a movie-file to standard-out.
# High-resolution means longer size than 1,000 of X-length.

{
  # SIDを検索(文字列"Program"を含む行に存在する)
  if ( index($0, "Program") != 0 ) {
    # 見つけたらその行に含まれるSIDを検索
    match($0, /[0-9]+/)

    # SIDが無い時は0にセットする
    if (RSTART < 1 ) {
      SID = 0
    } else {
      SID=substr($0,RSTART, RLENGTH)
    }
  # 次行を読み込み、文字列"Video"を検索
  } if ( SID != 0 ) {
    if ( index($0, "Video") != 0 ) {
      # 見つけたらその行に含まれる [123456789][0-9]*x[0-9]+ を解像度として取得
      match($0, /[123456789][0-9]*x[0-9]+/)
      Resolution = substr($0,RSTART, RLENGTH)
      # 横幅を取得
      match(Resolution, /[123456789][0-9]*x/)
      Xwidth=substr(Resolution, RSTART, RLENGTH)
      Xwidth = substr(Xwidth,1,length(Xwidth)-1)
      # 横幅が1,000以上ならば、Program番号を出力
      if ( Xwidth > 1000 ) {
        print SID
      }
    }
  }
}
