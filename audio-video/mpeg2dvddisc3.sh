#!/bin/bash
#
# make DVD-VIDEO from mpeg2withoutVOBU
# (using replex to multiplex)
# (using growisofs to burn DVD-DISK directly without iso-file)
#
# 2007 Jun. 7.
# Ryuichi Hashimoto.
#

# set values
TMPDIR=/bigdata/tmp/work
LOGDIR=/bigdata/tmp/log
VOBUDIR=/bigdata/tmp/mpeg2
DVDDIR=/bigdata/tmp/dvd
DEVDVD=/dev/hdc
SPEED='8'

CMDNAME=`basename $0`
STARTTIME=`date`

# check args 
if [ $# -lt 1 ]; then 
	echo "Make DVD-VIDEO-DISK from mpeg2withoutVOBU(s). (01)" 1>&2
	echo "(using growisofs to burn DVD-DISK directly without iso-file)" 1>&2
	echo "Usage: $CMDNAME -t TmpDir -l LogDir -m Mpeg2withVOBU-Dir -d DVDimgDir -s Speed SrcMpeg2[ ...]" 1>&2
	exit 2
fi

# check command line options
while [ "$#" -gt 0 ]; do
	case $1 in
		"-h" | "--help" | "-\?")
			echo "Make DVD-VIDEO-DISK from mpeg2withoutVOBU\(s\). \(01\)" 1>&2
			echo "\(using growisofs to burn DVD-DISK directly without iso-file\)" 1>&2
			echo "Usage: $CMDNAME -t TmpDir -l LogDir -m Mpeg2withVOBU-Dir -d DVDimgDir -s Speed SrcMpeg2[ ...]" 1>&2
			exit 4
			;;
		"-t" | "--tmp")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -t | --tmp" 1>&2
				exit 5
			fi
			TMPVAR=$2
			TMPDIR=${TMPVAR%/}
			TMPDIR2=${TMPDIR%/}
			while  [ $TMPDIR != $TMPDIR2 ]; do
					TMPDIR=$TMPDIR2
					TMPDIR2=${TMPDIR%/}
			done
			;;
		"-l" | "--log")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -l | --log" 1>&2
				exit 5
			fi
			TMPVAR=$2
			LOGDIR=${TMPVAR%/}
			LOGDIR2=${LOGDIR%/}
			while  [ $LOGDIR != $LOGDIR2 ]; do
					LOGDIR=$LOGDIR2
					LOGDIR2=${LOGDIR%/}
			done
			;;
		"-m" | "--vobudir")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -m | --vobudir" 1>&2
				exit 5
			fi
			TMPVAR=$2
			VOBUDIR=${TMPVAR%/}
			VOBUDIR2=${VOBUDIR%/}
			while  [ $VOBUDIR != $VOBUDIR2 ]; do
					VOBUDIR=$VOBUDIR2
					VOBUDIR2=${VOBUDIR%/}
			done
			;;
		"-d" | "--dvdimg")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -d | --dvdimg" 1>&2
				exit 5
			fi
			TMPVAR=$2
			DVDDIR=${TMPVAR%/}
			DVDDIR2=${DVDDIR%/}
			while  [ $DVDDIR != $DVDDIR2 ]; do
					DVDDIR=$DVDDIR2
					DVDDIR2=${DVDDIR%/}
			done
			;;
		"-s" | "--speed")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -s | --speed" 1>&2
				exit 5
			fi
			SPEED=$2
			TMP=`expr $SPEED + 0`
			if [ $TMP -ne $SPEED ]; then
				echo -e "Error. -s|--speed must integer." 1>&2
				exit 6
			fi
			;;
		*)
			SRCNAME=($@)
			break
			;;
	esac
	shift
	shift
done

# check SrcMpegFile
for FNAME in ${SRCNAME[@]}
do
	if [ ! -f $FNAME ] || [ ! -r $FNAME ]; then
		echo -e "$FNAME does not exist or is not readable!" 1>&2
		exit 6
	fi
done

# get pathname for temporary/log files
SUBDIRNAME=`date "+_20%y%m%d%H%M%S"`

# mkdir TMPDIR
TMPDIR=${TMPDIR}/${SUBDIRNAME}
mkdir -p ${TMPDIR}
chmod u+rwx ${TMPDIR}
if [ ! -d $TMPDIR ] || [ ! -r $TMPDIR ] || [ ! -w $TMPDIR ] || [ ! -x $TMPDIR ]; then
	echo -e "Can't read/write/execute ${TMPDIR}" 1>&2
	exit 8
fi

# mkdir LOGDIR
LOGDIR=${LOGDIR}/${SUBDIRNAME}
mkdir -p ${LOGDIR}
chmod u+rwx ${LOGDIR}
if [ ! -d $LOGDIR ] || [ ! -r $LOGDIR ] || [ ! -w $LOGDIR ] || [ ! -x $LOGDIR ]; then
	echo -e "Can't read/write/execute ${LOGDIR}" 1>&2
	exit 10
fi

# checkdir $VOBUDIR
if [ -e $VOBUDIR ] && [ ! -d $VOBUDIR ]; then
	echo -e "$VOBUDIR is not Directory." 1>&2
	exit 12
fi
if [ ! -e $VOBUDIR ]; then
	mkdir -p ${VOBUDIR}
fi
chmod u+rwx ${VOBUDIR}
if [ ! -d $VOBUDIR ] || [ ! -r $VOBUDIR ] || [ ! -w $VOBUDIR ] || [ ! -x $VOBUDIR ]; then
	echo -e "Can't read/write/execute ${VOBUDIR}" 1>&2
	exit 14
fi

# mkdir $DVDDIR
DVDDIR=${DVDDIR}/${SUBDIRNAME}
mkdir -p ${DVDDIR}
chmod u+rwx ${DVDDIR}
if [ ! -d $DVDDIR ] || [ ! -r $DVDDIR ] || [ ! -w $DVDDIR ] || [ ! -x $DVDDIR ]; then
	echo -e "Can't read/write/execute ${DVDDIR}" 1>&2
	exit 16
fi

for FNAME in ${SRCNAME[@]}
do
	BASENAME=`basename $FNAME`
	BASENAME=${BASENAME%.*}
	BASENAME=`date "+%m%d%H%M%S"`${BASENAME}

	replex -k -i PS -t DVD -o "${VOBUDIR}/${BASENAME}.mpeg2" "$FNAME" 2> "${LOGDIR}/${BASENAME}.replex.log"
	
	if [ $? -ne 0 ]; then
		echo -e "Error : replex ${FNAME}." 1>&2
		exit 18
	fi
	chmod u+r ${VOBUDIR}/${BASENAME}.mpeg2
	TIMEENDMPLEX=`date`
done

# authoring 1/2
echo 1>&2
echo "$STARTTIME Started" 1>&2
echo "$TIMEENDMPLEX Now" 1>&2
echo -e 'Authoring...' 1>&2
dvdauthor -t -o ${DVDDIR}/ -v 4:3 -c "0,5:00,10:00,15:00,20:00,25:00,30:00,35:00,40:00,45:00,50:00,55:00,1:00:00,1:05:00,1:10:00,1:15:00,1:20:00,1:25:00,1:30:00,1:35:00,1:40:00,1:45:00,1:50:00,1:55:00,2:00:00,2:5:00,2:10:00,2:15:00,2:20:00,2:25:00,2:30:00,2:35:00,2:40:00,2:45:00,2:50:00,2:55:00,3:00:00,3:05:00,3:10:00,3:15:00,3:20:00,3:25:00,3:30:00,3:35:00,3:40:00,3:45:00,3:50:00,3:55:00,4:00:00" ${VOBUDIR}/*.mpeg2 2> "${LOGDIR}/author1.log"
if [ $? -ne 0 ]; then
	echo -e 'dvdauthor 1/2 : Error' 1>&2
	exit 24
fi
TIMEENDAUTHOR1=`date`

# authoring 2/2
echo 1>&2
echo "$STARTTIME Started" 1>&2
echo "$TIMEENDAUTHOR1 Now" 1>&2
echo -e 'Still authoring...' 1>&2
dvdauthor -T -o ${DVDDIR}/ 2> "${LOGDIR}/author2.log"
if [ $? -ne 0 ]; then
	echo -e 'dvdauthor 2/2 : Error' 1>&2
	exit 26
fi
rm ${VOBUDIR}/*.mpeg2
TIMEENDAUTHOR2=`date`

# check dvd-image-file-size
#du -bs ${DVDDIR} | awk '{ if ( $1 > 4681426944 ) {exit 28 } else {exit 0 } }'
du -bs ${DVDDIR} | awk '{ if ( $1 > 4686004224 ) {exit 28 } else {exit 0 } }'
if [ $? -ne 0 ]; then
	echo -e "Error. ${DVDDIR} is TOO BIG for DVD-VIDEO." 1>&2
	exit 30
fi

# set dvd-drive DMA on
DEVDMA=`/sbin/hdparm $DEVDVD | grep 'using_dma' | awk '{print $3}'`
if [ $DEVDMA -ne 1 ]; then
	/sbin/hdparm -d 1 $DEVDVD
	if [ $? -ne 0 ]; then
		echo -e "Error. Failed to set DMA ON for ${DEVDVD}." 1>&2
		exit 33
	fi
fi

# burn DVD-VIDEO
echo 1>&2
echo "$STARTTIME Started" 1>&2
echo "$TIMEENDAUTHOR2 Now" 1>&2
echo -e 'Burning DVD-VIDEO...' 1>&2
growisofs -dvd-video -udf -speed=${SPEED} -Z /dev/dvd ${DVDDIR}/ 2> "${LOGDIR}/growisofs.log"

if [ $? -eq 0 ]; then
	rm -rf ${TMPDIR}
	echo -e "$CMDNAME Finished!" 1>&2
	echo -e "DVD-Video made." 1>&2
	echo "$STARTTIME Started" 1>&2
	echo "$TIMEENDSEPARATEVIDEO Last video-separating finished" 1>&2
	echo "$TIMEENDSEPARATEAUDIO Last audio-separating finished" 1>&2
	echo "$TIMEENDMPLEX Last replex finished" 1>&2
	echo "$TIMEENDAUTHOR1 Authoring1 finished" 1>&2
	echo "$TIMEENDAUTHOR2 Authoring2 finished" 1>&2
	FINISHTIME=`date`
	echo "$FINISHTIME All finished" 1>&2
	echo "Delete ${LOGDIR} if you don\'t use them." 1>&2
fi

exit $?

