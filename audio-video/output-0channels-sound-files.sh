#!/bin/sh

# 2016 Sep. 08.
# Ryuichi Hashimoto.
# Output filenames which has 0 channels sound-stream.


# get all filenames in the directory.
for avfile in `\find . -maxdepth 1 -type f -name "*.ts" -o -name "*.mp4"`; do
    # Does the file have a sound stream?
    avconvStr=`avconv -i $avfile 2>&1`
    echo $avconvStr | grep -q ': Audio: .*, 0 channels,'

    # in case of no sound
    if [ $? -eq 0  ]; then
        echo $avfile
    fi
done

