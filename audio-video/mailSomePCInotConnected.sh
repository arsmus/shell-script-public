#!/bin/bash
# 2020 Jul. 04.
# 2020 Jun. 24.
# Ryuichi Hashimoto.

# If PCI-device given on arguement is not connected, send mail.


Cmd=${0##*/}

MailTitle='Not_connected_on_PCI-port!'
MailTo=ryu1peace@gmail.com


################
### function ###
################
function usage() {
cat << EOP
usage: $Cmd PCI-DEVICE 
If PCI-device given on arguement is not connected, send mail.
EOP
}


############
### main ###
############
if [ $# -ne 1 ]; then
  usage
  \exit 1
fi

\lspci | \grep -q "$1" > /dev/null 2>&1
Result=$?
if [ ${Result} -eq 0 ]; then 
  # connected
  \exit 1
fi

# mail
\echo "$1 is not connected on PCI-port." | \mail -s `echo -e "${MailTitle}" | \nkf -jM` ${MailTo}
\exit 0

