#!/bin/bash
# 2021 Jan. 24.
# 2020 Oct. 12.
# 2020 Oct. 11.
# Ryuichi Hashimoto.

# Decrypt b25-encrypted-tv-video-ts-files
#   usage: COMMAND DIR [SUFFIX]


CMD=${0##*/}
Suffix='.decrypted'
MediaInfoTxtPath=/tmp/mediainfo.txt

function usage() {
echo "usage: ${CMD} DIR [SUFFIX]"  >&2
echo "Files of which extension is '.ts' are processed." >&2
echo "If no SUFFIX, '.decrypted' is default string." >&2
echo "Decrypt b25-encrypted-tv-video-ts-files in DIR recursively" >&2
echo "Decrypted file is saved in the same directory of encrypted file." >&2
echo "Filename of decrypted file is InfileSUFFIX.ts." >&2
exit 1
}


############
### main ###
############

if [ $# -lt 1 ]; then
  echo "No arguement." >&2
  usage
  exit 1
fi
if [ $# -gt 2 ]; then
  echo "Too many arguements." >&2
  usage
  exit 1
fi

if [ $# -eq 2 ]; then
  Suffix=$2
fi

Dir="${1%/}"
if [ ! -d ${Dir} ]; then
  echo "${Dir} is not directory." >&2
  usage
  exit 1
fi

which mediainfo > /dev/null 2> /dev/null
Result=$?
if [ $Result -ne 0 ]; then
  echo "Please install mediainfo." 1>&2
  exit 1
fi

while read -d $'\0' File; do
  \mediainfo "${File}" > $MediaInfoTxtPath
  # Videoの文字列が含まれる最初の行の行番号を取得
  LineVideo=`cat $MediaInfoTxtPath | grep -n -m 1  Video | sed -e 's/:.*$//'`
  # 先頭行からVideoの文字列が含まれる行までを削除
  cat $MediaInfoTxtPath | sed -e '1,'${LineVideo}'d' > ${MediaInfoTxtPath}-2
  # 最初の空行の行番号を取得
  LineNL=`cat ${MediaInfoTxtPath}-2 | grep -n -P -m 1 "^$"| sed -e 's/:.*$//'`
  # 最初の空行から最終行までを削除しEncrypted文字列の有無を調べる
  cat ${MediaInfoTxtPath}-2 | sed -e ''${LineNL}',$d' | grep -qs 'Encrypted'
  ResultCode=$?
      # ResultCode   0: matched   1: unmatched
  if [ $ResultCode -eq 1 ]; then
    # Encrypted文字列がなければ次のファイル処理に移る
    continue
  fi

  # b25を復号する
  b25 "${File}" "${File}"${Suffix}.ts
  ResultCode=$?
  if [ $ResultCode -ne 0 ]; then
    echo "Failed to decrypt b25 in ${File}." >&2
    if [ -f "${File}"${Suffix}.ts ]; then
      \rm "${File}"${Suffix}.ts
    fi
  fi
  if [ -f "${File}"${Suffix}.ts ]; then
    \rm "${File}"
  fi
done < <(\find -L "${Dir}"/ -name "*.ts" -print0)

if [ -f ${MediaInfoTxtPath} ]; then
  \rm ${MediaInfoTxtPath}*
fi
exit 0

