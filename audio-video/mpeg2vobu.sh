#!/bin/bash
#
# convert from mpeg2withoutVOBU to mpeg2withVOBU
#
# 2006 Aug. 7 - 2009. Aug. 9.
# Ryuichi Hashimoto All Rights Reserved.

# debug mode
DBG='off'
#DBG='on'

if [ "$DBG" = 'on' ]; then
	echo "$*" > "/bigfile/myth/mpeg2vobu1.tmp"
fi

# set values
TMPDIR=/bigfile/tmp/work
LOGDIR=/bigfile/tmp/log
VOBUDIR=/bigfile/tmp/mpeg2
OVERWRITE='off'

CMDNAME=`basename $0`
WA=`whoami`

# check args 
if [ $# -lt 1 ]; then 
	echo "Convert from mpeg2withoutVOBU(s) to mpeg2withVOBU(s).(01)" 1>&2
	echo "Mpeg2-file(with VOBU) is made in [-o] option directory." 1>&2
	echo "New file-name is attached '.mpeg2' extension." 1>&2
	echo "Usage: $CMDNAME -w -t TmpDir -l LogDir -o Mpeg2withVOBU-Dir SrcMpeg2[ ...]" 1>&2
	echo "        -w : Even if same name file exists, overWrite it." 1>&2
	exit 2
fi

# check command line options
while [ "$#" -gt 0 ]; do
	case $1 in
		"-h" | "--help" | "-\?")
			echo "Convert from mpeg2withoutVOBU(s) to mpeg2withVOBU(s). (02)" 1>&2
			echo "Mpeg2-file(with VOBU) is made in [-o] option directory." 1>&2
			echo "New file-name is attached '.mpeg2' extension." 1>&2
			echo "Usage: $CMDNAME -w -t TmpDir -l LogDir -o Mpeg2withVOBU-Dir SrcMpeg2[ ...]" 1>&2
			echo "       -w : Even if same name file exists, overWrite it." 1>&2
			exit 4
			;;
		"-w" | "--overwrite")
			OVERWRITE='on'
			;;
		"-t" | "--tmp")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -t | --tmp" 1>&2
				exit 6
			fi
			TMPVAR=$2
			TMPDIR=${TMPVAR%/}
			TMPDIR2=${TMPDIR%/}
			while  [ $TMPDIR != $TMPDIR2 ]; do
					TMPDIR=$TMPDIR2
					TMPDIR2=${TMPDIR%/}
			done
			shift
			;;
		"-l" | "--log")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -l | --log" 1>&2
				exit 8
			fi
			TMPVAR=$2
			LOGDIR=${TMPVAR%/}
			LOGDIR2=${LOGDIR%/}
			while  [ $LOGDIR != $LOGDIR2 ]; do
					LOGDIR=$LOGDIR2
					LOGDIR2=${LOGDIR%/}
			done
			shift
			;;
		"-o" | "--outdir")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -o | --outdir" 1>&2
				exit 10
			fi
			TMPVAR=$2
			VOBUDIR=${TMPVAR%/}
			VOBUDIR2=${VOBUDIR%/}
			while  [ $VOBUDIR != $VOBUDIR2 ]; do
					VOBUDIR=$VOBUDIR2
					VOBUDIR2=${VOBUDIR%/}
			done
			shift
			;;
		*)
			SRCNAME=($@)
			break
			;;
	esac
	shift
done


if [ "$DBG" = 'on' ]; then
	echo "$*" > "/bigfile/myth/mpeg2vobu2.tmp" 1>&2
fi

# get pathname for temporary/log files
SUBDIRNAME=`date "+_20%y%m%d%H%M%S"`

# mkdir TMPDIR
TMPDIR="${TMPDIR}/${SUBDIRNAME}"
if [ ! -e ${TMPDIR} ]; then
	mkdir -p ${TMPDIR}
fi

# check dir-permition
FUSER=`ls -ld ${TMPDIR} | awk '{print $3}'`
if [ "$WA" = "$FUSER" ]; then
	chmod u+rwx ${TMPDIR}
fi

if [ ! -d $TMPDIR ] || [ ! -r $TMPDIR ] || [ ! -w $TMPDIR ] || [ ! -x $TMPDIR ]; then
	echo -e "Can't read/write/execute ${TMPDIR}" 1>&2
	exit 14
fi


if [ "$DBG" = 'on' ]; then
	echo "$*" > "/bigfile/myth/mpeg2vobu25.tmp" 1>&2
fi

# mkdir LOGDIR
LOGDIR="${LOGDIR}/${SUBDIRNAME}"
if [ ! -e ${LOGDIR} ]; then
	mkdir -p ${LOGDIR}
fi

# check dir-permition
FUSER=`ls -ld ${LOGDIR} | awk '{print $3}'`
if [ "$WA" = "$FUSER" ]; then
	chmod u+rwx ${LOGDIR}
fi

if [ ! -d $LOGDIR ] || [ ! -r $LOGDIR ] || [ ! -w $LOGDIR ] || [ ! -x $LOGDIR ]; then
	echo -e "Can't read/write/execute ${LOGDIR}" 1>&2
	exit 16
fi


if [ "$DBG" = 'on' ]; then
	echo "$*" > "/bigfile/myth/mpeg2vobu30.tmp" 1>&2
fi

# checkdir $VOBUDIR
if [ ! -e $VOBUDIR ]; then
	mkdir -p ${VOBUDIR}
fi
if [ -e $VOBUDIR ] && [ ! -d $VOBUDIR ]; then
	echo -e "$VOBUDIR is not Directory." 1>&2
	exit 18
fi

# check dir-permition
FUSER=`ls -ld ${VOBUDIR} | awk '{print $3}'`
if [ "$WA" = "$FUSER" ]; then
	chmod u+rwx ${VOBUDIR}
fi

if [ ! -d $VOBUDIR ] || [ ! -r $VOBUDIR ] || [ ! -w $VOBUDIR ] || [ ! -x $VOBUDIR ]; then
	echo -e "Can't read/write/execute ${VOBUDIR}" 1>&2
	exit 20
fi

STARTTIME=`date`


if [ "$DBG" = 'on' ]; then
	echo "${SRCNAME[@]}" > "/bigfile/myth/mpeg2vobu60.tmp"
fi

for FNAME in ${SRCNAME[@]}
do

	BASENAME=`basename $FNAME`
	MAINNAME=${BASENAME%.*}

	# check SrcMpegFile
	if [ ! -f $FNAME ]; then
		echo -e "$FNAME does not exist!" 1>&2
		continue
	fi

	# check file-permition
	FUSER=`ls -l $FNAME | awk '{print $3}'`
	if [ "$WA" = "$FUSER" ]; then
		chmod u+rw $FNAME
	fi

	if [ ! -r $FNAME ]; then
		echo -e "$FNAME is not readable!" 1>&2
		continue
	fi

	if [ "$DBG" = 'on' ]; then
		echo "tmpdir :  $TMPDIR   fname : $FNAME  basename : $BASENAME" > "/bigfile/myth/tmp30.${MAINNAME}" 1>&2
	fi

	# if same name file exists, not work.
	if [ $OVERWRITE != 'on' ] && [ -e ${VOBUDIR}/${MAINNAME}.mpeg2 ]; then
		echo -e "${VOBUDIR}/${MAINNAME}.mpeg2 : Same name file exists in target directory." 1>&2
		continue
	fi

	if [ "$DBG" = 'on' ]; then
		echo "tmpdir :  $TMPDIR   fname : $FNAME  basename : $BASENAME" > "/bigfile/myth/tmp40.${MAINNAME}" 1>&2
	fi

	# separate video from mpeg-file
	echo 1>&2
	echo "$STARTTIME Started" 1>&2
	echo -e "Separating video from ${FNAME}..." 1>&2
	mpeg2desc -v0 -o "${TMPDIR}/${MAINNAME}.m2v" < "$FNAME" > /dev/null
	TIMEENDSEPARATEVIDEO=`date`

	if [ "$DBG" = 'on' ]; then
		echo "tmpdir :  $TMPDIR   fname : $FNAME  basename : $BASENAME" > "/bigfile/myth/tmp50.${MAINNAME}" 1>&2
	fi

	# separate audio from mpeg-file
	echo 1>&2
	echo "$STARTTIME Started" 1>&2
	echo "$TIMEENDSEPARATEVIDEO Now" 1>&2
	echo -e "Separating audio from ${FNAME}..." 1>&2
	mpeg2desc -a0 -o "${TMPDIR}/${MAINNAME}.m2a" < "$FNAME" > /dev/null
	TIMEENDSEPARATEAUDIO=`date`
	chmod u+r ${TMPDIR}/${MAINNAME}.m*

	if [ "$DBG" = 'on' ]; then
		echo "tmpdir :  $TMPDIR   fname : $FNAME  basename : $BASENAME" > "/bigfile/myth/tmp60.${MAINNAME}" 1>&2
	fi

	# unify video and audio
	echo 1>&2
	echo "$STARTTIME Started" 1>&2
	echo "$TIMEENDSEPARATEAUDIO Now" 1>&2
	echo -e 'Unifying video and audio to mpeg for DVD-VIDEO...' 1>&2
	echo -e "${FNAME}" 1>&2
	mplex -f 8 -o "${VOBUDIR}/${MAINNAME}.mpeg2" "${TMPDIR}/${MAINNAME}.m2v" "${TMPDIR}/${MAINNAME}.m2a" 2> "${LOGDIR}/${MAINNAME}.mplex.log"

	if [ "$DBG" = 'on' ]; then
		echo "VOBUDIR/MAINNAME ${VOBUDIR}/${MAINNAME} ,  TMPDIR/MAINNAME ${TMPDIR}/${MAINNAME}  " > "/bigfile/tmp/work/on-mplex.tmp" 1>&2
	fi

	if [ $? -ne 0 ]; then
		echo -e "mplex : Error in video audio unifying on ${FNAME}." 1>&2
		exit 22
	fi

	if [ "$DBG" = 'on' ]; then
		echo "tmpdir :  $TMPDIR   fname : $FNAME  basename : $BASENAME" > "/bigfile/myth/tmp70.${MAINNAME}" 1>&2
	fi

	rm ${TMPDIR}/${MAINNAME}.m2v ${TMPDIR}/${MAINNAME}.m2a
	TIMEENDMPLEX=`date`

	if [ "$DBG" = 'on' ]; then
		echo "tmpdir :  $TMPDIR   fname : $FNAME  basename : $BASENAME" > "/bigfile/myth/tmp80.${MAINNAME}" 1>&2
	fi

done

if [ "${TMPDIR}" != "${VOBUDIR}" ]; then
	RSL=${VOBUDIR##$TMPDIR}
	if [ "${RSL}" = "${VOBUDIR}" ]; then
		rm -rf ${TMPDIR}
	fi
fi

echo -e "$CMDNAME Finished!" 1>&2
echo -e "MPEG2-file-with-VOBU made in ${VOBUDIR}/" 1>&2
echo "$STARTTIME Started" 1>&2
echo "$TIMEENDSEPARATEVIDEO Last video-separating finished" 1>&2
echo "$TIMEENDSEPARATEAUDIO Last audio-separating finished" 1>&2
echo "$TIMEENDMPLEX Last mplex finished" 1>&2
echo "Delete ${LOGDIR} if you don't use them." 1>&2


if [ "$DBG" = 'on' ]; then
	echo "100" > "/bigfile/myth/mpeg2vobu100.tmp"
fi

exit $?

