#!/bin/bash
#
# 2015 Jul. 05.
# 2011 May 07.
# ryuichi Hashimoto.
#
# Convert ts-files to H.264-files in current directory.
#   (No arguments)
#

CMDNAME=`basename $0`
FFMPEGCMD='avconv'
FILE1='tmpls1.txt'
FILE2='tmpls2.txt'
N=0

umask 0000

if [ $# -gt 0 ]; then
	echo "Convert ts-files to H.264-files in current directory." 1>&2
	echo "Usage: $CMDNAME" 1>&2
	echo "(No arguments)" 1>&2
	exit 1
fi


ls > $FILE1
ls > $FILE2

# ファイル名を配列に読み込む
mapfile tmpary < $FILE1
for LINELF in "${tmpary[@]}"
do
	# ファイル名末尾の改行コードを取り除く
	LINE=${LINELF%?}

	# timestampが現在時刻のファイルは無視する
	touch tmpnow
	if [ $LINE -ot tmpnow ]; then

		# 拡張子だけを得る
		KAKUCHOSI=${LINE##*.}

		if [ $KAKUCHOSI = 'ts' ] || [ $KAKUCHOSI = 'mp4' ] ; then

			# .mp4ファイルのコーデックがmpeg2の場合は処理対象とする
			if [ $KAKUCHOSI = 'mp4' ] ; then
				( ${FFMPEGCMD} -i $LINE 1> /dev/null ) 2>&1 | egrep ': Video: ' | egrep 'mpeg2video' -q
				if [ $? -ne 0 ]; then
					continue
				fi
			fi

			# ファイル名の最初のピリオド以下を取り除く
			BASETMP=${LINE%%.*}

			# _tss がファイル名語尾にあれば取り除く
			BASESTR=${BASETMP%%_tss}

			# 拡張子を無視したファイル名が検索先ファイルに１つだけの場合 tsファイルをmp4ファイルに変換する
			CNT=`grep -c $BASESTR ${FILE2}`
			if [ $CNT -eq 1 ]; then
				N=$(($N + 1))
				ts2h264avconv.sh $LINE
				echo "Made mp4-file. ${LINE}$n"
			fi
		fi
	fi
done

if [ -e tmpnow ]; then
	rm tmpnow
fi

echo "Finished." 1>&2
echo "$N file(s) done." 1>&2

