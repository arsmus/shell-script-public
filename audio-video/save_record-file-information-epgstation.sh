#!/bin/bash
# 2025 Jan. 18.
# 2021 May 21.
# Ryuichi Hashimoto.

# Get tv-record-file-information from epgstation-db in mysql to save to directory.


Cmd=${0#*/}
DbName=epgstation
DestDir=.
DestFileName=record-file-info-epgstation.txt
ConfigFile="${HOME}/EPGStation/config/config.yml"

################
### function ###
################
function usage() {
cat << EOS
This script gets tv-record-file-information from epgstation-db in mysql to save to directory.
Run this script as the user that runs epgstation.
Enable the user to sudo mysql without password.(visudo)
usage: $cmd -D DBNAME -d DesDir
EOS
return 0
}

############
### main ###
############
# check arguements
if [ $# -ne 4 ]; then
  echo "Error: Number of arguements." 1>&2
  usage 1>&2
  exit 1
fi

# get command-line-arguements
for i in {0..1}
do
  ArgVal1="$1"
  shift
  ArgVal2="$1"
  shift
  case "${ArgVal1}" in
    "-D")
      DbName="${ArgVal2}"
      DbNameTmp=${ArgVal2}
      if [ $DbName != $DbNameTmp ]; then
        echo "Name of database can not have space." 1>&2
        usage 1>&2
        exit 1
      fi
      ;;
    "-d")
      DestDir="${ArgVal2%/}"
      if [ ! -d "$DestDir" ]; then
        echo "$DestDir does not exist." 1>&2
        usage 1>&2
        exit 1
      fi
      if [ ! -w "$DestDir" ]; then
        echo "$DestDir is not writable." 1>&2
        usage 1>&2
        exit 1
      fi
      ;;
    *)
      echo "Fault on arguements." 1>&2
      usage 1>&2
      exit 1
      ;;
  esac
done

# get directory-path of recorded-files
if [ ! -f "$ConfigFile" ]; then
  echo "${ConfigFile} which has directory-path of recorded files does not exist. "
  exit 1
fi
ParentDir=$(cat "$ConfigFile" | grep -P '^[ \t]*path')
ParentDir=${ParentDir#*\'}
ParentDir=${ParentDir%\'*}
ParentDir=${ParentDir%/}
if [ ! -d "${ParentDir}" ]; then
  echo "${ParentDir}(directory-path of recorded files) does not exist. "
  exit 1
fi

# backup an existing file
if [ -f "${DestDir}/${DestFileName}" ]; then
  cp "${DestDir}/${DestFileName}" "${DestDir}/${DestFileName}.old"
fi

# get record-file-information from mysql and
# connect parentDir-column and recorded-filename-column (replace tab to /)
# save to file.
sudo /usr/bin/mysql -D ${DbName} -e "select video_file.id, '${ParentDir}' as parentDirPath, video_file.filePath, video_file.type, video_file.name, video_file.size, video_file.recordedId, recorded.startAt, recorded.endAt, recorded.name, recorded.description, recorded.extended from video_file LEFT OUTER JOIN recorded ON video_file.recordedId = recorded.id" | \
sed "s/\r//g; s/\\\n/　/g; s:${ParentDir}\t*:${ParentDir}/:; s/\t/,/g" > "${DestDir}/${DestFileName}"

exit 0
