#!/bin/bash
# 2018 May 06.
# Ryuichi Hashimoto.

# Convert video to H.264 and AAC codec file.
#   resolution, fps etc. depend on original video.
#   ${Cmd} VideoFile To-Dir


Cmd=`basename $0`

# check number of arguements
if [ $# -lt 1 -o $# -gt 2 ]; then
   echo "Convert video to H.264 and AAC codec file." 1>&2
   echo "  resolution, fps etc. depend on original video." 1>&2
   echo "${Cmd} VideoFile To-Dir" 1>&2
   exit 1
fi

# check and get source media file
Inpath=$1
if [ ! -f $Inpath ]; then
   echo "${Inpath} does not exist." 1>&2
   exit 1
fi
Infile=`basename $Inpath`

echo $Infile

# check and get destination filepath
OutDir=$2
if [ ! -d ${OutDir} ]; then
   echo "${OutDir} does not exist." 1>&2
   exit 1
fi
OutDir=${OutDir%/}
Outpath=${OutDir}/${Infile%.*}.mp4

echo $Outpath

FfmpegCmd='ffmpeg'
FfmpegPresetfile='/usr/local/my/ffmpeg/libx264-ts-good.ffpreset'
CPU_CORES=$(/usr/bin/getconf _NPROCESSORS_ONLN)

${FfmpegCmd} -y -i ${Inpath} -f mp4 -vcodec libx264 -fpre ${FfmpegPresetfile} -bufsize 20000k -maxrate 25000k -acodec aac -absf aac_adtstoasc -strict -2 -threads $CPU_CORES ${Outpath}
exit 0

