#!/bin/bash
# 2021 Jan. 17.
# Ryuichi Hashimoto.
# A script to be runned after recording in chinachu.
# 
# Convert mpeg2-ts-video to H.264 codec of 800x450 and AAC codec.
# Usage: COMMAND
#  Outfile-name : INFILE.mp4.
#  But INFILE(abc.mp4) -> OUTFILE(abc.mp4)
#
# This program uses programs below. 
#   is_b25encrypted.sh
#   ffmpeg
#   tssplitter_lite.
#   get_high-resolution-sids_by_ffmpeg.awk
#
# Change "NewX=MAX-WIDTH-VALUE" in the script to change max width of a converted video.
#
# Filename of converted video is "original video basefilename+SID.mp4"
#
# Put libx264-ts-good.ffpreset in any directory and tell the directory in the script.
##########################################
### libx264-ts-good.ffpreset           ###
### (remove "# " of head of each line) ###
##########################################
# level=41
# crf=25
# coder=1
# flags=+loop
# cmp=+chroma
# partitions=+parti8x8+parti4x4+partp8x8+partb8x8
# me_method=umh
# subq=7
# me_range=16
# g=250
# keyint_min=25
# sc_threshold=40
# i_qfactor=0.71
# b_strategy=1
# qmin=10
# rc_eq=’blurCplx^(1-qComp)’
# bf=16
# bidir_refine=1
# refs=6
##########################################


Cmdname=${0##*/}
Infile=$1
NewX=870
CPU_CORES=$(/usr/bin/getconf _NPROCESSORS_ONLN)
FFMPEGCMD='ffmpeg'
\which ${FFMPEGCMD} 1> /dev/null 2> /dev/null
Result=$?
if [ ${Result} -ne 0 ]; then 
   \echo "Please install ${FFMPEGCMD}." 1>&2
   \exit 1
fi

# FFMPEGpresetfile='/YOUR/FFMPEG/PRESET/DIR/libx264-ts-good.ffpreset'
FFMPEGpresetfile='/usr/local/my/ffmpeg/libx264-ts-good.ffpreset'
if [ ! -f $FFMPEGpresetfile ]; then
  \echo "Please put ${FFMPEGpresetfile}." 1>&2
  exit 1
fi

\which tssplitter_lite 1> /dev/null 2> /dev/null
Result=$?
if [ ${Result} -ne 0 ]; then 
   \echo "Please install tssplitter_lite." 1>&2
   \exit 1
fi

\which is_b25encrypted.sh 1> /dev/null 2> /dev/null
Result=$?
if [ ${Result} -ne 0 ]; then 
   \echo "Please install is_b25encrypted.sh." 1>&2
   \exit 1
fi

\which get_high-resolution-sids_by_ffmpeg.awk 1> /dev/null 2> /dev/null
Result=$?
if [ ${Result} -ne 0 ]; then 
   \echo "Please install get_high-resolution-sids_by_ffmpeg.awk." 1>&2
   \exit 1
fi

############
# function #
############
function usage() {
cat << EOP
Usage: $Cmdname
  Outfile-name : INFILE.mp4.
  But INFILE(abc.mp4) -> OUTFILE(abc.mp4)
  STANDARD-OUT: successfully converted filenames
Convert mpeg2-ts-video to H.264 codec of 800x450 and AAC codec.
EOP
}

############
# function #
############
# get resolution of movie.
getResolut(){
  Resolut=`${FFMPEGCMD} -i ${InfileTmp} 2>&1 | \grep -oP '\d\d+x\d\d+'`
  ResolutX=`\echo $Resolut | \grep -oP '^\d\d+'`
  ResolutY=`\echo $Resolut | \grep -oP '\d\d+$'`
  # x-size of resolution is ResolutX
  # y-size of resolution is ResolutY
  # by the result of above routine.
}

############
# function #
############
getNewResolution(){
  # get resolution for new video.
  getResolut
  # NewX : max length of x-width
  while [[ NewX -gt 100 ]]; do
    NewY=$(( NewX * ResolutY / ResolutX ))
    # NewYが小数点以下を有するかどうかチェック
    NewYReal=`\echo "scale=5; $NewY + 0.00000" | \bc`
    NewY2=`\echo "scale=5; $NewX * $ResolutY / $ResolutX" | \bc`
    if [[ $NewYReal = $NewY2 ]]; then
      # NewYが奇数ではffmpegでの動画変換が失敗する
      if [ `expr $NewY % 2` == 0 ]; then
        # 偶数
        \break
      fi
    fi
    NewX=$(( NewX - 10 ))
  done
  if [[ $NewX -lt 101 ]]; then
    \echo "Could not get the resolution for new video of ${InfileTmp}." 2>&1
    \continue
  fi
  # x-size of resolution for new video is NewX
  # y-size of resolution for new video is NewY
  # by the result of above routine.
}

############
# function #
############
convert2h264(){
  ${FFMPEGCMD} -y -i ${InfileTmp} -f mp4 -vcodec libx264 -fpre ${FFMPEGpresetfile} -r 30000/1001 -aspect 16:9 -s ${NewX}x${NewY} -bufsize 20000k -maxrate 25000k -acodec aac -absf aac_adtstoasc -strict -2 -threads $CPU_CORES $OutfileTmp
  Result=$?
  if [ $Result -eq 0 ]; then
    \echo "${OutfileTmp}"
  else
    if [ -f $OutfileTmp ]; then
      \rm -f $OutfileTmp
    fi
    \echo "Failed H.264 converting." 1>&2
  fi
}

############
### main ###
############

# abort if other ffmpegs are running.
if [ $$ != $(pgrep -fo ffmpeg) ]; then
  # Plural ffmpegs are running.
  echo "Other ffmpegs are running."
  echo "Abort."
  exit 1
fi

# display usage when no args
if [ $# -ne 0 ]; then
  usage 1>&2
  \exit 1
fi

if [ ! -f $Infile ]; then
  \echo "${Infile} does not exist." 1>&2
  \continue
fi

$(is_b25encrypted.sh ${Infile})
ResultCode=$?
if [ $ResultCode -ne 2 ]; then
  echo "Aborted. Maybe ${Infile} is b25-encrypted." 1>&2
  \continue
fi

# Ignore BS-campus-program(*BS11*) because converting this kind of files to mp4 would fail.
# \echo $fname | \grep -q BS11
# Result=$?
# if [[ 0 = $Result ]]; then
#   \continue
# fi

Outfile=${Infile}.mp4

# check if video codec is mpeg2.
( ${FFMPEGCMD} -i $Infile 1> /dev/null ) 2>&1 | \egrep ': Video: ' | \egrep -q 'mpeg2video'
ResultCode=$?
if [ $ResultCode -ne 0 ]; then
  \echo "$Infile is not mpeg2video." 1>&2
  \continue
fi

# get SIDs and convert each SID-program to H.264.
SIDs=`${FFMPEGCMD} -i ${Infile} 2>&1 | \get_high-resolution-sids_by_ffmpeg.awk`
CountSID=`\echo $SIDs | \wc -w`
if [ $CountSID -eq 1 ]; then
  InfileTmp=${Infile}
  getResolut   
  if [ $ResolutX -lt 1000 ]; then
    # In case of small resolution movie, not change resolution
    NewX=$ResolutX
    NewY=$ResolutY
  else
    getNewResolution
  fi
  # convert to H.264
  OutfileTmp="${Infile}-${SIDs}.mp4"
  convert2h264
elif [ $CountSID -gt 1 ]; then
  for SID in $SIDs
  do
    # ts-split
    \tssplitter_lite ${Infile} "${Infile}-${SID}.ts" ${SID}
    InfileTmp="${Infile}-${SID}.ts"
    getResolut   
    if [ $ResolutX -lt 1000 ]; then
      # In case of small resolution movie, not change resolution
      NewX=$ResolutX
      NewY=$ResolutY
    else
      getNewResolution
    fi
    # convert to H.264.
    OutfileTmp="${Infile}-${SID}.mp4"
    convert2h264
  done
else
  echo "${Infile} has no video." 1>&2
  \continue
fi
\exit 0

