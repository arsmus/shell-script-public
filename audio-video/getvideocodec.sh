#!/bin/bash
# 2021 Jan. 03.
# Ryuichi Hashimoto.

# Output codec-type of VideoFile to standard-out.
# mediainfo-package is required.


Cmd=${0##*/}

############
# function #
############
usage() {
cat << EOP
Usage: $Cmd VideoFile
Standard-out: codec-type of VideoFile
mediainfo is required.
EOP
}

########
# main #
########
# check relied softwares
TmpStr=$(which mediainfo)
Result=$?
if [ $Result -ne 0 ]; then
    echo "Please install mediainfo." 1>&2
    \exit 1
fi

# check arguement
if [ $# -ne 1 ]; then
  echo "number of arguements must be 1."  1>&2
  usage
  exit 1
fi
if [ ! -f $1 ]; then
  echo "${1} is not a file."  1>&2
  usage
  exit 1
fi

LineMatchFormat=$(\mediainfo $1 | grep -m 1 'Format')
Result=$?
if [ $Result -ne 0 ]; then
  echo "No Format." 1>&2
  exit 1
fi
echo $LineMatchFormat | sed -e 's/^.*:[ \t][ \t]*//'
exit 0

