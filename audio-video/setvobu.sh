#!/bin/bash
#
# attach VOBU to a mpeg2-file, copy new file to old file
# Caution : original file is to bo deleted
#
# 2006 Oct. 05.
# 2009 Aug. 09.
# Written by Ryuichi Hashimoto.
#

# debug mode
#DBG='on'
DBG='off'

# set values
TMPDIR=/bigfile/tmp/work
LOGDIR=/bigfile/tmp/log
DSTDIR="${PWD}"
OVERWRITE='off'
VOBUFILELOG="setvobufile.log"


# Check if other mpeg2/dvd tools are running, not work (because of free-space of hdd)
fuser `which mpeg2desc` `which mplex` `which replex` `which dvdauthor` `which mkisofs` `which growisofs` `which dvdrecord` 1>&2
if [ $? = 0 ]; then
	echo 'Other mpeg2/dvd tools are running. (mpeg2desc mplex replex dvdauthor mkisofs growisofs dvdrecord)' 1>&2
	echo 'Stopped.' 1>&2
	exit 2
fi

CMDNAME=`basename $0`
WA=`whoami`

# check args
if [ $# -lt 1 ]; then
	echo "Attach VOBU to a mpeg2-file, copy new file to old file.(01)" 1>&2
	echo "Caution : original file is to be deleted" 1>&2
	echo "Must be executed by ROOT-user." 1>&2
	echo "Usage: $CMDNAME -f -t TmpDir -l LogDir -d dstDir SrcMpeg2 [...]" 1>&2
	echo "                -f : Work without past-done-check." 1>&2
	exit 4
fi

# check command line options
while [ "$#" -gt 0 ]; do
	case $1 in
		"-h" | "--help" | "-\?")
		echo "Attach VOBU to a mpeg2-file, copy new file to old file.(01)" 1>&2
			echo "Caution : original file is deleted" 1>&2
			echo "Must be executed by ROOT-user." 1>&2
			echo "Usage: $CMDNAME -f -t TmpDir -l LogDir -d dstDir SrcMpeg2 [...]" 1>&2
			echo "                -f : Work without timestamp-check." 1>&2
			exit 6
			;;
		"-f" | "--force")
			OVERWRITE='on'
			;;
		"-t" | "--tmp")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -t | --tmp" 1>&2
				exit 8
			fi
			TMPVAR=$2
			TMPDIR=${TMPVAR%/}
			TMPDIR2=${TMPDIR%/}
			while  [ $TMPDIR != $TMPDIR2 ]; do
				TMPDIR=$TMPDIR2
				TMPDIR2=${TMPDIR%/}
			done
			shift
			;;
		"-l" | "--log")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -l | --log" 1>&2
				exit 10
			fi
			TMPVAR=$2
			LOGDIR=${TMPVAR%/}
			LOGDIR2=${LOGDIR%/}
			while  [ $LOGDIR != $LOGDIR2 ]; do
				LOGDIR=$LOGDIR2
				LOGDIR2=${LOGDIR%/}
			done
			shift
			;;
		"-d" | "--dst")
			if [ "$#" -lt 2 ]; then
				echo "Error. No value for -d | --dst" 1>&2
				exit 12
			fi
			TMPVAR=$2
			DSTDIR=${TMPVAR%/}
			DSTDIR2=${DSTDIR%/}
			while  [ $DSTDIR != $DSTDIR2 ]; do
				DSTDIR=$DSTDIR2
				DSTDIR2=${DSTDIR%/}
			done
			shift
			;;
		*)
			SRCNAME=($@)
			break
			;;
	esac
	shift
done

# get pathname for temporary nuv files
SUBDIRNAME=`date "+_20%y%m%d%H%M%S"`

# temporary nuv-file dir
TMPDSTDIR="${TMPDIR}/nuv${SUBDIRNAME}"
mkdir -p ${TMPDSTDIR}

# check dir-permition
FUSER=`ls -ld ${TMPDSTDIR} | awk '{print $3}'`
if [ "$WA" = "$FUSER" ]; then
	chmod u+rwx ${TMPDSTDIR}
fi
if [ ! -d $TMPDSTDIR ] || [ ! -r $TMPDSTDIR ] || [ ! -w $TMPDSTDIR ] || [ ! -x $TMPDSTDIR ]; then
	echo -e "Can't read/write/execute ${TMPDSTDIR}" 1>&2
	exit 19
fi

# checkdir $DSTDIR
if [ ! -e $DSTDIR ] || [ ! -d $DSTDIR ]; then
	echo -e "$DSTDIR does not exist or is not directory." 1>&2
	exit 20
fi

# check dir-permition
FUSER=`ls -ld ${DSTDIR} | awk '{print $3}'`
if [ "$WA" = "$FUSER" ]; then
	chmod u+rwx ${DSTDIR}
fi

if [ ! -d $DSTDIR ] || [ ! -r $DSTDIR ] || [ ! -w $DSTDIR ] || [ ! -x $DSTDIR ]; then
	echo -e "Can't read/write/execute ${DSTDIR}" 1>&2
	exit 24
fi

for FNAME in ${SRCNAME[@]}
do
	FSIZE1=`ls -l ${FNAME} | awk '{print $5}'`

	# get filename without directory
	BASENAME=`basename $FNAME`

	# get base-file-name without extension
	MAINNAME=${BASENAME%.*}

	# check SrcMpegFile
	# Source- mpeg2-file permition is to be checked in mpeg2vobu

	# Check SrcFile-extension
	#
	# If below lines are valid, only '.nuv' files are allowed.
	#
	# EXTNAME=${BASENAME##*.}
	# if [ "${EXTNAME}" != 'nuv' ]; then
	#	echo -e "$BASENAME : Extension is not 'nuv'. Skip." 1>&2
	#	continue
	#fi

	# In case OVERWRITE-option is off,
	# if SrcFile is already got processed, not work.
	if [ "${OVERWRITE}" = 'off' ]; then
		grep -q $BASENAME ${DSTDIR}/$VOBUFILELOG
		if [ $? -eq 0 ]; then
			echo "$FNAME : Already done." 1>&2
			continue
		fi	

		# get endtime-of-recording from nuv-filename
		#TMPVAR=${BASENAME#*_*_}
		#RECENDTIME=${TMPVAR%??.nuv}

		# get modification time of FNAME
		# FILEMODIFTIME=`date -d "\`ls -l --full-time ${FNAME} | awk '{print $7" "$8" "$9" "$10}'\`" +%Y%m%d%H%M`

		# if [ $RECENDTIME != $FILEMODIFTIME ]; then
			# echo "$FNAME : Already done." 1>&2
			# continue
		# fi
	fi

	# If SrcMpegFile is in being written, not work.
	FILETIME=`date -r $FNAME +%s | awk '{ printf("%d", $1 / 100) }'`
	NOWTIME=`date +%s | awk '{ printf("%d", $1 / 100) }'`
	if [ $FILETIME -eq $NOWTIME ]; then
		echo -e "$FNAME may be being written!" 1>&2
		continue
	fi

	FSIZE2=`ls -l ${FNAME} | awk '{print $5}'`
	if [ $FSIZE1 -ne $FSIZE2 ]; then
		continue
	fi
	
	# check file-permition
	# file-permition is to be checked in mpeg2vobu

#	mpeg2vobu -t $TMPDIR -l $LOGDIR -o $TMPDSTDIR $FNAME
	mpeg2vobu3 -t $TMPDIR -l $LOGDIR -o $TMPDSTDIR $FNAME
	if [ $? -ne 0 ]; then
		echo "mpeg2vobu : Error" 1>&2
		echo "Process next object..." 1>&2
		continue
	fi

	cp ${TMPDSTDIR}/${MAINNAME}.mpeg2 ${DSTDIR}/${BASENAME}
	if [ $? -ne 0 ]; then
		echo "Error : Copy new mpeg2VOBU file to ${DSTDIR}/." 1>&2
		exit 28
	fi

	rm ${TMPDSTDIR}/${MAINNAME}.mpeg2
	echo ${BASENAME} >> ${DSTDIR}/$VOBUFILELOG
done

rm -rf ${TMPDSTDIR}

