#!/usr/bin/zsh

# radikoの番組表検索

# オリジナル版掲載場所　https://mac-ra.com/shellscript-radiko-timefree/

# 2021 Apr. 25. 変更 Hashimoto Ryuichi.
# 2021 Jan. 17. 変更 Hashimoto Ryuichi.


Cmd=${0##*/}


# https://nlftp.mlit.go.jp/ksj/gml/codelist/PrefCd.html
# 上記ページなどを参照にして、都道府県コードを入力する。
# 東京なら「13」、兵庫なら「28」
# area='13'
area='28'

# 作業場のフォルダへ移動する。radiko-dl.sh の作業場とあわせる
# cd ~/Downloads/radiko
cd /bigdata/02/sound/radio/unsaved

quick='off'
day='today'

# オプションの取得
while [ $# -gt 0 ]
do
    case $1 in
        -i) shift; id="$1" ;;
        -k) shift; keyword="$1" ;;
        -d) shift; day="$1" ;;
        -a) shift; area="$1";;
        -q) quick='on' ;;
        *) break ;;
    esac
    shift
done


################
### function ###
################
# 使い方表示
function usage() {
cat << EOS
radikoの番組表検索
オリジナル版掲載場所　https://mac-ra.com/shellscript-radiko-timefree/

usage: $Cmd -i 放送局ID -k キーワード [-d DAY] [-a AreaCode] [-q] 
　キーワード：番組表にマッチする文言
  DAY: 日付は6桁で。（2019年10月3日 → 191003 みたいな）
  AreaCode: 都道府県番号 (参照 https://nlftp.mlit.go.jp/ksj/gml/codelist/PrefCd.html )
  -q: 検索と同時に録音を開始する
EOS
}


# 放送局ID を表示する関数
function showStID() {
    curl -s https://radiko.jp/v2/station/list/JP"${1}".xml | grep -E '<\/id>|<\/name>' | sed -e 's/ //g' | perl -pe 's/<\/id>\n/ /g' | sed -e 's/<id>//g' | perl -pe 's/<name>//g' | perl -pe 's/<\/name>\n/\n/g'
}


############
### main ###
############
# オプションのチェック
if test -z "$id" || test -z "$keyword"; then
    echo '-i 放送局ID -k キーワード' 1>&2
    echo 'エリアコード：'$area 1>&2
    echo '------------------------------' 1>&2
    showStID $area
    echo '------------------------------' 1>&2
    echo "" 1>&2
    usage
    exit 1
fi

# 日付のチェック
if ! test "$day" = 'today'; then
    if test `echo "${#day}"` -eq 6; then
        day=20${day}
        day='date/'"$day"
    else
        echo '日付は6桁で。（2019年10月3日 → 191003 みたいな）' 1>&2
        exit 1
    fi
fi

# 番組表の取得
prog=`curl -s https://radiko.jp/v3/program/"${day}"/JP"${area}".xml | grep -B 1 "${keyword}" | grep -E 'prog id=|master_id=|title'`

# 番組名、開始時刻、終了時刻の取得
title=`echo "${prog}" | grep -o "<title>[^<]*</title>" | sed -e 's/<[^>]*>//g'`
ft=`echo "$prog" | tr '"' '\n' | grep -A 1 'ft=' | grep -vE 'ft=|--'`
to=`echo "$prog" | tr '"' '\n' | grep -A 1 'to=' | grep -vE 'to=|--'`

# 複数の番組が見つかった場合を想定して for ループで表示。
# -q オプションがついている場合は、そのまま保存に入る。
# あんまり連続したら怒られそうなので「sleep 60」で一分間を置く。必要ないかも。
wl=`echo $title | wc | awk '{print $1}'`

for num in `seq $wl`
do           
    namae=`echo $title | sed -n "$num"p | sed 's/ /_/g'`
    hajimari=`echo $ft | sed -n "$num"p`
    hajimari=${hajimari#20}
    hajimari=${hajimari% *}
    owari=`echo $to | sed -n "$num"p`
    owari=${owari#20}
    owari=${owari% *}


    
    if test "$quick" = 'off'; then
        echo  '-id '"${id}"' -ft '"${hajimari}"' -to '"${owari}"' -n '\'"${namae}"\'' -o OUT-DIR'
    else
	# check radiko-dl.sh is installed
        which radiko-dl.sh 1> /dev/null 2> /dev/null
        ResultCode=$?
        if [ $ResultCode -ne 0 ]; then
            echo "Please install radiko-dl.sh." 1>&2
            exit 2
	fi

        #sh radiko-dl.sh -id "${id}" -ft "${hajimari}" -to "${owari}" -n "${namae}"
        radiko-dl.sh -id "${id}" -ft "${hajimari}" -to "${owari}" -n "${namae}"
        if test $wl -gt $num; then
            sleep 60
        fi
    fi
done

