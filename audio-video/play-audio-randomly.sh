#!/bin/bash
# 2021 Aug. 16.
# 2019 May 16.
# Ryuichi Hashimoto.

# Play an audio file randomly in the directory recursively specified in the argument.

# Synopsis : command DIR

commandpath="$0"

if [ $# -ne 1 ]; then
  echo "${commandpath##*/} DIR" >&2
  exit 1
fi

dir="$1"
if [ ! -d "${dir}" ]; then
  echo "${dir} is not directory." >&2 
  echo "${commandpath##*/} DIR" >&2
  exit 1
fi
dir="${dir%/}" 

countFiles=`find -L "${dir}/" -name '*.mp3' -o -name '*.m4a' -o -name '*.wav' -o -name '*.ogg' -o -name '*.aac' -o -name '*.au' -o -name '*.aif' -o -name '*.iff' -o -name '*.sdn' -o -name '*.svx' | wc -l`
numFile="$(( $RANDOM % $countFiles + 1 ))"
vlc --no-fullscreen `find -L "${dir}/" -name '*.mp3' -o -name '*.m4a' -o -name '*.wav' -o -name '*.ogg' -o -name '*.aac' -o -name '*.au' -o -name '*.aif' -o -name '*.iff' -o -name '*.sdn' -o -name '*.svx' | sed -n ${numFile}p`

