#!/bin/bash
# 2021 Jan. 03.
# Ryuichi Hashimoto
# Output resolution of video

Cmd=${0##*/}

##############
## funcfion ##
##############
function usage() {
\cat << EOP
usage: ${Cmd} FILE
standard-output: resolution of video
EOP
}

##########
## main ##
##########
# check relied softwares
TmpStr=$(which mediainfo)
Result=$?
if [ $Result -ne 0 ]; then
    echo "Please install mediainfo" 1>&2
    \exit 1
fi

# check command-line-arguements
if [ $# -ne 1 ]; then
    usage 1>&2  
    \exit 1
fi
if [ ! -f $1 ]; then
  echo "${1} is not a file."  1>&2
  usage
  exit 1
fi

# check number of streams
Output=$(mediainfo $1 | grep -c '^[ \t]*Video')
if [ $Output -ne 1 ]; then
  echo "Aborted. Num of streams in $1 is not one." 1>&2
  exit 1
fi

# Has video?
LineMatchVideo=$(\mediainfo $1 | grep -m 1 'Video')
Result=$?
if [ $Result -ne 0 ]; then
    echo "No video in ${1}" 1>&2
    exit 1
fi

# get resolution of video
LineMatchWidth=$(\mediainfo $1 | grep -m 1 'Width')
Result=$?
if [ $Result -ne 0 ]; then
    echo "No Width in ${1}" 1>&2
    exit 1
fi
Width=$(echo $LineMatchWidth | sed -e 's/^.*:[ \t][ \t]*//' | sed -e 's/[ \t][ \t]*pixels//')

LineMatchHeight=$(\mediainfo $1 | grep -m 1 'Height')
Result=$?
if [ $Result -ne 0 ]; then
    echo "No Height in ${1}" 1>&2
    exit 1
fi
Height=$(echo $LineMatchHeight | sed -e 's/^.*:[ \t][ \t]*//' | sed -e 's/[ \t][ \t]*pixels//')
echo "${Width}x${Height}"

exit 0

