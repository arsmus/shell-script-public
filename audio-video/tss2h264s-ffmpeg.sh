#!/bin/bash
# 2021 Jan. 26.
# 2020 Oct. 10.
# 2020 Apr. 18.
# 2018 Nov. 23.
# 2018 Jun. 16.
# 2011 May  07.
# ryuichi Hashimoto.
#
# Convert mpeg2-videos to H.264-files in current directory.
#   If there are plural files of almost same filename, no process works.
# ts2h264ffmpeg-wait_another_ffmpeg_finish.sh is required.
# specifications of converted files depend on ts2h264ffmpeg-wait_another_ffmpeg_finish.sh.
# STNDARD-OUT: (depends on ts2h264ffmpeg-wait_another_ffmpeg_finish.sh)


CMDNAME=${0##/}
FFMPEGCMD='ffmpeg'
FILE1='tmpls1.txt'
FILE2='tmpls2.txt'
N=0
## N : counter for processed files

umask 0000


function usage() {
cat << EOP
Usage: $CMDNAME
  Convert mpeg2-videos to H.264-files in current directory.
    If there are plural files of almost same filename, no process works.
  ts2h264ffmpeg-wait_another_ffmpeg_finish.sh is required.
  specifications of converted files depend on ts2h264ffmpeg-wait_another_ffmpeg_finish.sh.
  STNDARD-OUT: (depends on ts2h264ffmpeg-wait_another_ffmpeg_finish.sh)
EOP
}


############
### main ###
############

if [ $# -gt 0 ]; then
  usage 1>&2
  \exit 1
fi


\ls > $FILE1
\ls > $FILE2

## カレントディレクトリ内のファイル名を総当たりしてmpeg2をH.264にする
# カレントディレクトリ内のファイル名を配列tmparyに格納する
\mapfile tmpary < $FILE1
for LINELF in "${tmpary[@]}"
do
  # ファイル名末尾の改行コードを取り除く
  LINE=${LINELF%?}

  # timestampが現在時刻のファイルは無視する
  \touch tmpnow
  if [ ! "${LINE}" -ot tmpnow ]; then
    \continue
  fi

  # ファイルがオープンされていればスキップする
  \lsof | \grep "${LINE}"
  ResultCode=$?
  if [ ${ResultCode} -eq 0 ]; then
    \continue
  fi

  # 拡張子だけを得る
  KAKUCHOSI=${LINE##*.}

  if [ $KAKUCHOSI = 'ts' ] || [ $KAKUCHOSI = 'mp4' ] || [ $KAKUCHOSI = 'm2ts' ] ; then

    # .mp4ファイルのコーデックがmpeg2の場合は処理対象とする
    if [ $KAKUCHOSI = 'mp4' ] ; then
      ( ${FFMPEGCMD} -i $LINE 1> /dev/null ) 2>&1 | \egrep ': Video: ' | \egrep 'mpeg2video' -q
      ResultCode=$?
      if [[ $ResultCode -ne 0 ]]; then
        # mpeg2でない。次のファイルを処理
        \continue
      fi
    fi

    # ファイル名の最初のピリオド以下を取り除く
    BASETMP=${LINE%%.*}

    # _tss がファイル名語尾にあれば取り除く
    BASESTR=${BASETMP%%_tss}

    # 拡張子を無視したファイル名が検索先ファイルに１つだけの場合 tsファイルをmp4ファイルに変換する
    CNT=`\grep -c "$BASESTR" "${FILE2}"`
    if [ $CNT -eq 1 ]; then
      N=$(($N + 1))
      \ts2h264ffmpeg-wait_another_ffmpeg_finish.sh $LINE
    fi
  fi
done

if [ -e tmpnow ]; then
  \rm tmpnow
fi

\echo "Finished." 1>&2
\echo "$N file(s) done." 1>&2

exit 0

