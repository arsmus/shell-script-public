#!/bin/bash
# 2019 Apr. 15.
# 2019 Jan. 26.
# 2018 Nov. 11.
# 2015 Sep. 12.
# 2015 Jul. 22.
# Ryuichi Hashimoto.

# This script sets a reservation of TV-program on recpt1.
# Set at-command to start recording.
# at-time is set 1 minute before the time user set.
# Throw pre-recording-time(sec), channel, duration(sec), rec-mode, out-file-path to at-command.


# set pre-recording time, post-recording time (sec).
declare -i preSec=10
declare -i postSec=5

RECORDER=`which recpt1`
if [ $? -ne 0 ] ; then
  echo 'recpt1 not exist.'
  exit 1
fi

# get recording information from keyboard.
while :
do
  # get TV-channel
  while :
  do
    # channel number in Kobe.
    # NHK-sogo-Kobe->22 NHK-KYOIKU->13 YTV->14 ABC->15 MBS->16 KTV->17 TVO->18 SUN->26 J:COM->24
    # BS1->101 BS-Premium->103 BS日テレ->141 BS朝日->151 BS-TBS->161 BS-Japan->171 BS-Fuji->181
    echo ' NHK-sogo->22 NHK-KYOIKU->13 YTV->14 ABC->15 MBS->16 KTV->17 TVO->18 SUN->26 J:COM->24'
    echo ' BS1->101 BS-Premium->103 BS日テレ->141 BS朝日->151 BS-TBS->161 BS-Japan->171 BS-Fuji->181'
    echo -n 'Enter number (abort->a): ' >&2
    read CHANNEL

    if [ "${CHANNEL}" = 'a' ]; then
      exit 1
    fi

    # 入力チャンネル値が数字であることのチェック
    expect_empty=`echo -n ${CHANNEL} | sed 's/[0-9]//g'`
    if [ "x" = "x${CHANNEL}" ] || [ "x" != "x${expect_empty}" ]; then
      echo 'Enter number only.' >&2
      continue

    # 入力channel値が1-3桁であることをチェック
    elif [ `echo ${#CHANNEL}` -gt 3 ]; then
      echo 'Enter number of 1 digit to 3 digits.' >&2
      continue
    fi
    break
  done

  # get start-time and end-time of recording.
  while :
  do

    # get start-time of recording.
    while :
    do
      nowtime=`date '+%y%m%d%H%M'`
      startDatetime=$((nowtime + 2))

      echo >&2
      echo "Input start datetime. Default:${startDatetime}" >&2
      echo -n "yyMMddhhmm or A(bort): " >&2
      read StrInput

      # aborting
      if [ "${StrInput}" = 'a' -o "${StrInput}" = 'A' ]; then
        exit 1
      fi

      # デフォルト時刻以外が入力された時
      if [ "${StrInput}" != '' ]; then
        nowtime=`date '+%y%m%d%H%M'`

        # 入力年月日時値が数字でなければ再入力
        expect_empty=`echo -n ${StrInput} | sed 's/[0-9]//g'`
        if [ "x" = "x${StrInput}" ] || [ "x" != "x${expect_empty}" ]; then
          echo 'Enter number only.' >&2
          continue

        # 入力年月日時値が10桁でなければ再入力
        elif [ `echo ${#StrInput}` -ne 10 ]; then
          echo 'Enter 10 digit number.' >&2
          continue

        # check future time
        elif [ $((StrInput - nowtime)) -lt 2 ]; then
          echo 'Enter future time after at least 2 minutes from now.' >&2
          continue
        fi
        
        # 録画開始時刻を変数にセットする
        startDatetime=$StrInput
      fi

      # 開始時刻入力完了
      break

    done
    unixStartTime=`utconv.sh 20${startDatetime}00`

    # get end-time of recording.
    while :
    do
      # get default end-time of recording
      unixEndTime=$(( unixStartTime + (60 * 60) ))
      endDatetime=`utconv.sh -r $unixEndTime`
      endDatetime=${endDatetime#20}
      endDatetime=${endDatetime%00}

      echo >&2
      echo "Input end datetime. Default:${endDatetime}" >&2
      echo -n "yyMMddhhmm or A(bort): " >&2
      read StrInput

      # aborting
      if [ "${StrInput}" = 'a' -o "${StrInput}" = 'A' ]; then
        exit 1
      fi

      # デフォルト時刻以外が入力された時
      if [ "${StrInput}" != '' ]; then

        # 入力年月日時値が数字でなければ再入力
        expect_empty=`echo -n ${StrInput} | sed 's/[0-9]//g'`
        if [ "x" = "x${StrInput}" ] || [ "x" != "x${expect_empty}" ]; then
          echo 'Enter number only.' >&2
          continue

        # 入力年月日時値が10桁でなければ再入力
        elif [ `echo ${#StrInput}` -ne 10 ]; then
          echo 'Enter 10 digit number.' >&2
          continue
        fi
        # 録画終了時刻を変数にセットする
        endDatetime=$StrInput
      fi

      # 終了時刻入力完了
      break

    done
    unixEndTime=`utconv.sh 20${endDatetime}00`

    # 録画時間(秒)を算出
    declare -i durationSec
    durationSec=$((unixEndTime - unixStartTime))
    # 録画時間が60秒未満なら再入力
    if [ $durationSec -lt 60 ]; then
      echo "Start: ${startDatetime}" >&2
      echo "End  : ${endDatetime}" >&2
      echo "Duration(sec): ${durationSec}" >&2
      echo "Too short or invalid between start-time and end-time." >&2
      echo "Re-enter." >&2
      continue
    fi
    break
  done

  # 保存ディレクトリの入力
  while :
  do
    saveDir=${PWD}
    echo >&2
    echo -n "Input directory to save (Default:${saveDir}) A(bort): " >&2
    read StrInput

    if [ "${StrInput}" = 'a' -o "${StrInput}" = 'A' ]; then
      exit 1
    fi

    if [ "${StrInput}" != '' ]; then
      saveDir=${StrInput}
    fi
    if [ ! -w "${saveDir}" ]; then
      echo " ${saveDir} not exist or not writable . Re-enter." >&2
      continue
    fi
    break
  done

  # get recording-mode
  while :
  do
    echo >&2
    echo 'Raw ts file -> 0' >&2
    echo 'Diet ts file -> 1' >&2
#    echo 'H.264 file -> 2' >&2
#    echo 'MP3 file -> 3' >&2
    echo -n 'Input encoding mode(0 or 1) or A(bort): ' >&2
    read MODE

    if [ "$MODE" = 'a' -o "$MODE" = 'A' ]; then
      exit 1
    fi

#    if [ "$MODE" = '0' ] || [ "$MODE" = '1' ]  || [ "$MODE" = '2' ] || [ "$MODE" = '3' ];  then
    if [ "$MODE" = '0' ] || [ "$MODE" = '1' ];  then
      break
    fi
    continue
  done

  # create file-name
  # チャンネル番号が2桁の数値なら地上デジタル放送
  if [ ${#CHANNEL} -eq 2 ]; then
    outFile="20${startDatetime}00_20${endDatetime}00GR${CHANNEL}"

  # チャンネル番号が3桁の数値なら衛星放送
  elif [ ${#CHANNEL} -eq 3 ]; then
    outFile="20${startDatetime}00_20${endDatetime}00BS${CHANNEL}"

  else
    echo "Invalid channel : ${CHANNEL}" >&2
    exit 2
  fi
  case "${MODE}" in
    '0') outFile=${outFile}.ts ;;
    '1') outFile=${outFile}_tss.ts ;;
    '2') outFile=${outFile}.mp4 ;;
    '3') outFile=${outFile}.mp3 ;;
    *)   echo "Invalid recording mode : ${MODE}" >&2
          exit 3
          ;;
  esac

  # get full-path of record-file.
  # delete '/' at the end of given dir-str.
  saveDir=`echo ${saveDir} | sed -e "s/\/$//"`
  outFilePath="${saveDir}/${outFile}"

  # Reconfirm recording information
  while :
  do
    echo >&2
    echo ' NHK-sogo->22 NHK-KYOIKU->13 YTV->14 ABC->15 MBS->16 KTV->17 TVO->18 SUN->26 J:COM->24' >&2
    echo ' BS1->101 BS-Premium->103 BS日テレ->141 BS朝日->151 BS-TBS->161 BS-Japan->171 BS-Fujii->181' >&2
    echo "Channel: ${CHANNEL}" >&2

    yy=${startDatetime:0:2}
    mon=${startDatetime:2:2}
    dd=${startDatetime:4:2}
    hh=${startDatetime:6:2}
    min=${startDatetime:8:2}
    echo "Start: 20${yy}/${mon}/${dd} ${hh}:${min}" >&2

    yy=${endDatetime:0:2}
    mon=${endDatetime:2:2}
    dd=${endDatetime:4:2}
    hh=${endDatetime:6:2}
    min=${endDatetime:8:2}
    echo "End: 20${yy}/${mon}/${dd} ${hh}:${min}" >&2

    echo "Duration(sec): ${durationSec}" >&2
    echo "Encoding mode : ${MODE}" >&2
    echo "Output file : ${outFilePath}" >&2
    echo   -n 'OK?(y/n) (abort->a)' >&2
    read ok

    if [ "$ok" = 'a' -o "$ok" = 'A' ]; then
      exit 1
    fi

    if [ "$ok" = 'n' -o "$ok" = 'N' ]; then
      continue 2
    elif [ "$ok" = 'y' -o "$ok" = 'Y' ]; then
      break 2
    else
      continue
    fi
  done
done

# echo 'finished reconfirming.' >&2

# get time before 1 minute to start time
unixTimeBefore1minStart=$((${unixStartTime} - 60))
timeBefore1minStart=`utconv.sh  -r ${unixTimeBefore1minStart}`

yy=${timeBefore1minStart:2:2}
mon=${timeBefore1minStart:4:2}
dd=${timeBefore1minStart:6:2}
hh=${timeBefore1minStart:8:2}
min=${timeBefore1minStart:10:2}

# set at-command to record TV-program
echo "do-pt-tv-record.sh ${preSec} ${postSec} ${CHANNEL} ${durationSec} ${MODE} ${outFilePath}" | at "${hh}:${min} ${dd}.${mon}.${yy}" 
echo 'Reservation is set.' >&2

exit 0

