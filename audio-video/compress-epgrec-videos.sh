#!/bin/bash
# 2021 Jan. 26
# 2020 Oct. 10.
# 2020 Oct. 04.
# 2020 Apr. 19.
# 2020 Apr. 18.
# 2020 Apr. 17.
# 2018 Dec. 09.
# 2018 Dec. 07.
# 2018 Dec. 02.
# 2018 Oct. 14.
# 2017 Sep. 23.
# Ryuichi Hashimoto.

# Convert MPEG2video-files of EPGREC-type-filename in Dir to MPEG4s and delete redundunt files in Dir.
#   STANDARD-OUT: depends on tss2h264-ffmpeg.sh
#   Specifications of converted files: depend on tss2h264-ffmpeg.sh

# EPGREC-type-filename: ^20\d{12}_20\d{12}(GR|BS)CHAN.EXT
#   20170923012300_20170923022200GR22.ts
#   20170923210000_20170923215900BS103.ts

# Programs below that I wrote are required.
#   delredundrecfiles-epgrecfilenamematch.rb
#   Videofile.rb
#   tss2h264s-ffmpeg.sh
#   ispts.sh


CommandName=${0##*/}
DelRedund='delredundrecfiles-epgrecfilenamematch.rb'
Tss='tss2h264s-ffmpeg.sh'

# Set commands that you do not want to run together with this script.
#   If these commands are running, this program is aborted.
# RelatedCommands='PROGRAM1|PROGRAM2'
RelatedCommands='ffmpeg|ts2h264|delredundrecfiles|HandBrakeCLI'

function usage() {
\cat << EOP
usage: ${CommandName} Dir
  Convert MPEG2video-files of EPGREC-type-filename in Dir to MPEG4s and delete r
edundunt files in Dir.
    STANDARD-OUT: depends on tss2h264-ffmpeg.sh
    Specifications of converted files: depend on tss2h264-ffmpeg.sh

  EPGREC-type-filename: ^20\d{12}_20\d{12}(GR|BS)CHAN.EXT
    20170923012300_20170923022200GR22.ts
    20170923210000_20170923215900BS103.ts

  Programs below that I wrote are required.
    delredundrecfiles-epgrecfilenamematch.rb
    Videofile.rb
    tss2h264s-ffmpeg.sh
    ispts.sh
EOP
}


############
### main ###
############
if [ $# -ne 1 ]; then
  usage 1>&2
  \exit 1
fi

FileDir=$1

# convert relative path to absolute path
HeadLetter=${FileDir:0:1}
if [[ '/' != $HeadLetter ]]; then
  # $1 is relative path
  Wd=`pwd`
  FileDir=${Wd}/${FileDir}
fi
FileDir=${FileDir%/}
if [ ! -r ${FileDir} ]; then
  \echo "${FileDir} does not exist or is not readable.." 1>&2
  usage 1>&2
  \exit 1
fi

# Set delete-command of redundent files
\which ${DelRedund} > /dev/null
Result=$?
if [ $Result -gt 0 ]; then
  \echo "${DelRedund} is not installed." 1>&2
  \exit 1
fi
DelRedundCom="nohup ${DelRedund} -d ${FileDir} < /dev/null > ${FileDir}/delredund-stdout.log 2> ${FileDir}/delredund-stderr.log &"

# Set H264-convert-command
\which ${Tss} > /dev/null
Result=$?
if [ $Result -gt 0 ]; then
  \echo "${Tss} is not installed." 1>&2
  \exit 1
fi
TssCom="nohup ${Tss} < /dev/null > tss2h264s-stdout.log 2> tss2h264s-stderr.log &"

# Run deleteRedundent-file-command/convert-file-command if related commands are not running.
\ispts.sh | \grep -E "${RelatedCommands}"
Ispt="$?"
   # Ispt 0: related commands are running.
   #      1: related commands are not running.

if [ 1 -eq ${Ispt} ]; then 
  cd ${FileDir}
  LastFinishedCmd=`\ls -lt delredund-std* tss2h264s-std* | \head -n 1 | \awk '{print $9}'`
  case ${LastFinishedCmd} in
     *tss*)
       \eval ${DelRedundCom} ;;
     *)
       \eval ${TssCom} ;;
  esac
  \exit 0
else
   \echo "Related commands are running." 1>&2
   \echo "Aborted." 1>&2
   \exit 1
fi
\exit 0

