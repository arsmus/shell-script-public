#!/bin/bash
# 2021 Sep. 02.
# 2021 Aug. 16.
# 2021 Jan. 10.
# 2019 Jun. 23.
# 2018 May  03.
# 2017 Oct. 29.
# 2017 Sep. 24.
# 2017 Sep. 17.
# Ryuichi Hashimoto.

# Play an audio-video file randomly in the directory recursively specified in the argument.

# Usage: command DIR

commandpath="$0"

if [ $# -ne 1 ]; then
  echo "${commandpath##*/} DIR" >&2
  exit 1
fi

dir="$1"
if [ ! -d "${dir}" ]; then
  echo "${dir} is not directory." >&2 
  echo "${commandpath##*/} DIR" >&2
  exit 1
fi

if [ '/' != ${dir: -1} ]; then
  dir="${dir}/" 
fi

countFiles=`find -L "${dir}" -name '*.mp4' -o -name '*.webm' -o -name '*.m2ts' -o -name '*.ogg' -o -name '*.wmv' -o -name '*.m4v' -o -name '*.flv' -o -name '*.ts' -o -name '*.avi' -o -name '*.mp3' -o -name '*.wav' -o -name '*.aac' -o -name '*.m4a' -o -name '*.au' -o -name '*.aif' -o -name '*.iff' -o -name '*.sdn'  -o -name '*.svx' | wc -l`
numFile="$(( $RANDOM % $countFiles + 1 ))"
vlc --no-fullscreen `find -L "${dir}" -name '*.mp4' -o -name '*.webm' -o -name '*.m2ts' -o -name '*.ogg' -o -name '*.wmv' -o -name '*.m4v' -o -name '*.flv' -o -name '*.ts' -o -name '*.avi' -o -name '*.mp3' -o -name '*.wav' -o -name '*.aac' -o -name '*.m4a' -o -name '*.au' -o -name '*.aif' -o -name '*.iff' -o -name '*.sdn'  -o -name '*.svx' | sed -n ${numFile}p`

