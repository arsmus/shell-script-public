#!/bin/bash
#
# 2016 May  06.
# 2010 Aug. 12.
# Ryuichi Hashimoto.
#
# convert mpeg2 to H.264.
#

FFMPEGCMD='avconv'
CPU_CORES=$(/usr/bin/getconf _NPROCESSORS_ONLN)
# FFMPEG_DATADIR=/usr/local/my/ffmpeg
# export FFMPEG_DATADIR

# check args
if [ $# -lt 1 ]; then
	echo 'No source video-file.'
	exit
fi

SRCS=($@)

for FNAME in ${SRCS[@]}
do
	$FFMPEGCMD -i $FNAME 2>&1 | egrep 'Video: mpeg2video' > /dev/null
	if [ $? -ne 0 ]; then
		echo "${FNAME} is not mpeg2-video-file."
	else

    ${FFMPEGCMD} -y -i  ${FNAME} -f mp4 -c:v libx264 -preset medium -bufsize 20000k -maxrate 25000k -c:a libmp3lame -ac 2 -b:a 192k -threads $CPU_CORES ${FNAME}.mp4

		if [ $? -ne 0 ]; then
			echo "Failed converting from mpeg2 to H.264."
			rm ${FNAME}.mp4
		else
			mv ${FNAME} ${FNAME}.mpg
		fi
	fi
done

