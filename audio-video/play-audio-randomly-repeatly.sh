#!/bin/bash
# 2021 Feb. 18.
# Ryuichi Hashimoto.

# Repeat play-audio-randomly.sh the number of times specified on the arguement.


Cmd=${0##*/}

################
### function ###
################
function usage() {
\cat << EOS
usage: $Cmd Dir Number
  Dir: Directory which audio-files are saved
  Number: Number of repeat of play-audio-randomly.sh

Repeat play-audio-randomly.sh the number of times specified with Number.
EOS
}

############
### main ###
############
# check arguement
if [ $# -ne 2 ]; then
  usage
  \exit 1
fi

# get media directory
MediaDir="$1"
if [ ! -d "$MediaDir" ]; then
  \echo "$MediaDir does not exist." 1>&2 
  usage
  \exit 1
fi

NumLoop="$2"
# is arguement number?
\expr "$NumLoop" + 1 1> /dev/null 2> /dev/null
ResultCode=$?
if [ $ResultCode -gt 1 ]; then
  \echo "Please input number on 2nd arguement." 1>&2
  usage
  \exit 1
fi

# check dependency
\which play-audio-randomly.sh 1> /dev/null 2> /dev/null
ResultCode=$?
if [ $ResultCode -ne 0 ]; then
  \echo "Please install play-audio-randomly.sh from https://bitbucket.org/arsmus/shell-script-public/src/master/audio-video/play-audio-randomly.sh." 1>&2
  \exit 1
fi

# play audio repeatly
CountLoop=0
while [ $CountLoop -lt $NumLoop ]; do
  CountLoop=$(\expr $CountLoop + 1)
  \play-audio-randomly.sh "$MediaDir"
done
exit 0

