#!/bin/bash
# 2021 Apr. 25.
# 2021 Feb. 04.
# 2020 Oct. 14.
# 2020 Sep. 12.
# 2019 Jul. 08.
# 2019 Jul. 07.
# 2019 Mar. 16.

# This script was downloaded from https://gist.github.com/matchy2/9515cecbea40918add594203dc61406c
# This script is introduced at https://tech.matchy.net/archives/241 (まっちーさんのテックよりなBlog)
# Ryuichi Hashimoto modified this script with https://qiita.com/je3kmz/items/59669b1ceae39fc33594


# COMMAND channel_name duration(minuites) [outputdir] [prefix]


Cmdname=${0##*/}

pid=$$
date=`date "+%Y%m%d%H%M"`
outdir="."


# function
usage() {
cat << EOP
Usage: $Cmdname channel_name duration(minuites) [outputdir] [prefix]
A program to record NHK-radio-program
EOP
}


############
### main ###
############
if [ $# -le 1 ]; then
  usage 1>&2
  exit 1
fi

if [ $# -ge 2 ]; then
  channel=$1

  # Duration(sec)
  expr "$2" + 1 > /dev/null 2>&1
  if [ $? -gt 1 ]; then
    echo "Error. Second parameter must be number." 1>&2
    usage
    exit 1
  fi
  DURATION=`expr $2 \* 60`
fi
if [ $# -ge 3 ]; then
  outdir=$3
  outdir=${outdir%/}
  if [ ! -d ${outdir} ]; then 
    echo "Error. Third parameter must be existing directory." 1>&2
    usage
    exit 1
  fi
fi
PREFIX=${channel}
if [ $# -ge 4 ]; then
  PREFIX=$4
fi

#
# set channel
#
case $channel in
  "NHK1")
    aspx="https://nhkradioakr1-i.akamaihd.net/hls/live/511633/1-r1/1-r1-01.m3u8"
    ;;
  "NHK2")
    aspx="https://nhkradioakr2-i.akamaihd.net/hls/live/511929/1-r2/1-r2-01.m3u8"
    ;;
  "FM")
    aspx="https://nhkradioakfm-i.akamaihd.net/hls/live/512290/1-fm/1-fm-01.m3u8"
    ;;
  "NHK1_SAPPORO")
    aspx="https://nhkradioikr1-i.akamaihd.net/hls/live/512098/1-r1/1-r1-01.m3u8"
    ;;
  "NHK2_SAPPORO")
    aspx="https://nhkradioakr2-i.akamaihd.net/hls/live/511929/1-r2/1-r2-01.m3u8"
    ;;
  "FM_SAPPORO")
    aspx="https://nhkradioikfm-i.akamaihd.net/hls/live/512100/1-fm/1-fm-01.m3u8"
    ;;
  "NHK1_SENDAI")
    aspx="https://nhkradiohkr1-i.akamaihd.net/hls/live/512075/1-r1/1-r1-01.m3u8"
    ;;
  "NHK2_SENDAI")
    aspx="https://nhkradioakr2-i.akamaihd.net/hls/live/511929/1-r2/1-r2-01.m3u8"
    ;;
  "FM_SENDAI")
    aspx="https://nhkradiohkfm-i.akamaihd.net/hls/live/512076/1-fm/1-fm-01.m3u8"
    ;;
  "NHK1_NAGOYA")
    aspx="https://nhkradiockr1-i.akamaihd.net/hls/live/512072/1-r1/1-r1-01.m3u8"
    ;;
  "NHK2_NAGOYA")
    aspx="https://nhkradioakr2-i.akamaihd.net/hls/live/511929/1-r2/1-r2-01.m3u8"
    ;;
  "FM_NAGOYA")
    aspx="https://nhkradiockfm-i.akamaihd.net/hls/live/512074/1-fm/1-fm-01.m3u8"
    ;;
  "NHK1_OSAKA")
    aspx="https://nhkradiobkr1-i.akamaihd.net/hls/live/512291/1-r1/1-r1-01.m3u8"
    ;;
  "NHK2_OSAKA")
    aspx="https://nhkradioakr2-i.akamaihd.net/hls/live/511929/1-r2/1-r2-01.m3u8"
    ;;
  "FM_OSAKA")
    aspx="https://nhkradiobkfm-i.akamaihd.net/hls/live/512070/1-fm/1-fm-01.m3u8"
    ;;
  "NHK1_HIROSHIMA")
    aspx="https://nhkradiofkr1-i.akamaihd.net/hls/live/512086/1-r1/1-r1-01.m3u8"
    ;;
  "NHK2_HIROSHIMA")
    aspx="https://nhkradioakr2-i.akamaihd.net/hls/live/511929/1-r2/1-r2-01.m3u8"
    ;;
  "FM_HIROSHIMA")
    aspx="https://nhkradiofkfm-i.akamaihd.net/hls/live/512087/1-fm/1-fm-01.m3u8"
    ;;
  "NHK1_MATSUYAMA")
    aspx="https://nhkradiozkr1-i.akamaihd.net/hls/live/512103/1-r1/1-r1-01.m3u8"
    ;;
  "NHK2_MATSUYAMA")
    aspx="https://nhkradioakr2-i.akamaihd.net/hls/live/511929/1-r2/1-r2-01.m3u8"
    ;;
  "FM_MATSUYAMA")
    aspx="https://nhkradiozkfm-i.akamaihd.net/hls/live/512106/1-fm/1-fm-01.m3u8"
    ;;
  "NHK1_FUKUOKA")
    aspx="https://nhkradiolkr1-i.akamaihd.net/hls/live/512088/1-r1/1-r1-01.m3u8"
    ;;
  "NHK2_FUKUOKA")
    aspx="https://nhkradioakr2-i.akamaihd.net/hls/live/511929/1-r2/1-r2-01.m3u8"
    ;;
  "FM_FUKUOKA")
    aspx="https://nhkradiolkfm-i.akamaihd.net/hls/live/512097/1-fm/1-fm-01.m3u8"
    ;;
  *)
    echo "failed channel" 1>&2
    exit 1
    ;;
esac

# ffmpeg -loglevel quiet -y -t ${DURATION} -i ${aspx} -acodec libmp3lame -ab 128k "${outdir}/${date}_${PREFIX}.mp3"
ffmpeg -y -i ${aspx} -t ${DURATION} -codec copy "${outdir}/${date}_${PREFIX}.m4a"

exit 0

