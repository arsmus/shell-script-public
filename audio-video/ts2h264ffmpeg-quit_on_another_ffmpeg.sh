#!/bin/bash
# 2022 Jan. 02.
# 2021 Mar. 20.
# 2021 Feb. 20.
# 2021 Feb. 09.
# 2021 Jan. 26.
# 2020 Apr. 19.
# 2020 Jan. 12.
# 2019 Jul. 20.
# 2019 Jun. 22.
# 2018 Dec. 02.
# 2018 Nov. 23.
# 2010 Jun. 11.
# Ryuichi Hashimoto.

# Convert mpeg2-ts-video to H.264 codec-video of about 800x450 and AAC codec.
# Usage: COMMAND INFILE
#  INFILE codec type : MPEG2-file."
#  Outfile-name : INFILE.mp4.
#  But INFILE(abc.mp4) -> OUTFILE(abc.mp4)
#  OUTFILE is put in the same directory of INFILE.
#  Message of SUCCESS or FAIL is outputted to STANDARD-OUT
#  INFILE is not deleted.
#
# This program uses programs below. 
#   ffmpeg
#   tssplitter_lite
#   get_high-resolution-sids_by_ffmpeg.awk
#   mediainfo
#   is_b25encrypted.sh
#   isMpeg2.sh
#
# Change "NewX=MAX-WIDTH-VALUE" in the script to change max width of a converted video.
#
# Filename of converted video is "original video basefilename+SID.mp4"
#
# Put libx264-ts-good.ffpreset in any directory and tell the directory in the script.


Cmdname=${0##*/}
CmdPath="$0"
DestDir="_none_"
NewX=870
CPU_CORES=$(/usr/bin/getconf _NPROCESSORS_ONLN)
# make ffmpeg use cpu less
if [ $CPU_CORES -gt 1 ]; then
  CPU_CORES=$(( ${CPU_CORES}/2 ))
fi

FFMPEGCMD='ffmpeg'
# FFMPEGpresetfile='/YOUR/FFMPEG/PRESET/DIR/libx264-ts-good.ffpreset'
FFMPEGpresetfile='/usr/local/my/ffmpeg/libx264-ts-good.ffpreset'

# check ffmpeg-configuration-file
if [ ! -f "${FFMPEGpresetfile}" ]; then
  echo "Please put ${FFMPEGpresetfile}" 1>&2
  echo "##########################################"
  echo "### libx264-ts-good.ffpreset           ###"
  echo "### (remove "# " of head of each line) ###"
  echo "##########################################"
  echo "# level=41"
  echo "# crf=25"
  echo "# coder=1"
  echo "# flags=+loop"
  echo "# cmp=+chroma"
  echo "# partitions=+parti8x8+parti4x4+partp8x8+partb8x8"
  echo "# me_method=umh"
  echo "# subq=7"
  echo "# me_range=16"
  echo "# g=250"
  echo "# keyint_min=25"
  echo "# sc_threshold=40"
  echo "# i_qfactor=0.71"
  echo "# b_strategy=1"
  echo "# qmin=10"
  echo "# rc_eq=’blurCplx^(1-qComp)’"
  echo "# bf=16"
  echo "# bidir_refine=1"
  echo "# refs=6"
  echo "##########################################"
fi

# check depended programs are installed
\which ${FFMPEGCMD} 1> /dev/null 2> /dev/null
ReturnCode=$?
if [ ${ReturnCode} -ne 0 ]; then
   \echo "Please install ${FFMPEGCMD}." 1>&2
   \echo "Failed_converting"
   \exit 1
fi
\which tssplitter_lite 1> /dev/null 2> /dev/null
ResuldCode=$?
if [ ${ResuldCode} -ne 0 ]; then 
   \echo "Please install tssplitter_lite." 1>&2
   \echo "Failed_converting"
   \exit 1
fi
\which get_high-resolution-sids_by_ffmpeg.awk 1> /dev/null 2> /dev/null
ResuldCode=$?
if [ ${ResuldCode} -ne 0 ]; then 
   \echo "Please install get_high-resolution-sids_by_ffmpeg.awk from https://bitbucket.org/arsmus/shell-script-public/src/master/audio-video/get_high-resolution-sids_by_ffmpeg.awk" 1>&2
   \echo "Failed_converting"
   \exit 1
fi
\which isMpeg2.sh 1> /dev/null 2> /dev/null
ResuldCode=$?
if [ ${ResuldCode} -ne 0 ]; then 
   \echo "Please install isMpeg2.sh from https://bitbucket.org/arsmus/shell-script-public/src/master/audio-video/isMpeg2.sh" 1>&2
   \echo "Failed_converting"
   \exit 1
fi
\which is_b25encrypted.sh 1> /dev/null 2> /dev/null
ResuldCode=$?
if [ ${ResuldCode} -ne 0 ]; then 
   \echo "Please install is_b25encrypted.sh from https://bitbucket.org/arsmus/shell-script-public/src/master/audio-video/is_b25encrypted.sh" 1>&2
   \echo "Failed_converting"
   \exit 1
fi


############
# function #
############
function usage() {
cat << EOS
Usage: $Cmdname [-d DestDir] INFILE
-d: destination directory. Default: same directory as INFILE-directory

Convert mpeg2-ts-video to H.264 codec-video of about 800x450 and AAC codec.
  INFILE codec type : MPEG2-file.
  Outfile-name : INFILE.mp4.
  But INFILE(abc.mp4) -> OUTFILE(abc.mp4)
  STANDARD-OUT: successfully converted new filepath
  INFILE is not deleted.
EOS
}

############
# function #
############
# get resolution of movie.
function getResolut(){
  Resolut=`${FFMPEGCMD} -i "${InfileTmpPath}" 2>&1 | \grep -oP '\d\d+x\d\d+'`
  ResolutX=`\echo $Resolut | \grep -oP '^\d\d+'`
  ResolutY=`\echo $Resolut | \grep -oP '\d\d+$'`
  # x-size of resolution is ResolutX
  # y-size of resolution is ResolutY
  # by the result of above routine.
}

############
# function #
############
function getNewResolution(){
  # get resolution for new video.
  getResolut
  # NewX : max length of x-width
  while [[ NewX -gt 100 ]]; do
    NewY=$(( NewX * ResolutY / ResolutX ))
    # NewYが小数点以下を有するかどうかチェック
    NewYReal=`\echo "scale=5; $NewY + 0.00000" | \bc`
    NewY2=`\echo "scale=5; $NewX * $ResolutY / $ResolutX" | \bc`
    if [[ $NewYReal = $NewY2 ]]; then
      # NewYが奇数ではffmpegでの動画変換が失敗する
      if [ `expr $NewY % 2` == 0 ]; then
        # 偶数
        \break
      fi
    fi
    NewX=$(( NewX - 10 ))
  done
  if [[ $NewX -lt 101 ]]; then
    \echo "Could not get the resolution for new video of ${InfileTmpPath}." 1>&2
    \echo "Failed_converting_${InfileTmpPath}"
    \exit 1
  fi
  # x-size of resolution for new video is NewX
  # y-size of resolution for new video is NewY
  # by the result of above routine.
}

############
# function #
############
function convert2h264(){
  ${FFMPEGCMD} -y -i "${InfileTmpPath}" -f mp4 -vcodec libx264 -fpre ${FFMPEGpresetfile} -r 30000/1001 -aspect 16:9 -s ${NewX}x${NewY} -bufsize 20000k -maxrate 25000k -acodec aac -absf aac_adtstoasc -strict -2 -threads $CPU_CORES "$OutfilePath" 1> /dev/null 2> /dev/null
  ResultCode=$?
  if [ $ResultCode -ne 0 ]; then
    if [ -f "${OutfilePath}" ]; then
      \rm -f "${OutfilePath}"
    fi
    ResultConvertCode=1
  else
    ResultConvertCode=0
  fi
}

############
### main ###
############
# output usage to std-err when no args
if [ $# -lt 1 ] || [ $# -gt 3 ]; then
  echo "Number of arguements is illegal." 1>&2
  usage 1>&2
  \echo "Failed_converting"
  \exit 1
fi

# abort if another ffmpeg is running
##   If there is a process that Both PID and GID are different from this script's, another process of same name as this process is running.
##   Abort if another process of same name as this process is running.

# get GID of this process
ProcList=$(ps ax -o pid,ppid,pgid)
PidLines=$(echo "${ProcList}" | grep $$)
Gid=$(echo "$PidLines" | awk -v Pid=$$ '{if ($1 == Pid) print $3}')
# get PID that might be another process whose name is same as this script
OtherPid=$(pgrep -fo "$CmdPath")
# If OtherPid and Gid are different from this script's, another process of this script is running.
if [ $$ -ne $OtherPid ]; then
  OtherGid=$(echo "$ProcList" | awk -v Pid=$OtherPid '{if ($1 == Pid) print $3}')
  if [ $OtherGid -ne $Gid ]; then
    echo "Another ${CmdPath} is running." 1>&2
    echo "Aborted." 1>&2
    exit 1
  fi
fi

# get command-line-arguements
while [ $# -gt 0 ]; do
  if [ "$1" = "-d" ]; then
    if [ $# -ne 3 ]; then
      \echo "Number of arguements is illegal." 1>&2
      usage 1>&2
      \echo "Failed_converting"
      \exit 1
    fi
    shift
    DestDir="$1"
    if [ ! -d "$DestDir" ]; then
      \echo "${DestDir} does not exist." 1>&2
      \echo "Failed_converting"
      \exit 1
    fi
    DestDir="${DestDir%/}"
  else
    if [ $# -gt 1 ]; then
      \echo "Arguements are too many." 1>&2
      usage 1>&2
      \echo "Failed_converting"
      \exit 1
    fi
    InfilePath="$1"
    if [ ! -f "$InfilePath" ]; then
      \echo "${InfilePath} does not exist." 1>&2
      \echo "Failed_converting"
      \exit 1
    fi
  fi
  shift
done

# set Destination Directory
if [ "${DestDir}" = "_none_" ]; then
  if [[ "${InfilePath}" =~ / ]]; then
    # get parent path
    DestDir="${InfilePath%/*}"
  else
    # arguement filenamePath does not have parent directory
    DestDir=$(pwd)
    DestDir="${DestDir%/}"
  fi
fi

# skip if InfilePath is already opened.
CountLsof=0
## loop while InfilePath is opened
while [ ${CountLsof} -lt 5 ]; do
  \lsof  1> /dev/null 2> /dev/null | \grep "${InfilePath}" 1> /dev/null 2> /dev/null
  ResultCode=$?
  if [ $ResultCode -ne 0 ]; then
    # InfilePath is not opened
    # able to continue process
    \break
  fi
  sleep 5
  CountLsof=$(expr $CountLsof + 1)
done
if [ $CountLsof -gt 3 ]; then
  # InfilePath is opened
  \echo "${InfilePath} has been already opened."
  \echo "Failed_converting_${InfilePath}"
  exit 1
fi

## check if video codec is mpeg2.
isMpeg2.sh "${InfilePath}" 1> /dev/null 2> /dev/null
ResultCode=$?
if [ $ResultCode -ne 0 ]; then
  \echo "${InfilePath} is not mpeg2video." 1>&2
  \echo "Failed_converting_${InfilePath}"
  \exit 1
fi

## check if file is encrypted.
is_b25encrypted.sh "${InfilePath}" 1> /dev/null 2> /dev/null
ResultCode=$?
if [ $ResultCode -ne 2 ]; then
  \echo "${InfilePath} is encrypted." 1>&2
  \echo "Failed_converting_${InfilePath}"
  \exit 1
fi

# get SIDs and convert each SID-program to H.264.
SIDs=$(${FFMPEGCMD} -i "${InfilePath}" 2>&1 | \get_high-resolution-sids_by_ffmpeg.awk 2> /dev/null )
CountSID=`\echo $SIDs | \wc -w`
if [ $CountSID -eq 1 ]; then
  InfileTmpPath="${InfilePath}"
  getResolut
  if [ $ResolutX -lt 1000 ]; then
    # In case of small resolution movie, not change resolution
    NewX=$ResolutX
    NewY=$ResolutY
  else
    getNewResolution
  fi
  # convert to H.264
  OutfilePathTmp="${DestDir}/${InfilePath##*/}"
  OutfilePathTmp="${OutfilePathTmp%.*}"
  OutfilePath="${OutfilePathTmp}-${SIDs}.mp4"
  convert2h264
    # result is set in ResultConvertCode.
  if [ ${ResultConvertCode} -eq 0 ];then
    \echo "${OutfilePath}"
  else
    \echo "convert2h264 function failed."
    \echo "Failed_converting_${InfileTmpPath}"
  fi
elif [ $CountSID -gt 1 ]; then
  for SID in $SIDs
  do
    # ts-split
    \tssplitter_lite "${InfilePath}" "${InfilePath%.*}-${SID}.m2ts" ${SID}
    InfileTmpPath="${InfilePath%.*}-${SID}.m2ts"
    getResolut
    if [ $ResolutX -lt 1000 ]; then
      # In case of small resolution movie, not change resolution
      NewX=$ResolutX
      NewY=$ResolutY
    else
      getNewResolution
    fi
    # convert to H.264.
    OutfilePathTmp="${DestDir}/${InfilePath##*/}"
    OutfilePathTmp="${OutfilePathTmp%.*}"
    OutfilePath="${OutfilePathTmp}-${SIDs}.mp4"
    convert2h264
      # result is set in ResultConvertCode.
    if [ $ResultConvertCode -eq 0 ];then
      \echo "${OutfilePath}"
    else
      \echo "convert2h264 function failed."
      \echo "Failed_converting_${InfileTmpPath}"
    fi
  done
else
  \echo "${InfilePath} has no video." 1>&2
  \echo "Failed_converting_${InfilePath}"
  \exit 1
fi

\exit 0

