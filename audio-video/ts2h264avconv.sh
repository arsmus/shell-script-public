#!/bin/bash
#
# 2016 Jul. 03.
# 2016 Feb. 15.
# 2010 Jun. 11.
# Ryuichi Hashimoto.

# Convert mpeg2-movie to H.264 codec.
#	Convert mpeg2-ts-video to H.264(x264)-video.
#	Usage: $cmdname INFILE [...]
#	  INFILE Type : Mpeg2-file.
#	  Outfile-name : INFILE.mp4.
#	  But INFILE(abc.mp4) -> OUTFILE(abc.mp4)
#

cmdname=`basename $0`
CPU_CORES=$(/usr/bin/getconf _NPROCESSORS_ONLN)
FFMPEGCMD='avconv'


# function : Get one program from movie-file.
tssplit()
{
	if [ ${TYPE} = "BS" -a ${CHANNEL} = "101" ]; then
		tssplitter_lite ${infile} ${infile}_part.ts ${CHANNEL}
		if [ $? -eq 0 -a -s ${infile}_part.ts ]; then
			rm -f ${infile}
			echo "NHK-BS1(101) partial split done." 1>&2
		else
			mv -f ${infile} ${infile}_part.ts
			echo "NHK-BS1(101) partial split failed. Remains full." 1>&2
		fi
	elif [ ${TYPE} = "BS" -a ${CHANNEL} = "102" ]; then
		tssplitter_lite ${infile} ${infile}_part.ts ${CHANNEL}
		if [ $? -eq 0 -a -s ${infile}_part.ts ]; then
			rm -f ${infile}
			echo "NHK-BS2(102) partial split done." 1>&2
		else
			mv -f ${infile} ${infile}_part.ts
			echo "NHK-BS2(102) partial split failed. Remains full." 1>&2
		fi
	else

		# Get sid
		SID=`${FFMPEGCMD} -i ${infile} 2>&1 | grep Program | head -n 1 | sed -e 's/^ *//' | cut -d " " -f2`
		tssplitter_lite ${infile} ${infile}_part.ts $SID
		if [ $? -eq 0 -a -s ${infile}_part.ts ]; then
			rm -f ${infile}
			echo "Partial split done." 1>&2
		else
			mv -f ${infile} ${infile}_part.ts
			echo "Partial split failed. Remains full." 1>&2
		fi
	fi
}


# function
#    count programs in movie-file.
#     and
#    split programs.
countprogsplit()
{
	# count SIDs
	NUMSID=`${FFMPEGCMD} -i ${infile} 2>&1 | grep Program | wc -l`

	if [ $NUMSID -lt 1 ]; then
		mv -f ${infile} ${infile}_part.ts
		echo 'No programs in movie-file.' 1>&2
	elif [ $NUMSID -eq 1 ]; then
		mv -f ${infile} ${infile}_part.ts
		echo 'No need for split.' 1>&2
	else
		tssplit
	fi
}


##
##
## main
##
##

# display usage when no args
if [ $# -lt 1 ]; then
	echo "Convert mpeg2-ts-video to H.264(x264)-video. (01)" 1>&2
	echo "Usage: $cmdname INFILE [...]" 1>&2
	echo "  INFILE Type : Mpeg2-file." 1>&2
	echo "  Outfile-name : INFILE.mp4." 1>&2
	echo "  But INFILE(abc.mp4) -> OUTFILE(abc.mp4)" 1>&2
	exit
fi

# get filenames in array
infiles=($@)

for fname in ${infiles[@]}
do

	if [ ! -f $fname ]; then
		echo "$fname does not exist." 1>&2
		continue
	fi

	infile=$fname
	outfile=${infile}.mp4

	# check if video codec is mpeg2
	( ${FFMPEGCMD} -i $infile 1> /dev/null ) 2>&1 | egrep ': Video: ' | egrep 'mpeg2video' -q
	if [ $? -ne 0 ]; then
		echo "$infile is not mpeg2video." 1>&2
		continue
	fi


	# get tv-type & tv-channel
	# GRound digital?
	TYPE='0'
	CHANNEL='0'
	isgr=`expr  $infile : .*GR`
	if [ $isgr -gt 1 ]; then
		TYPE='GR'
		tmpChannel=${infile##*GR}
		CHANNEL=${tmpChannel%%.*}
	fi

	# BS digital?
	isbs=`expr  $infile : .*BS`
	if [ $isbs -gt 1 ]; then
		TYPE='BS'
		tmpChannel=${infile##*BS}
		CHANNEL=${tmpChannel%%.*}
	fi

	if [ $TYPE = '0' ]; then
		echo "Unknown broad-type." 1>&2
		continue
	fi

	if [ $CHANNEL = '0' ]; then
		echo "Unknown channel." 1>&2
		continue
	fi


	# count programs in movie-file.
	# split programs.
	countprogsplit


	## convert to H.264.
	#while [ `ps ax | grep ffmpeg | grep -vc grep` -gt 2 ];
	#do
		#sleep 300
	#done

	# FFMPEG_DATADIR=/usr/local/my/ffmpeg
	# export FFMPEG_DATADIR
	# ffmpeg -y -i ${infile}_part.ts -f mp4 -vcodec libx264 -vpre ts-good -r 30000/1001 -aspect 16:9 -s 800x450 -bufsize 20000k -maxrate 25000k -acodec copy -absf aac_adtstoasc -threads $CPU_CORES $outfile

	${FFMPEGCMD} -y -i ${infile}_part.ts -f mp4 -c:v libx264 -preset medium -r 30000/1001 -aspect 16:9 -s 800x450 -bufsize 20000k -maxrate 25000k -acodec copy -threads $CPU_CORES $outfile
#	${FFMPEGCMD} -y -i ${infile}_part.ts -f mp4 -c:v libx264 -preset medium -r 30000/1001 -aspect 16:9 -s 800x450 -bufsize 20000k -maxrate 25000k -c:a libmp3lame -ac 2 -b:a 192k -threads $CPU_CORES $outfile

	if [ $? -eq 0 ]; then
		savefile=${outfile/.mp4.mp4/.mp4}
		mv -f $outfile $savefile
		echo "Converted to H.264 : $savefile" 1>&2
	else
		if [ -f $outfile ]; then
			rm -f $outfile
		fi
		echo "Failed H.264 converting." 1>&2
	fi
done

