#!/bin/sh

# 2020 Apr. 18.
# 2018 May  06.
# 2015 Sep. 19.
# Ryuichi Hashimoto.

# loop ispt.sh N-times.
#   return code
#     0: related commands are running
#     1: related commands are not running


# set loop-count
NumLoop=5

# set default-retrun-code.
Result=1

while [ $NumLoop -gt 0 ]; do
  ispt.sh
    # return code of ispt.sh
    #   0: related commands are running
    #   1: related commands are not running

  if [ 0 -eq $? ]; then
    # when some commands are running
    Result=0
  fi
  \sleep 1
  NumLoop=$(($NumLoop - 1))
done
\exit $Result

