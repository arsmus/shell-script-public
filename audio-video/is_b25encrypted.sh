#!/bin/bash
# 2021 Jan. 24.
# 2021 Jan. 03.
# Ryuichi Hashimoto.

# check video is encrypted with arib std-b25.
# mediainfo-package is required.


Cmd=${0##*/}

############
# function #
############
usage() {
cat << EOP
Usage: $Cmd VideoFile
result code
  2: video is not encrypted
  3: video is encrypted
mediainfo is required.
EOP
}

########
# main #
########
# check relied softwares
TmpStr=$(which mediainfo)
Result=$?
if [ $Result -ne 0 ]; then
    echo "Please install mediainfo." 1>&2
    \exit 1
fi

# check arguement
if [ $# -ne 1 ]; then
  echo "number of arguements must be 1."  1>&2
  usage
  exit 1
fi
if [ ! -f $1 ]; then
  echo "${1} is not a file."  1>&2
  usage
  exit 1
fi

# mediainfoで暗号化有無を確認する。
# mediainfo出力では、decryptされた動画でもTextセクションにEncryptedも文字が入っていることがあるので、Textセクションを取り除いた上で、Encrypted文字列を検索する
\mediainfo $1 | \
awk 'NR==1,/Text/ {print $0}' | \
grep -m 1 'Encrypted'
Result=$?
if [ $Result -ne 0 ]; then
  # video is not encrypted
  exit 2
else
  # video is encrypted
  exit 3
fi
exit 0

