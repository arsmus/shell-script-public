#!/bin/bash
# 2021 Jan. 30.
# Ryuichi Hashimoto.

# check video is encrypted or not.
# mediainfo-package is required.


Cmd=${0##*/}

############
# function #
############
usage() {
cat << EOP
Usage: $Cmd VideoFile
result code
  0: video is encrypted.
  1: error
  2: video is not encrypted.
mediainfo is required.
EOP
}

########
# main #
########
# check relied softwares
TmpStr=$(which mediainfo)
Result=$?
if [ $Result -ne 0 ]; then
    echo "Please install mediainfo." 1>&2
    \exit 1
fi

# check arguement
if [ $# -ne 1 ]; then
  echo "number of arguements must be 1."  1>&2
  usage
  exit 1
fi
if [ ! -f $1 ]; then
  echo "${1} is not a file."  1>&2
  usage
  exit 1
fi

# mediainfoで暗号化有無を確認する。
# mediainfo出力のTextセクションにはdecryptされていてもEncrypedが残るので扱わないようにする
mediainfo $1 | sed -n '1,/Text/p' | sed -n '/Encryption/p' | grep -q 'Encrypted'
ResultCode=$?
if [ $ResultCode -eq 0 ]; then
  # the file is encrypted
  exit 0
else
  # the file is not encrypted
  exit 2
fi
exit 1

