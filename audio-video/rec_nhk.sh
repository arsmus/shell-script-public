#!/bin/bash
# from https://gist.github.com/matchy256/9515cecbea40918add594203dc61406c

pid=$$
date=`date '+%Y-%m-%d-%H_%M'`
outdir="."

if [ $# -le 1 ]; then
  echo "usage : $0 channel_name duration(minuites) [outputdir] [prefix]"
  exit 1
fi

if [ $# -ge 2 ]; then
  channel=$1
  DURATION=`expr $2 \* 60 + 15`
fi
if [ $# -ge 3 ]; then
  outdir=$3
fi
PREFIX=${channel}
if [ $# -ge 4 ]; then
  PREFIX=$4
fi

#
# set channel
#
case $channel in
  "NHK1")
    aspx="https://radio-stream.nhk.jp/hls/live/2023229/nhkradiruakr1/master48k.m3u8"
    ;;
  "NHK2")
    aspx="https://radio-stream.nhk.jp/hls/live/2023501/nhkradiruakr2/master48k.m3u8"
    ;;
  "FM")
    aspx="https://radio-stream.nhk.jp/hls/live/2023507/nhkradiruakfm/master48k.m3u8"
    ;;
  "NHK1_SAPPORO")
    aspx="https://radio-stream.nhk.jp/hls/live/2023545/nhkradiruikr1/master48k.m3u8"
    ;;
  "FM_SAPPORO")
    aspx="https://radio-stream.nhk.jp/hls/live/2023546/nhkradiruikfm/master48k.m3u8"
    ;;
  "NHK1_SENDAI")
    aspx="https://radio-stream.nhk.jp/hls/live/2023543/nhkradiruhkr1/master48k.m3u8"
    ;;
  "FM_SENDAI")
    aspx="https://radio-stream.nhk.jp/hls/live/2023544/nhkradiruhkfm/master48k.m3u8"
    ;;
  "NHK1_NAGOYA")
    aspx="https://radio-stream.nhk.jp/hls/live/2023510/nhkradiruckr1/master48k.m3u8"
    ;;
  "FM_NAGOYA")
    aspx="https://radio-stream.nhk.jp/hls/live/2023511/nhkradiruckfm/master48k.m3u8"
    ;;
  "NHK1_OSAKA")
    aspx="https://radio-stream.nhk.jp/hls/live/2023508/nhkradirubkr1/master48k.m3u8"
    ;;
  "FM_OSAKA")
    aspx="https://radio-stream.nhk.jp/hls/live/2023509/nhkradirubkfm/master48k.m3u8"
    ;;
  "NHK1_HIROSHIMA")
    aspx="https://radio-stream.nhk.jp/hls/live/2023512/nhkradirufkr1/master48k.m3u8"
    ;;
  "FM_IROSHIMA")
    aspx="https://radio-stream.nhk.jp/hls/live/2023513/nhkradirufkfm/master48k.m3u8"
    ;;
  "NHK1_MATSUYAMA")
    aspx="https://radio-stream.nhk.jp/hls/live/2023547/nhkradiruzkr1/master48k.m3u8"
    ;;
  "FM_MATSUYAMA")
    aspx="https://radio-stream.nhk.jp/hls/live/2023548/nhkradiruzkfm/master48k.m3u8"
    ;;
  "NHK1_FUKUOKA")
    aspx="https://radio-stream.nhk.jp/hls/live/2023541/nhkradirulkr1/master48k.m3u8"
    ;;
  "FM_FUKUOKA")
    aspx="https://radio-stream.nhk.jp/hls/live/2023542/nhkradirulkfm/master48k.m3u8"
    ;;
  *)
    echo "failed channel"
    exit 1
    ;;
esac

# MP3に変換する場合
ffmpeg -loglevel quiet -y -t ${DURATION} -i ${aspx} -acodec libmp3lame -ab 128k "${outdir}/${PREFIX}_${date}.mp3"
#avconv -loglevel quiet -y -t ${DURATION} -i ${aspx} -acodec mp3 -ab 128k "${outdir}/${PREFIX}_${date}.mp3"

# AACでとりだす場合
#ffmpeg -loglevel quiet -y -t ${DURATION} -i ${aspx} -vn -acodec copy -bsf:a aac_adtstoasc "${outdir}/${PREFIX}_${date}.m4a"
#ffmpeg -y -t ${DURATION} -i ${aspx} -vn -c:a aac -ar 48000 -ab 192k -ac 2 "${outdir}/${PREFIX}_${date}.aac"

exit 0

