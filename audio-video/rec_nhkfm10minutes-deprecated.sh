#!/bin/bash
#
# 2016 Apr. 29.
# 2011 Sep. 19.
# Ryuichi Hashimoto.
#
# Record NHK-FM radio.
#

# 録画時間(seconds)
RTIME=600

# 録音出力ファイル
OFILE='/bigdata/radio/20160429nhkfm.flv'

sleep 5

rtmpdump -B ${RTIME} --rtmp "rtmpe://netradio-fm-flash.nhk.jp" --playpath 'NetRadio_FM_flash@63343' --app "live" -W http://www3.nhk.or.jp/netradio/files/swf/rtmpe.swf --live -o $OFILE

#rtmpdump -B ${RTIME} --rtmp "rtmpe://netradio-fm-flash.nhk.jp" --playpath 'NetRadio_FM_flash@63343' -W http://www3.nhk.or.jp/netradio/files/swf/rtmpe.swf -v -q -o $OFILE

