#!/bin/bash
# 2022 Jan. 09.
# Ryuichi Hashimoto.
#

Cmd="${0##*/}"

################
### function ###
################
function usage() {
cat << EOP
usage: "$Cmd" FILE1 FILE2
Output the files that exist in only one of the two files.
EOP
}

function get_files_on_one_side() {

  if [ $# -ne 2 ]; then
    echo "Number of arguements of get_files_on_one_side() must be 2."
    exit 1
  fi

  local File1="$1"
  local File2="$2"

  # set all files in $File1 to an array
  Stems=($(\cat "$File1" | grep -Po "/bigdata/01/[^ \,\"].*" | sed -e "s/\..*$//" | sed -e "s:/bigdata/.*/::"))

  # find files which do not exist in $File2
  for Stem in "${Stems[@]}"; do
    \cat "$File2" | grep -q ${Stem}
    RetValue=$?
    if [ $RetValue -ne 0 ]; then
      echo "$File1 : ${Stem}"
    fi
  done
}

############
### main ###
############
# check command-line-arguements
if [ $# -ne 2 ]; then
  \echo "Number of command-line-arguements must be 2."
  usage
  \exit 1
fi
if [ ! -f "$1" ] || [ ! -r "$1" ]; then
  \echo "$1 does not exist or is not readable."
  \exit 1
fi
if [ ! -f "$2" ] || [ ! -r "$2" ]; then
  \echo "$2 does not exist or is not readable."
  \exit 1
fi

# get files which exist in only one file.
get_files_on_one_side "$1" "$2"

exit 0

