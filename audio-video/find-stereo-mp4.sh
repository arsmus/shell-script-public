#!/bin/bash
# 2016 Feb. 11.
# Ryuichi Hashimoto.

mp4Dir="/bigdata/tv"

for file in `\find $mp4Dir -type f -name '*.mp4' `; do
  outStr=`avconv -i $file 2>&1 `
  # num=`echo $outStr | egrep -c "Audio.* stereo,"`
  num=`echo $outStr | grep -c "stereo"`
  if [ $num -gt 0 ]; then
    ls -l --block-size=M $file
  fi
done

