#!/bin/bash
# 2020 Jun. 24.
# Ryuichi Hashimoto.

# Check if PCI-device given on arguement is connected


Cmd=${0##*/}


################
### function ###
################
function usage() {
cat << EOP
Check if PCI-device given on arguement is connected.
usage: ${Cmd} PCI-DEVICE
Result code
 0: pci-device is connected
 1: pci-device is not connected
EOP
}


############
### main ###
############
if [ $# -ne 1 ]; then
  usage
  exit 1
fi

\lspci | \grep -q "$1"
Result=$?
if [ $Result -ne 0 ]; then
  # no pci-device
  \exit 1
else
  # pci-device is connected
  \exit 0
fi

