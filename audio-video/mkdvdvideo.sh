#!/bin/bash
#
# make dvd-video from mpeg2-file
#
# 2006 Jul. 8.
#
# Originally from  http://www.macmil.co.jp/~macmil/linux015.htm
#
#


# set values
TMPDIR=/home/bigdata/tmp
DVDDIR=/home/bigdata/dvd
LOGDIR=/home/bigdata/log

# check args 
if [ $# -ne 1 ]; then 
	CMDNAME=`basename $0`
	echo -e "Usage: $CMDNAME SrcMpeg2File"
	exit 1
fi

# check SrcMpegFile
if [ ! -f $1 ] || [ ! -r $1 ]; then
	echo -e "$1 does not exist or is not readable!"	
	exit 2
fi

# check $TMPDIR directory
if [ ! -d $TMPDIR ] || [ ! -r $TMPDIR ] || [ ! -w $TMPDIR ]; then
	echo -e "Make ${TMPDIR} directory and set mode 777"
	exit 3
fi

LS_RSLT=`ls -1 ${TMPDIR} 2> /dev/null`
if [ ${#LS_RSLT} -ne 0 ]; then
	echo "File(s) exist in $TMPDIR"
	exit 4
fi

# check $DVDDIR directory
if [ ! -d $DVDDIR ] || [ ! -r $DVDDIR ] || [ ! -w $DVDDIR ]; then
	echo -e "Make ${DVDDIR} directory and set mode 777"	
	exit 5
fi

LS_RSLT=`ls -1 ${DVDDIR} 2> /dev/null`
if [ ${#LS_RSLT} -ne 0 ]; then
	echo -e "File(s) exist in $DVDDIR"
	exit 6
fi

# get filename for temporary files
SUBNAME=`date "+%Y%H%M%S"`
BASENAME=`basename $1`
BASENAME=${BASENAME%.*}
BASENAME=${BASENAME:=${SUBNAME}}

# check $LOGDIR directory
if [ ! -d $LOGDIR ] || [ ! -r $LOGDIR ] || [ ! -w $LOGDIR ]; then
	echo -e "Make ${LOGDIR} directory and set mode 777"	
	exit 7
fi

# mkdir SUB-LOGDIR
LOGDIR=${LOGDIR}/${BASENAME}
mkdir ${LOGDIR}
chmod 777 ${LOGDIR}
if [ ! -d $LOGDIR ] || [ ! -r $LOGDIR ] || [ ! -w $LOGDIR ]; then
	echo -e "Can't Make ${LOGDIR} directory or not read/write ${LOGDIR}"	
	exit 9
fi

# separate video from mpeg-file
STARTTIME=`date`
echo
echo $STARTTIME
echo -e 'Separating audio from mpeg...'
mpeg2desc -v0 -o "${TMPDIR}/${BASENAME}.m2v" < "$1"

# separate audio from mpeg-file
echo
echo `date`
echo -e 'Separating video from mpeg...'
mpeg2desc -a0 -o "${TMPDIR}/${BASENAME}.m2a" < "$1"

chmod 666 ${TMPDIR}/${BASENAME}.m*

# unify video and audio
echo
echo `date`
echo -e 'Unifying video and audio to mpeg for DVD-VIDEO...'
mplex -f 8 -o "${TMPDIR}/${BASENAME}.mpg" "${TMPDIR}/${BASENAME}.m2v" "${TMPDIR}/${BASENAME}.m2a" 2> "${LOGDIR}/${BASENAME}.mplex.log"
if [ $? -ne 0 ]; then
	echo -e 'mplex : Error in video audio unifying'
	exit 11
fi

chmod 666 ${TMPDIR}/${BASENAME}.m*

# authoring 1/2
echo
echo `date`
echo -e 'Authoring...'
dvdauthor -o ${DVDDIR}/ -v 4:3 -c "0,5:00,10:00,15:00,20:00,25:00,30:00,35:00,40:00,45:00,50:00,55:00,1:00:00,1:05:00,1:10:00,1:15:00,1:20:00,1:25:00,1:30:00,1:35:00,1:40:00,1:45:00,1:50:00,1:55:00,2:00:00,2:5:00,2:10:00,2:15:00,2:20:00,2:25:00,2:30:00,2:35:00,2:40:00,2:45:00,2:50:00,2:55:00,3:00:00,3:05:00,3:10:00,3:15:00,3:20:00,3:25:00,3:30:00,3:35:00,3:40:00,3:45:00,3:50:00,3:55:00,4:00:00" "${TMPDIR}/${BASENAME}.mpg" 2> "${LOGDIR}/${BASENAME}.author1.log"
if [ $? -ne 0 ]; then
	echo -e 'dvdauthor 1/2 : Error'
	exit 12
fi

# authoring 2/2
echo
echo `date`
echo -e 'Still authoring...'
dvdauthor -T -o ${DVDDIR}/ 2> "${LOGDIR}/${BASENAME}.author2.log"
if [ $? -ne 0 ]; then
	echo -e 'dvdauthor 2/2 : Error'
	exit 13
fi

# burn DVD-VIDEO
echo
echo `date`
echo -e 'Burning DVD-VIDEO...'
growisofs -dvd-video -udf -Z /dev/dvd ${DVDDIR}/ 2> "${LOGDIR}/${BASENAME}.growisofs.log"

if [ $? -eq 0 ]; then
#	rm     ${TMPDIR}/*
#	rm -rf ${DVDDIR}/*
	echo -e "mkdvd : $1 to DVD-Video Finished!"
	echo $STARTTIME
	FINISHTIME=`date`
	echo $FINISHTIME
	echo "Delete ${TMPDIR}/${BASENAME}/*, ${DVDDIR}/* if you don't use them."
fi

exit $?
