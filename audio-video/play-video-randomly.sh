#!/bin/bash
# 2017 Apr. 15.
# 2017 Apr. 08.
# 2017 Feb. 25.
# Ryuichi Hashimoto.

# Play an video file randomly in the directory recursively specified in the argument.

# Synopsis : command DIR

commandpath="$0"

if [ $# -ne 1 ]; then
  echo "${commandpath##*/} DIR" >&2
  exit 1
fi

dir="$1"
if [ ! -d "${dir}" ]; then
  echo "${dir} is not directory." >&2 
  echo "${commandpath##*/} DIR" >&2
  exit 1
fi

if [ '/' != ${dir: -1} ]; then
  dir="${dir}/" 
fi

countFiles=`find "${dir}" -name '*.mp4' -o -name '*.flv' -o -name '*.ts' -o -name '*.mpg' -o -name '*.mpeg' | wc -l`
numFile="$(( $RANDOM % $countFiles + 1 ))"
vlc --no-fullscreen `find "${dir}" -name '*.mp4' -o -name '*.flv' -o -name '*.ts' -o -name '*.mpg' -o -name '*.mpeg' | sed -n ${numFile}p`

