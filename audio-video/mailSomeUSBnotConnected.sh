#!/bin/bash
# 2020 Jul. 04.
# 2020 Jun. 24.
# Ryuichi Hashimoto.

# If USB-device given on arguement is not connected, send mail.


Cmd=${0##*/}

MailTitle='Not_connected_on_USB-port!'
MailTo=ryu1peace@gmail.com


################
### function ###
################
function usage() {
cat << EOP
usage: $Cmd USB-DEVICE 
If USB-device given on arguement is not connected, send mail.
EOP
}


############
### main ###
############
if [ $# -ne 1 ]; then
  usage
  \exit 1
fi

\lsusb | \grep -q "$1" > /dev/null 2>&1
Result=$?
if [ ${Result} -eq 0 ]; then 
  # USB device is connected
  \exit 1
fi

# mail
\echo "$1 is not connected on USB-port." | \mail -s `echo -e "${MailTitle}" | \nkf -jM` ${MailTo}
\exit 0

