#!/bin/sh

# 2016 Sep. 06.
# 2016 Sep. 05.
# Ryuichi Hashimoto.
# Output filenames which has no sound-stream.


# get all filenames in the directory.
for avfile in `\find . -maxdepth 1 -type f -name "*.ts" -o -name "*.mp4"`; do
    # Does the file have a sound stream?
    avconvStr=`avconv -i $avfile 2>&1`
    echo $avconvStr | grep -q ': Audio:'

    # in case of no sound
    if [ $? -ne 0  ]; then
        echo $avfile
    fi
done

