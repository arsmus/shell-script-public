#!/bin/bash
#
# 2010 Aug. 12.
# Ryuichi Hashimoto.
#
# convert mpeg2 to H.264.
#

CPU_CORES=$(/usr/bin/getconf _NPROCESSORS_ONLN)
FFMPEG_DATADIR=/usr/local/my/ffmpeg
export FFMPEG_DATADIR

# check args
if [ $# -lt 1 ]; then
	echo 'No original file.'
	exit
fi

SRCS=($@)

for FNAME in ${SRCS[@]}
do
	ffmpeg -i $FNAME 2>&1 | egrep 'Video: mpeg2video' > /dev/null
	if [ $? -ne 0 ]; then
		echo "${FNAME} is not mpeg2-video-file."
	else
		ffmpeg -y -i ${FNAME} -f mp4 -vcodec libx264 -vpre ts-good03 -r 30000/1001 -bufsize 20000k -maxrate 25000k -acodec libmp3lame -ac 2 -ab 128k -ar 48000 -threads ${CPU_CORES} ${FNAME}.mp4
		if [ $? -ne 0 ]; then
			echo "Failed converting from mpeg2 to H.264."
			rm ${FNAME}.mp4
		else
			mv ${FNAME} ${FNAME}.mpg
		fi
	fi
 done

