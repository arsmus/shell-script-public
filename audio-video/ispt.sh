#!/bin/bash
#
# 2021 Mar. 25.
# 2020 Sep. 22.
# 2020 Jun. 30.
# 2019 Dec. 31.
# 2018 Jun. 29.
# 2018 May  16.
# 2016 Mar. 21 07:32.
# 2010 Apr. 29.
# Written by Ryuichi Hashimoto.
#
# Check if recpt1, epgrec, etc are running.
#   retrun code
#     0: related commands are running
#     1: related commands are not running


CMDS='b25|dvbv5-zap|rec-|rec_|shepherd.php|sheepdog|collie|HandBrake|getepg|epgdump|do-record|recpt1|ffmpeg|avconv|tssplitter_lite|ts2h264|delredundrecfiles|grive|list_all-info_rec-files'
ps ax -f | grep -v grep | grep -E ${CMDS}
RESULT="$?"
if [ $RESULT -ne 0 ];then
   exit 1
fi
exit 0

