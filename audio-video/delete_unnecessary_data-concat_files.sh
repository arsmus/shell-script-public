#!/bin/bash
# 2022 Jan. 15.
# 2022 Jan. 10.
# 2022 Jan. 02.
# Ryuichi Hashimoto.
# Refer "function usage()" to know about this software.


\set -o noclobber
Cmd=${0##*/}

################
### function ###
################
function usage() {
\cat << EOP
usage: "${Cmd}"

Description:
  Update files of recorded-TV-program information of epgstation, epgrec, chinachu.
  Concatnate files of recorded-TV-program information of epgstation, epgrec, chinachu to 1 file.
EOP
}

############
### main ###
############
RootDir=/bigdata/01/record-file-info
RecInfoFilepath="${RootDir}/record-file-info.txt"
SrcInfoFilepath1="${RootDir}/epgrec/record-file-info-epgrec20210517.txt"
SrcInfoFilepath2="${RootDir}/chinachu/record-file-info-chinachu20210417.txt"
SrcInfoFilepath3="${RootDir}/epgstation210418/record-file-info-epgstation.txt"

# Information-files of epgrec, chinachu contains unwated tv-filepath strings.
# To remove these strings, put these files into an array. 
# Do not edit SrcInfoFilepath3. This file is edited by EpgStation.
SrcInfoFilepaths=("${SrcInfoFilepath1}" "${SrcInfoFilepath2}")

# check command line
if [ $# -gt 0 ]; then
  usage
  \exit 1
fi

# check file permission and backup files
if [ ! -x "${RootDir}" ]; then
  \echo "${RootDir} is not executable." 2>&1
  \exit 1
fi
if [ ! -r "${RootDir}" ]; then
  \echo "${RootDir} is not readable." 2>&1
  \exit 1
fi
\mv "${RecInfoFilepath}" "${RecInfoFilepath}".prev
for Infofile in ${SrcInfoFilepaths[@]}; do
  if [ ! -r "${Infofile}" ]; then
   \echo "${Infofile} is not readable." 2>&1
   \exit 1
  fi
  if [ ! -w "${Infofile}" ]; then
    \echo "${Infofile} is not writable." 2>&1
    \exit 1
  fi
  \cp -a "${Infofile}" "${Infofile}.prev"
done
if [ ! -r "${SrcInfoFilepath3}" ]; then
  \echo "${SrcInfoFilepath3} is not readable." 2>&1
  \exit 1
fi

# check dependency of software
\which catWithNewline.sh 1> /dev/null 2> /dev/null
RetValue=$?
if [ ${RetValue} -ne 0 ]; then
  \echo "Aborted." 1>&2
  \echo "Please install catWithNewline.sh." 1>&2
  \exit 1
fi

# update each information file
#   If TV-file has been deleted, delete the lines in the information file
#
# How to update each information file
#  Set all TV-filepaths in information file to an array.
#  Remove TV-filepaths which exist in storage from the array.
#  Find and delete texts in information files which include the TV-filepath in the array.

for SrcFilepath in ${SrcInfoFilepaths[@]}; do
  if [ ! -f "${SrcFilepath}" ]; then
    \continue
  fi

  # duplicate information file to edit text
  \cp -a "${SrcFilepath}" "${SrcFilepath}.work"
  WorkFile="${SrcFilepath}.work"

  # set all TV-filepath strings in the file to an array
  # TvFilepathStems=($(\cat "${SrcFilepath}" | \grep -oP "/bigdata/01/[^, \t\.]+" | \sort -u))

  # set all TV-filepath-stem strings in the file to an array
  TvFilepathStems=($(\cat "${SrcFilepath}" | grep -Po "/bigdata/01/[\-_/a-zA-Z0-9]*" | \sort -u))

  # remove TV-filepath-stems which exist in storage from the array
  for PathStem in ${TvFilepathStems[@]}; do
    # get how many same stem files in /bigdata/01/
    CountFiles=$(\find "${PathStem%/*}/" -name "${PathStem##*/}*" -type f | wc -l)
    if [ ${CountFiles} -lt 1 ]; then
      # TvFilepathStem does not exist
      # Delete all lines of TvFilepathStem in SrcFilepath
      \cat "${WorkFile}" | \sed -e "\|${PathStem}|d" > "${WorkFile}".tmp
      \mv "${WorkFile}".tmp "${WorkFile}"
    fi
  done
  \mv "${WorkFile}" "${SrcFilepath}"
done

# create new record-file-info.txt
## 3 files(SrcInfoFilepath[1,2,3] should be concatenated.
##   set 3 files into an array
SrcInfoFilepaths+=("${SrcInfoFilepath3}")

ConcatFiles=()
for EachFile in ${SrcInfoFilepaths[@]}; do
  if [ -f "${EachFile}" ]; then
    ConcatFiles=("${ConcatFiles[@]}" "${EachFile}")
  fi
done
\catWithNewline.sh $(\echo ${ConcatFiles[@]}) > "${RecInfoFilepath}".tmp
\mv "${RecInfoFilepath}".tmp "${RecInfoFilepath}"
\exit 0

