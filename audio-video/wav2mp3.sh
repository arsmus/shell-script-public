#!/bin/bash

# 2013 Oct. 20.
# convert wav-sound-file to mp3-sound-file
# 	from http://d.hatena.ne.jp/cprcn/20100623

for i in ./*; \
 do cp "$i" /tmp/test.wav && \
 /usr/bin/lame --preset extreme /tmp/test.wav /tmp/test.mp3 && \
 mv /tmp/test.mp3 "${i//.wav/.mp3}"; \
done

