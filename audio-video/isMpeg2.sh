#!/bin/bash
# 2021 Mar. 19.
# 2021 Jan. 30.
# 2021 Jan. 28.
# Ryuichi Hashimoto.

# check video is mpeg2-video-file or not.
# mediainfo-package is required.


Cmd=${0##*/}

############
# function #
############
usage() {
cat << EOP
Usage: $Cmd VideoFile
result code
  0: video is mpeg2-video-file.
  1: Error
  2: video is not mpeg2-video-file.
mediainfo is required.
EOP
}

########
# main #
########
# check relied softwares
TmpStr=$(which mediainfo)
Result=$?
if [ $Result -ne 0 ]; then
    echo "Please install mediainfo." 1>&2
    \exit 1
fi

# check arguement
if [ $# -ne 1 ]; then
  echo "number of arguements must be 1."  1>&2
  usage
  exit 1
fi
if [ ! -f "$1" ]; then
  echo "${1} is not a file."  1>&2
  usage
  exit 1
fi

# mediainfo出力のAudioセクションの手前までの文字列について
# "Format  : MPEG-TS"
# "Format  : MPEG Video"
# "Format version  : Version 2"
# があればMPEG2と判定する
mediainfo "$1" | sed -n '1,/Audio/p' | grep 'Format' | tr -d '\n' | grep -Pq "Format *: MPEG-TSFormat *: *MPEG VideoFormat version *: Version 2"
ResultCode=$?
if [ $ResultCode -eq 0 ]; then
  # the file is MPEG2
  exit 0
else
  # the file is not MPEG2
  exit 2
fi
exit 1

