#!/bin/bash
# 2022 Jan. 15.
# Ryuichi Hashimoto.

Cmd="${0##*/}"
InfoFile1=/bigdata/01/tmp-record-file-info/epgrec/record-file-info-epgrec20210517.txt
InfoFile2=/bigdata/01/tmp-record-file-info/chinachu/record-file-info-chinachu20210417.txt
OldInfoFile1=/bigdata/01/tmp-record-file-info/epgrec/record-file-information-unified.txt
OldInfoFile2=/bigdata/01/tmp-record-file-info/chinachu/record-file-info-chinachu20210417.txt.202201021824
SearchDir="$1"

if [ $# -ne 1 ]; then
  echo "Give search directory to arguement." 1>&2
  echo "Usage: ${Cmd} Search-Dir"
  exit 1
fi

SearchDir="${SearchDir%/}"

while read -d $'\0' EachFile; do
  grep -q -m 1 "${EachFile}" "${InfoFile1}"
  RetVal=$?
  if [ ${RetVal} -eq 0 ]; then
    break
  fi
  grep -q -m 1 "${EachFile}" "${InfoFile2}"
  RetVal=$?
  if [ ${RetVal} -eq 0 ]; then
    break
  fi
  grep -q -m 1 "${EachFile}" "${OldInfoFile1}"
  RetVal=$?
  if [ ${RetVal} -eq 0 ]; then
    echo "${EachFile} is not written in ${InfoFile1} but written in ${OldInfoFile1}"
    break
  fi
  grep -q -m 1 "${EachFile}" "${OldInfoFile2}"
  RetVal=$?
  if [ ${RetVal} -eq 0 ]; then
    echo "${EachFile} is not written in ${InfoFile2} but written in ${OldInfoFile2}"
    break
  fi
done < <(find "${SearchDir}/" -type f -print0)
exit 0

