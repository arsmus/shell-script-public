#!/bin/bash
#
# 2010 Aug. 13.
# Ryuichi Hashimoto.
#
# Convert mp2-sound-file to mp3-sound-file.
#

# check args
if [ $# -lt 1 ]; then
	echo 'No original file.'
	exit
fi

SRCS=($@)

for FNAME in ${SRCS[@]}
do
	ffmpeg -i $FNAME 2>&1 | egrep 'Audio: mp2' > /dev/null
	if [ $? -ne 0 ]; then
		echo "${FNAME} is not mp2-sound-file."
	else
		lame --mp2input --vbr-new -V0 -b256 -F -f -m s --notemp --nores --interch 1 -p -k $FNAME ${FNAME}.mp3
		if [ $? -ne 0 ]; then
			echo "Failed converting from mp2 to mp3."
			rm ${FNAME}.mp3
		else
			mv ${FNAME} ${FNAME}.mp2
		fi
	fi
done

