#!/bin/bash
# 2022 Jan. 02.
# Ryuichi Hashimoto
 
# Delete FILE.ts in Dir1, if FILE.mp4 exists in Dir2


CMDNAME=${0##*/}

################
### function ###
################
function usage() {
cat << EOP
Usage: $CMDNAME TS-DIR MP4-DIR

Duplicate ts-files in TS-DIR having mp4-files in MP4-DIR will be deleted.

FILEX.ts in TS-DIR is deleted, if FILEX.mp4 exists in MP4-DIR.
"FILEX" is unique literal to the video.
FILEXaa.ts and FILEXbb.ts in TS-DIR are going to be deleted.
EOP
}

############
### main ###
############

# check command line arguements
if [ $# -ne 2 ]; then
  \echo "Invalid number of command line arguement" 1>&2
  \echo
  usage 1>&2
  \exit 1
fi

TsDir="$1"
TsDir="${TsDir%/}"
Mp4Dir="$2"
Mp4Dir="${Mp4Dir%/}"

# check given dirs
if [ ! -d "${TsDir}" ]; then
  \echo "${TsDir} is not directory."
  \exit 1
fi
if [ ! -r "${TsDir}" ]; then
  \echo "${TsDir} is not readable."
  \exit 1
fi
if [ ! -w "${TsDir}" ]; then
  \echo "${TsDir} is not writable."
  \exit 1
fi
if [ ! -x "${TsDir}" ]; then
  \echo "${TsDir} is not accessable."
  \exit 1
fi
if [ ! -d "${Mp4Dir}" ] || [ ! -r "${Mp4Dir}" ] || [ ! -x "${Mp4Dir}" ]; then
  \echo "${TsDir} must have the permission of rx."
  \exit 1
fi

# get duplicate mp4-files from Mp4sDir and
# delete it.
while read -d $'\0' Mp4File; do
  Mp4FileStem=$(echo "${Mp4File}" | sed "s/-[0-9]*\.mp4//")
  Mp4FileStem="${Mp4FileStem##*/}"
  if [ -f "${TsDir}/${Mp4FileStem}"*.m2ts ]; then
    rm "${TsDir}/${Mp4FileStem}"*.m2ts
  fi
  if [ -f "${TsDir}/${Mp4FileStem}"*.ts ]; then
    rm "${TsDir}/${Mp4FileStem}"*.ts
  fi
done < <(find "${Mp4Dir}"/ -name "*.mp4" -type f -print0)

exit 0

