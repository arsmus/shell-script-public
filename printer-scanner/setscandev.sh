#!/bin/bash
# 2007 Mar. 18.
#
# Set device permission for Brother DCP-115C scanner.
#
# usbscanner node is needed
#	mknod -m 666 /dev/usbscanner c 180 48

DIR1=`sane-find-scanner -q | awk '{print $7}' | awk 'BEGIN { FS = ":"} ; {print $2 }'`
DIR2=`sane-find-scanner -q | awk '{print $7}' | awk 'BEGIN { FS = ":"} ; {print $3 }'`
chmod 0666  /proc/bus/usb/${DIR1}/${DIR2}

