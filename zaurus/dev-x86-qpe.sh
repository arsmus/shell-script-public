#!/bin/bash

CROSSCOMPILE=/opt/Embedix/tools
QPEDIR=/opt/Qtopia
QTDIR=/opt/Qtopia
PATH=$QTDIR/bin:$QPEDIR/bin:$PATH:/opt/Embedix/tools/bin:/opt/Qtopia/tmake/bin
TMAKEPATH=/opt/Qtopia/tmake/lib/qws/linux-x86-g++/
LD_LIBRARY_PATH=$QTDIR/lib:$LD_LIBRARY_PATH

export QPEDIR
export QTDIR
export PATH
export TMAKEPATH
export LD_LIBRARY_PATH
export PS1

echo "Altered environment for sharp Zaurus Development x86"

