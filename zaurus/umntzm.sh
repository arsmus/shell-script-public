#/bin/bash
#
# 2008 Dec. 28.
#
# Written by Ryuichi Hashimoto.
#
# Unmount directories for Zaurus-external-memory
#
#


# set DirName to variables
MNTDIR='/media/disk'
LOOPDIR='/mnt/ext2loop'


# Start unmount
umount $LOOPDIR
if [ $? -ne 0 ]; then
	echo "Failed to umount ${LOOPDIR}."
	exit 1
fi

umount $MNTDIR

