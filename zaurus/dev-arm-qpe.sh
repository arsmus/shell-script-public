#!/bin/bash

CROSSCOMPILE=/opt/Embedix/tools
QPEDIR=/opt/Qtopia/sharp
QTDIR=/opt/Qtopia/sharp
PATH=$QTDIR/bin:$QPEDIR/bin:$CROSSCOMPILE/bin:$PATH:/opt/Qtopia/tmake/bin
TMAKEPATH=/opt/Qtopia/tmake/lib/qws/linux-sharp-g++/
LD_LIBRARY_PATH=$QTDIR/lib:$LD_LIBRARY_PATH

export QPEDIR QTDIR PATH TMAKEPATH LD_LIBRARY_PATH PS1

echo "Altered environment for sharp Zaurus Development ARM"

