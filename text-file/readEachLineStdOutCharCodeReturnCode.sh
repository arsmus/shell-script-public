#!/usr/bin/env bash
# 2023 Feb. 23.
# Ryuichi Hashimoto.

## skelton ##
# TEXTFILE="TEXTFILEPATH"
# if [ ! -f "${TEXTFILE}" ] || [ ! -r "${TEXTFILE}" ]; then
#   echo "${TEXTFILE} is not a file or not readable." 1>&2
#   exit 1
# fi
#
# while read EachLine; do
#   echo $EachLine
# done < "${TEXTFILE}"


Cmd="${0##*/}"

function usage() {
cat << EOP
usage: ${Cmd} TEXTFILE

Read line by line from TEXTFILE.
Each line has a path of a text-file.
Standard-out char-code and return-code of the text-file.
EOP
}


if [ $# -ne 1 ]; then
  echo "Illegal number of auguements." 1>&2
  usage
  exit 1
fi


TEXTFILE="$1"
if [ ! -f "${TEXTFILE}" ] || [ ! -r "${TEXTFILE}" ]; then
  echo "${TEXTFILE} is not a file or not readable." 1>&2
  exit 1
fi

while read EachLine; do
  nkf --guess "${EachLine}"
done < "${TEXTFILE}"
exit 0

