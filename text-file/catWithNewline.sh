#!/bin/bash
# 2022 Jan. 10.
# 2021 May 30.
# Ryuichi Hashimoto.

# Concatenate files with a separater of a new line character


Cmd=${0#*/}
OptF=0

################
### function ###
################
function usage_exit() {
cat << EOS
Concatenate files with a separater of a new line character.
Usage: $Cmd [-d DestFilePath] tv-record-info-file1 tv-record-info-file2 [tv-record-info-file3 ...]
If '-d' option is not given, result is put to STANDARD-OUT.
EOS
exit 1
}

############
### main ###
############
# check commandline-arguements
if [ $# -lt 2 ]; then
  echo "Error: Too few arguements" 1>&2
  usage_exit 1>&2
fi

# get commandline-arguements
while getopts :d: OPT; do
  case $OPT in
    d) DestFilePath="$OPTARG"; OptF=1
      ;;
    *) usage_exit 1>&2
      ;;
  esac   
done
shift $((OPTIND - 1))

if [ $OptF -eq 1 ]; then
  # check DestDir
  DestDir=${DestFilePath%/*}
  if [ $DestDir != $DestFilePath ]; then
    if [ ! -d "${DestDir}" ]; then
      echo "${DestDir} does not exist." 1>&2
      usage_exit
    fi 
    if [ ! -w "${DestDir}" ]; then
      echo "${DestDir} is not writable." 1>&2
      exit 1
    fi 
    if [ ! -x "${DestDir}" ]; then
      echo "${DestDir} is not executable." 1>&2
      exit 1
    fi 
  fi

  # backup an existing file
  if [ -r "${DestFilePath}"  ]; then
    mv "${DestFilePath}" "${DestFilePath}.old"
    touch "${DestFilePath}"
  else
    echo "${DestFilePath} is not readable." 1>&2
    exit 1
  fi
else
  touch /tmp/record-file-info.txt
fi

# set files in command-line-arguements to an array
ArgFiles=("$@")

# concatenate files
for Count in `seq 1 $#`
do
  i=$(expr $Count - 1)
  if [ ! -f "${ArgFiles[${i}]}" ]; then
    echo "${ArgFiles[${i}]} does not exist." 1>&2
    continue
  fi
  if [ $OptF -eq 1 ]; then
    cat ${ArgFiles[${i}]} >> "${DestFilePath}"
    echo "" >> "${DestFilePath}"
  else
    cat ${ArgFiles[${i}]} >> /tmp/record-file-info.txt
    echo "" >> /tmp/record-file-info.txt
  fi
done

if [ $OptF -ne 1 ]; then
  cat /tmp/record-file-info.txt
  rm /tmp/record-file-info.txt
fi

exit 0

