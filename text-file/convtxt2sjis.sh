#!/bin/bash
#
# 2009 Oct. 18.
# Written by Ryuichi Hashimoto.
#
# Convert text-files to shift-jis text files.
#
mkdir ./sjis
if [ $? -ne 0 ]; then
	exit
fi


for F in ./*.txt; do
	nkf -s -Lw < $F > ./sjis/$F
done

