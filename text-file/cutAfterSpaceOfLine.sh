#!/bin/bash
# 2017 Nov. 23.
# Ryuichi Hashimoto.

# 標準入力から1行ずつ読み、最初の空白以降の文字列を削除して標準出力に送る


CMDNAME=`basename $0`

if [ $# -gt 0 ] && [ ! -p /dev/stdin ]; then
   echo "Too many arguements."
   echo "${CMDNAME} < TXT-FILE"
   exit 1
fi

cat - | while read line; do
   echo $line | sed -e 's/　.*/ /g' | sed -e 's/\t.*/ /g' | sed -e 's/ .*/ /g'
done

