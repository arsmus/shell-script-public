#!/bin/bash

# 2018 Apr. 28.
# Ryuichi Hashimoto.

# Set new-line(\n) at the end of file.


Cmd=`basename $0`

if [ $# -ne 1 ]; then
   echo "${Cmd} TEXT-FILE"
   exit 1
fi

# check if the type of the file is text or not.
FileType=`file --mime $1`
echo ${FileType} | grep -iq 'text'
if [ 0 -ne $? ]; then
   echo "${1} is not text-file."
   exit 1
fi

# get last line of the text-file.
str=`tail -n 1 ${1}`

# Output new line char at the end of file
# if the last line is not empty line.
if [ 0 -ne ${#str} ] ; then
   echo "" >> $1
fi
exit 0

