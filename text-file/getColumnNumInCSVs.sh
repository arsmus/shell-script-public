#!/bin/bash
# 2018 May 27.
# Ryuichi Hashimoto.

# Outputs csv-filename-path and counts of columns in line in the csv-file in directory.
# Outputs 0, if counts of columns are not same in the file.


Cmd=`basename $0`
if [ $# -ne 1 ]; then
   echo "$Cmd directory"
   exit 1
fi

Dir=$1
if [ ! -d $Dir ]; then
   echo "$Dir is not a directory."
   echo 1
fi
Dir=${Dir%/}

for File in `find ${Dir}/ -name "*.csv"`; do
   echo -n "$File "
   getCsvColumnNum.sh $File
   echo $?
done
exit 0

