#!/bin/bash
# 2018 Jul. 14.
# 2018 Jul. 11.
# 2018 Jul. 07.
# Ryuichi Hashimoto

# Inform you the type of csv-file by result-code.
#   result-code 1: double quoted string type  "str1",num,"str2"
#   result-code 2: no quoted string type       str1,num,str2
#   result-code 3: other types or error

# synopsis
#   COMMAND < CSV-FILE
#   COMMAND  CSV-FILE


Cmd=`basename $0`

# check command-line-arguement
if [ $# -gt 1 ]; then
   echo "SYNOPSIS  $Cmd < CSV-FILE"
   echo "          $Cmd CSV-FILE"
   echo ""
   echo "  Return the type of csv-file by result-code."
   echo "     return-code 1: double quoted string type  \"str1\",num,\"str2\""
   echo "     return-code 2: no quoted string type       str1,num,str2"
   echo "     return-code 3: other types or error"
   exit 3
fi

#### function
####   Return the type of csv-file by result-code.
####     return-code 1: double quoted string type  "str1",num,"str2"
####     return-code 2: no quoted string type       str1,num,str2
####     return-code 3: other types or error
####   
function countStrMatch() {
   # 検索文字列設定
   local SearchDoubleQuateHead=^\"
   local SearchDoubleQuateA=\",\"
   local SearchDoubleQuateB=\",[0-9]
   local SearchDoubleQuateC=[0-9],\"
   local SearchDoubleQuateEnd=\"$
   local SearchNoQuateHead=^[^\"0-9]
   local SearchNoQuateA=[^0-9\"],[^0-9\"]
   local SearchNoQuateB=[^0-9\"],[0-9]
   local SearchNoQuateC=[0-9],[^0-9\"]
   local SearchNoQuateEnd=[^0-9\"]$

   # カウント数格納変数
   local countMatchDoubleQuateHead=0
   local countMatchDoubleQuateStringA=0
   local countMatchDoubleQuateStringB=0
   local countMatchDoubleQuateStringC=0
   local countMatchDoubleQuateEnd=0
   local countMatchNoQuateHead=0
   local countMatchNoQuateStringA=0
   local countMatchNoQuateStringB=0
   local countMatchNoQuateStringC=0
   local countMatchNoQuateEnd=0

   while read Line
   do
      if [[ ${Line} = "err" ]]; then
         exit 3
      fi

      # count double-quate-matches at the head of string
      countMatch=`echo $Line | grep -c "${SearchDoubleQuateHead}"`
      countMatchDoubleQuateHead=$(($countMatchDoubleQuateHead + $countMatch))

      # count double-quate-matches in string
      countMatch=`echo $Line | sed "s/\(${SearchDoubleQuateA}\)/\1\n/g" | grep -c "${SearchDoubleQuateA}"`
      countMatchDoubleQuateStringA=$(($countMatchDoubleQuateStringA + $countMatch))

      countMatch=`echo $Line | sed "s/\(${SearchDoubleQuateB}\)/\1\n/g" | grep -c "${SearchDoubleQuateB}"`
      countMatchDoubleQuateStringB=$(($countMatchDoubleQuateStringB + $countMatch))

      countMatch=`echo $Line | sed "s/\(${SearchDoubleQuateC}\)/\1\n/g" | grep -c "${SearchDoubleQuateC}"`
      countMatchDoubleQuateStringC=$(($countMatchDoubleQuateStringC + $countMatch))

      # count double-quate-matches at the end of string
      countMatch=`echo $Line | grep -c $SearchDoubleQuateEnd`
      countMatchDoubleQuateEnd=$(($countMatchDoubleQuateEnd + $countMatch))

      # count no-quate-matches at the head of string
      countMatch=`echo $Line | grep -c "${SearchNoQuateHead}"`
      countMatchNoQuateHead=$(($countMatchNoQuateHead + $countMatch))

      # count no-quate-matches in string
      countMatch=`echo $Line | sed "s/\(${SearchNoQuateA}\)/\1\n/g" | grep -c "${SearchNoQuateA}"`
      countMatchNoQuateStringA=$(($countMatchNoQuateStringA + $countMatch))

      countMatch=`echo $Line | sed "s/\(${SearchNoQuateB}\)/\1\n/g" | grep -c "${SearchNoQuateB}"`
      countMatchNoQuateStringB=$(($countMatchNoQuateStringB + $countMatch))

      countMatch=`echo $Line | sed "s/\(${SearchNoQuateC}\)/\1\n/g" | grep -c "${SearchNoQuateC}"`
      countMatchNoQuateStringC=$(($countMatchNoQuateStringC + $countMatch))

      # count no-quate-matches at the end of string
      countMatch=`echo $Line | grep -c $SearchNoQuateEnd`
      countMatchNoQuateEnd=$(($countMatchNoQuateEnd + $countMatch))

   done < /dev/stdin

   countDoubleQuate=$(($countMatchDoubleQuateHead + $countMatchDoubleQuateStringA + $countMatchDoubleQuateStringB + $countMatchDoubleQuateStringC + $countMatchDoubleQuateEnd))
   countNoQuate=$(($countMatchNoQuateHead + $countMatchNoQuateStringA + $countMatchNoQuateStringB + $countMatchNoQuateStringC + $countMatchNoQuateEnd))

   # echo "ダブルクォート文頭　$countMatchDoubleQuateHead" 1>&2
   # echo "ダブルクォート文中区切りA　$countMatchDoubleQuateStringA" 1>&2
   # echo "ダブルクォート文中区切りB　$countMatchDoubleQuateStringB" 1>&2
   # echo "ダブルクォート文中区切りC　$countMatchDoubleQuateStringC" 1>&2
   # echo "ダブルクォート文末　$countMatchDoubleQuateEnd" 1>&2
   # echo "クォート無し文頭　$countMatchNoQuateHead" 1>&2
   # echo "クォート無し文中区切りA　$countMatchNoQuateStringA" 1>&2
   # echo "クォート無し文中区切りB　$countMatchNoQuateStringB" 1>&2
   # echo "クォート無し文中区切りC　$countMatchNoQuateStringC" 1>&2
   # echo "クォート無し文末　$countMatchNoQuateEnd" 1>&2
   echo "ダブルクォート区切り総数　$countDoubleQuate" 1>&2
   echo "クォート無し区切り総数　$countNoQuate" 1>&2

   if [ $countDoubleQuate -gt $countNoQuate ]; then
      return 1
   elif [ $countDoubleQuate -lt $countNoQuate ]; then
      return 2
   else
      return 3
   fi
   return 3
}


####
#### main
####
if [ -p /dev/stdin ]; then
   cat -
else
   if [ ! -f $1 ]; then
      echo "${1} : not exist." 1>&2
      echo "" 1>&2
      echo "SYNOPSIS  $Cmd < CSV-FILE" 1>&2
      echo "          $Cmd CSV-FILE" 1>&2
      echo "" 1>&2
      echo "  Returns the type of csv-file by result-code." 1>&2
      echo "     return-code 1: double quoted string type  \"str1\",num,\"str2\"" 1>&2
      echo "     return-code 2: no quoted string type       str1,num,str2" 1>&2
      echo "     return-code 3: other types or error" 1>&2
      echo "err"
   else
      cat $1
   fi
fi | countStrMatch

exit $?

