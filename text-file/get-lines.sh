#!/bin/bash
# 2017 Dec. 09.
# Ryuichi Hashimoto.

# Output lines of file in which count of letters is in the range specified.
# Output is standard-out.
#   COMMAND MIN MAX FILE
#     MIN: Least count of characters
#     MAX: Max count of characters


if [ $# -ne 3 ] || [ $1 -gt $2 ]; then 
   echo "COMMAND MIN MAX FILE"
   echo "  MIN: Least count of characters"
   echo "  MAX: Max count of characters"
   exit 1
fi

cat $3 | while read line 
do
   if [ ${#line} -ge $1 ] && [ ${#line} -le $2 ]  ; then
      echo ${line}
   fi
done
exit 0

