#!/bin/bash
# 2020 Oct. 31.
# 2020 Oct. 17.
# 2019 Oct. 22.
# 2019 Jan. 20.
# 2018 Nov. 18.
# 2018 Nov. 11.
# 2018 Nov. 10.
# 2018 Oct. 21.
# Ryuichi Hashimoto.

# Update an unified recorded-tv-csv-file.
# Usage: command

# "output_csv_from_epgrec-db.sh" which I wrote is required.


# RootDir='/bigdata/csv'
RootDir='/bigdata/disk1/csv'
UnifiedDir=${RootDir}/unified
UnifiedCsv='unified.csv'
LatestMysqlCsvDir=${RootDir}/latest_mysql
BackupDir="${HOME}/grive-rasumonly/backup/tv-video-list"
BackupOwner='USER'
CommandName=${0##*/}

if [[ '-h' = $1 ]] || [[ '--help' = $1 ]]; then
  \echo "Usage: ${CommandName}"
  \echo "Update an unified recorded-tv-csv-file."
  \echo "Unified recorded-tv-csv-file is ${UnifiedDir}/${UnifiedCsv}"
  \exit 1
fi

if [ $# -ne 0 ]; then
  \echo "Error arguement."
  \echo "Usage: ${CommandName}"
  \exit 1
fi

# get latest csv-files
\output_csv_from_epgrec-db.sh $LatestMysqlCsvDir

# backup an old csv-file
if [ ! -f ${UnifiedDir}/${UnifiedCsv} ]; then
  \touch ${UnifiedDir}/${UnifiedCsv}
fi
if [ -f ${UnifiedDir}/${UnifiedCsv} ]; then 
  FileSize=$(\ls -l ${UnifiedDir}/${UnifiedCsv} | \sed -e "s/[ $'\t'][ $'\t']*/ /g" | \cut -d " " -f 5)
  if [[ $FileSize -lt 4000000 ]]; then
    \cp -p ${UnifiedDir}/${UnifiedCsv} ${UnifiedDir}/${UnifiedCsv}.bak.tooSmall
  else
    \cp -p ${UnifiedDir}/${UnifiedCsv} ${UnifiedDir}/${UnifiedCsv}.bak
  fi
fi

# unify csv-files
\find ${UnifiedDir}/ $LatestMysqlCsvDir/ -type f -name '*.csv' | xargs cat | sort | uniq > /tmp/${UnifiedCsv}
\cp /tmp/${UnifiedCsv} ${UnifiedDir}/${UnifiedCsv}
\cp /tmp/${UnifiedCsv} ${BackupDir}/${UnifiedCsv}
\chmod 644 ${UnifiedDir}/${UnifiedCsv} ${BackupDir}/${UnifiedCsv}
\chown ${BackupOwner}:${BackupOwner} ${BackupDir}/${UnifiedCsv}

\exit 0

