#!/bin/bash
# 2018 Jun. 20.
# 2015 Jun. 23.
# 2015 Jun. 21.
# Ryuichi Hashimoto.

# compare same name text-files in 2 dirs appointed with arguments.
#   command -dir1 dir2

dir1=$1
dir2=$2

lastchar=${dir1: -1}
if [ $lastchar != '/' ]; then
  dir1="${dir1}/"
fi

lastchar=${dir2: -1}
if [ $lastchar != '/' ]; then
  dir2="${dir2}/"
fi

# set all filenames in dir1 to an array.
for filepath in ${dir1}* ; do
  if [ -f $filepath ]; then
    filetype=`file -b ${filepath}`
    filetype=${filetype// /_}
    if [ `echo $filetype | grep 'text'` ]; then
      filename=`basename $filepath`
      diff $filepath ${dir2}${filename}
    fi
  fi
done



