#!/usr/bin/env bash
# 2023 Feb. 19.
# 2023 Feb. 12.
# 2023 Feb. 05.
# 2023 Jan. 29.
# Ryuichi Hashimoto.
# Concatnate files to STANDARD-OUT, inserting enpty line between files.

set -u

Cmd=${0##*/}
NumEmpLines=1
PrintPath='f'
  # f: filename is printed at the top of each file.  
  # p: fullpath is printed at the top of each file.  
  # n: nothing is printed at the top of each file.


# usage
function usage() {
cat << EOP
Usage: ${Cmd} [options] FILE1 [FILE2 ...]
Concatnate files to STANDARD-OUT, inserting enpty line(s) between files.
Option
-n Num  Insert Num empty line(s) between files.
        Num must be 1 or greater than 1.
        Without "-n" option, 1 empty line is inserted between files.
-p f|p|n  f: fileName is printed at the top of each file.(Default) 
          p: fullPath is printed at the top of each file.
          n: nothing is printed at the top of each file.
EOP
}

# check existance of arguemants
if [ $# -lt 1 ]; then
  usage
  exit 1
fi

# get command-line-option
while getopts n:p: Opt; do
  case $Opt in
    n)
      NumEmpLines=${OPTARG}
      ;;
    p)
      PrintPath=${OPTARG}
      ;;
    \?)
      echo "Unexpected option." 1>&2
      usage
      exit 1
  esac
done
shift `expr "${OPTIND}" - 1`

# check arguement
if [ $# -lt 1 ]; then
  echo "Error. Specify files as arguments." 1>&2
  usage
  exit 1
fi

if [[ $NumEmpLines -lt 1 ]]; then
  echo "Illegal '-n' option." 1>&2
  usage
  exit 1
fi

if [[ 'f' != "${PrintPath}" ]] && [[ 'p' != "${PrintPath}" ]] && [[ 'n' != "${PrintPath}" ]]; then
    echo "Illegal '-p' option." 1>&2
    echo "Arguement of '-p' is f, p or n." 1>&2
    usage
    exit 1
fi

# output filename-list
AryFiles=()
IsError=0
while [ $# -gt 0 ];do
  # check file in arguements
  if [ ! -f "$1" ] || [ ! -r "$1" ]; then
    echo ""
    echo "!!ERROR!! $1 is not file or not readable."
    echo ""
    echo "Error. $1 is not file or not readable." 1>&2
    IsError=$(( $IsError + 1 ))
    shift
    continue
  fi

  # output filename
  echo "${1}"

  # ファイルのフルパスを配列に入れていく
  Fpath=$(readlink -f "${1}") 
  AryFiles+=("$Fpath")

  shift
done

# 改ページを出力
echo -en "\f"

# Concatnate files
# shiftで順次処理せず、ファイルの配列で処理する
IsError=0
for Fpath in ${AryFiles[@]}; do
  if [ ! -f "${Fpath}" ] || [ ! -r "${Fpath}" ]; then
    echo "!!ERROR!! ${Fpath} is not file or not readable."
    echo "Error. ${Fpath} is not file or not readable." 1>&2
    IsError=$(( $IsError + 1 ))
    shift
    continue
  fi

  # Print fileName at the top of each file
  if [[ 'f' == "${PrintPath}" ]]; then
    \echo "${Fpath##*/}"
  elif [[ 'p' == "${PrintPath}" ]]; then
    \echo "${Fpath}"
  fi

  # Concatnate files
  cat "${Fpath}"
  for Count in $(seq 1 $NumEmpLines ); do
    echo ""
  done
done

if [ $IsError -gt 0 ]; then
  echo "Failed to print $IsError file(s)."
  echo "Failed to print $IsError file(s)." 1>&2
  exit 1
fi
exit 0

