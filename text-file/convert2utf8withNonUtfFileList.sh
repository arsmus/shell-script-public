#!/usr/bin/env bash
# 2023 Feb. 22.
# Ryuichi Hashimoto.

Cmd="${0##*/}"

function usage() {
cat << EOP
$Cmd LISTFILE DESTDIR

LISTFILE is a textFile with filePath in each line.
EOP
}

if [ $# -ne 2 ]; then
  echo "Illegal num of arguements." 1>&2
  usage
  exit 1
fi

ListFile="$1"
if [ ! -f "$ListFile"  ] || [ ! -r "$ListFile" ]; then
  echo "$ListFile is not a file or not readable." 1>&2 
  exit 1
fi

DestDir="${2%/}"
if [ ! -d "$DestDir" ]; then
  echo "$DestDir is not directory." 1>&2
  exit 1
fi

while read Line; do
  nkf -w8 "$Line" > "${DestDir}/${Line##*/}"
done < "$ListFile"
exit 0

