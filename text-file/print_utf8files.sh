#!/usr/bin/env bash
# 2023 Feb. 23.
# 2023 Jan. 29.
# Ryuichi Hashimoto.
# Print utf-8-text-file(s)

cmd=${0##*/}

# usage
function usage() {
cat << EOP
Usage: ${Cmd} UTF8FILE1 [UTF8FILE2 ...]
Description: Print utf8-text-file(s) with duplex-no-tumble without GUI.
             左側綴じ両面印刷でUTF-8文字コードのテキストファイルを
             コマンドラインから印刷する
EOP
}

# check software depended on
which paps > /dev/null
if [ $? -ne 0 ]; then
  echo "Install paps." 1>&2
  exit 1
fi

# check existance of arguements
if [ $# -lt 1 ]; then
  usage
  exit 1
fi

# get printer from /etc/printcap
MyPrinter=$(grep -m 1 "^[^#]" /etc/printcap | sed "s/|.*//")

# print files
IsError=0
while [ $# -gt 0 ];do
  # check file in arguements
  if [ ! -f "$1" ] || [ ! -r "$1" ]; then
    echo "Error. $1 is not file or not readable." 1>&2
    IsError=$(( $IsError + 1 ))
    shift
    continue
  fi

  # print
  paps --header "$1" | lpr -P "$MyPrinter" -o Duplex=DuplexNoTumble
  shift
done

if [ $IsError -gt 0 ]; then
  echo "Failed to print $IsError file(s)." 1>&2
  exit 1
fi
exit 0

