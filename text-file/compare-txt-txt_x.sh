#!/bin/bash

# 2018 Apr. 21.
# 2018 Apr. 08.
# Ryuichi Hashimoto.

# compare FILE.txt with FILE.txt.x by modified time.

DirRaw=${HOME}/data/registration/latest/
DirEncrypt=${HOME}/data/grive-rasumonly/doc/registration/latest/

for RawPath in `find ${DirRaw} -type f`; do
   RawFile=`basename ${RawPath}`
   RawParentPath=`dirname ${RawPath}`
   RawParentDir=${RawParentPath##*/}
   EncryptFile=${RawFile}.x
   for EncryptPath in `find ${DirEncrypt} -type f -name ${EncryptFile}`; do
      EncryptParentPath=`dirname ${EncryptPath}`
      EncryptParentDir=${EncryptParentPath##*/}
      if [ ${RawParentDir} != ${EncryptParentDir} ]; then
         continue
      fi
   done

   if [ ${EncryptPath} -ot ${RawPath} ]; then
      echo "Abnormal!"
      ls -ltr ${EncryptPath} ${RawPath}
      echo ""
   fi
done
exit 0

