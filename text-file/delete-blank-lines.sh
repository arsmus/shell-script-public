#!/bin/bash
# 2017 Dec. 09.
# Ryuichi Hashimoto.

# delete blank lines in the file of arguement.
# output is standard-out.


cat $1 | while read line
do
   if [ -n "${line}" ]; then
      echo $line
   fi
done

