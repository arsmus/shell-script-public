#!/bin/bash
# 2018 Oct. 28.
# 2018 Oct. 21.
# Ryuichi Hashimoto.

# This script is run once initially.
# Concatenate latest csv-files from mysql and all csv-files in directory recursively.
# Synopsis: command


RootDir='/bigdata/csv'
UnifiedDir=${RootDir}/unified
UnifiedCsv='unified.csv'
LatestMysqlCsvDir=${RootDir}/latest_mysql
CommandName=$(basename $0)

if [[ '-h' = $1 ]] || [[ '--help' = $1 ]]; then
  echo "Synopsis: ${CommandName}"
  echo "Concatenate latest csv-files from mysql and all csv-files in directory recursively."
  exit 1
fi

if [ $# -ne 0 ]; then
  echo "Error arguement."
  echo "Synopsis: ${CommandName}"
  exit 1
fi

# get latest csv-files
output_csv_from_epgrec-db.sh $LatestMysqlCsvDir

# unify csv-files
# \find ${RootDir}/ -type f -name '*.csv' | xargs cat | sort | uniq > ${UnifiedDir}/${UnifiedCsv}
\find ${RootDir}/ -type f -name '*.csv' | xargs cat | sort | uniq > /tmp/${UnifiedCsv}
cp /tmp/${UnifiedCsv} ${UnifiedDir}/${UnifiedCsv}
chmod 755 ${UnifiedDir}/${UnifiedCsv}

exit 0

