#!/usr/bin/env bash
# 2023 Feb. 23.
# 2023 Feb. 22.
# Ryuichi Hashimoto.

Cmd="${0##*/}"

function usage() {
cat << EOP
NAME
  $Cmd - Output filePath that have non UTF-8 text and have EXT extension in DIR recursively to STANDARD-OUT. 

SYNOPSIS
  $Cmd DIR EXT
      EXT begins with dot(.).  ex) .txt
      When EXT is *, all files are searched.
EOP
}

if [ $# -ne 2 ]; then
  echo "Illegal number of arguements" 1>&2
  usage
  exit 1
fi

SearchDir="${1%/}"
Ext="$2"

if [ ! -d "$SearchDir" ]; then
  echo "$SearchDir is not directory." 1>&2
  exit 1
fi

while read -d $'\0' File; do
  RetStr=$(nkf --guess "${File}")
  if [[ ! $RetStr =~ "UTF-8" ]]; then
    echo "${File}"
  fi
done < <(find "${SearchDir}/" -type f -name "*${Ext}" -print0 )
exit 0

